/*
	Autor: José Zeck Alonso

	Segunda versão do flyway, destinada a excluir a tabela de teste do flyway  e criar as tabelas cadastrais
*/

-- DROPAR TABELA TESTE
DROP TABLE teste;

-- USUARIO
CREATE TABLE IF NOT EXISTS users (

	id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
	name VARCHAR(100) NOT NULL,
	email VARCHAR(100) NOT NULL,
	photo VARCHAR(500) NOT NULL,
	user_name VARCHAR(50) NOT NULL UNIQUE,
	password VARCHAR(100),
	document VARCHAR(11),
	create_date DATE,
	update_date DATE,
	create_user VARCHAR(100) NOT NULL,
	update_user VARCHAR(100),
	active BOOLEAN,
	CONSTRAINT PK_USERS PRIMARY KEY (id)

)ENGINE=InnoDB CHARSET=utf8mb4;
-------------------------------------------------------------------


-- CURSO
CREATE TABLE IF NOT EXISTS course(

	id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
	name_course VARCHAR(100),
	create_date DATE,
	update_date DATE,
	create_user VARCHAR(100) NOT NULL,
	update_user VARCHAR(100),
	CONSTRAINT PK_COURSE PRIMARY KEY (id)

)ENGINE=InnoDB CHARSET=utf8mb4;
-------------------------------------------------------------------


-- BAIRRO
CREATE TABLE IF NOT EXISTS district(

	id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
	name_district VARCHAR(100),
	create_date DATE,
	update_date DATE,
	create_user VARCHAR(100) NOT NULL,
	update_user VARCHAR(100),
	CONSTRAINT PK_DISTRICT PRIMARY KEY (id)

)ENGINE=InnoDB CHARSET=utf8mb4;


-- RUA
CREATE TABLE IF NOT EXISTS street(

		id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
		name_street VARCHAR(100),
		create_date DATE,
		update_date DATE,
		create_user VARCHAR(100) NOT NULL,
		update_user VARCHAR(100),
		CONSTRAINT PK_STREET PRIMARY KEY (id)

);


-- ENDERECO
-------------------------------------------------------------------

 
-- ESTUDANTE
CREATE TABLE IF NOT EXISTS student (

	id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
	name VARCHAR(100) NOT NULL,
	photo VARCHAR(500),
	cpf VARCHAR(11) NOT NULL,
	rg VARCHAR(20) NOT NULL,
	ra VARCHAR(20),
	email VARCHAR(100),
	phone1 VARCHAR(20),
	phone2 VARCHAR(20),
	birth_date DATE,
	user_student VARCHAR(50) NOT NULL,
	zip_code VARCHAR(9) NOT NULL,
	city VARCHAR(100) NOT NULL,
	complement VARCHAR(100),
	number_house INTEGER NOT NULL,
	uf CHAR(2),
	create_date DATE,
	update_date DATE,
	create_user VARCHAR(100) NOT NULL,
	update_user VARCHAR(100),
	payment_type VARCHAR(100) NOT NULL,
	active BOOLEAN,
	district_id INTEGER,
	street_id INTEGER,
	course_id INTEGER,
	CONSTRAINT PK_STUDENT PRIMARY KEY (id),
	CONSTRAINT FK_COURSE_STUDENT FOREIGN KEY (course_id) REFERENCES course (id),
	CONSTRAINT FK_DISTRICT_STUDENT FOREIGN KEY (district_id) REFERENCES district (id),
	CONSTRAINT FK_STREET_STUDENT FOREIGN KEY (street_id) REFERENCES street (id)
	

)ENGINE=InnoDB CHARSET=utf8mb4;


-- ANEXO DE ESTUDANTE
CREATE TABLE IF NOT EXISTS student_append(

	id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
	description VARCHAR(500) NOT NULL,
	append VARCHAR(500) NOT NULL,
    create_date DATE,
	update_date DATE,
	create_user VARCHAR(100) NOT NULL,
	update_user VARCHAR(100),
    student_id INTEGER,
	CONSTRAINT PK_STUDENTAPPEND PRIMARY KEY (id),
    CONSTRAINT FK_STUDENT_STUDENTAPPEND FOREIGN KEY (student_id) REFERENCES student (id)

)ENGINE=InnoDB CHARSET=utf8mb4;
-------------------------------------------------------------------



-- ASSOCIAÇÃO
CREATE TABLE IF NOT EXISTS association (

	id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
	owner VARCHAR(100) NOT NULL,
	fantasy_name VARCHAR(100) NOT NULL,
	cnpj VARCHAR(14) NOT NULL,
	email VARCHAR(100),
	phone VARCHAR(20),
	zip_code VARCHAR(9) NOT NULL,
	city VARCHAR(100) NOT NULL,
	complement VARCHAR(100),
	number_house INTEGER NOT NULL,
	uf CHAR(2),
	create_date DATE,
	update_date DATE,
	create_user VARCHAR(100) NOT NULL,
	update_user VARCHAR(100),
	district_id INTEGER,
	street_id INTEGER,
	CONSTRAINT PK_ASSOCIATION PRIMARY KEY (id),
	CONSTRAINT FK_DISTRICT_ASSOCIATION FOREIGN KEY (district_id) REFERENCES district (id),
	CONSTRAINT FK_STREET_ASSOCIATION FOREIGN KEY (street_id) REFERENCES street (id)
	

)ENGINE=InnoDB CHARSET=utf8mb4;


-- ANEXO ASSOCIAÇÃO
CREATE TABLE IF NOT EXISTS association_append (

	id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
	description VARCHAR(500) NOT NULL,
	append VARCHAR(500) NOT NULL,
	create_date DATE,
	update_date DATE,
	create_user VARCHAR(100) NOT NULL,
	update_user VARCHAR(100),
	association_id INTEGER,
	CONSTRAINT PK_ASSOCIATIONAPPEND PRIMARY KEY (id),
	CONSTRAINT FK_ASSOCIATION_APPEND FOREIGN KEY (association_id) REFERENCES association (id)

)ENGINE=InnoDB CHARSET=utf8mb4;
-------------------------------------------------------------------



-- UNIVERSIDADE
CREATE TABLE IF NOT EXISTS university(

	id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
	name VARCHAR(100),
	cnpj VARCHAR(14),
	email VARCHAR(100),
	phone VARCHAR(20),
	zip_code VARCHAR(9) NOT NULL,
	city VARCHAR(100) NOT NULL,
	complement VARCHAR(100),
	number_house INTEGER NOT NULL,
	uf CHAR(2) NOT NULL,
	create_date DATE,
	update_date DATE,
	create_user VARCHAR(100) NOT NULL,
	update_user VARCHAR(100),
	district_id INTEGER,
	street_id INTEGER,
	CONSTRAINT PK_UNIVERSITY PRIMARY KEY (id),
	CONSTRAINT FK_DISTRICT_UNIVERSITY FOREIGN KEY (district_id) REFERENCES district (id),
	CONSTRAINT FK_STREET_UNIVERSITY FOREIGN KEY (street_id) REFERENCES street (id)
	
)ENGINE=InnoDB CHARSET=utf8mb4;


-- ANEXO UNIVERSIDADE
CREATE TABLE IF NOT EXISTS university_append (

	id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
	description VARCHAR(100),
	append VARCHAR(500),
	create_date DATE,
	update_date DATE,
	create_user VARCHAR(100) NOT NULL,
	update_user VARCHAR(100),
	university_id INTEGER,
	CONSTRAINT PK_UNIVERSITYAPPEND PRIMARY KEY (id),
	CONSTRAINT FK_UNIVERSITY_APPEND FOREIGN KEY (university_id) REFERENCES university (id)

)ENGINE=InnoDB CHARSET=utf8mb4;
-------------------------------------------------------------------



-- EMPRESA
CREATE TABLE IF NOT EXISTS company(

	id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
	owner VARCHAR(100) NOT NULL,
	fantasy_name VARCHAR(100) NOT NULL,
	cnpj VARCHAR(14) NOT NULL,
	zip_code VARCHAR(9) NOT NULL,
	city VARCHAR(100) NOT NULL,
	complement VARCHAR(100),
	number_house INTEGER NOT NULL,
	uf CHAR(2) NOT NULL,
	active BOOLEAN,
	create_date DATE,
	update_date DATE,
	create_user VARCHAR(100) NOT NULL,
	update_user VARCHAR(100),
	district_id INTEGER,
	street_id INTEGER,
	CONSTRAINT PK_COMPANY PRIMARY KEY (id),
	CONSTRAINT FK_DISTRICT_COMPANY FOREIGN KEY (district_id) REFERENCES district (id),
	CONSTRAINT FK_STREET_COMPANY FOREIGN KEY (street_id) REFERENCES street (id)

)ENGINE=InnoDB CHARSET=utf8mb4;


-- ANEXO EMPRESA
CREATE TABLE IF NOT EXISTS company_append(

	id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
	description VARCHAR(100) NOT NULL,
	append VARCHAR(500) NOT NULL,
	create_date DATE,
	update_date DATE,
	create_user VARCHAR(100) NOT NULL,
	update_user VARCHAR(100),
	company_id INTEGER,
	CONSTRAINT PK_COMPANYAPPEND PRIMARY KEY (id),
	CONSTRAINT FK_COMPANY_APPEND FOREIGN KEY (company_id) REFERENCES company (id)

)ENGINE=InnoDB CHARSET=utf8mb4;
-------------------------------------------------------------------



-- MOTORISTA
CREATE TABLE IF NOT EXISTS driver (

	id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
	name VARCHAR(100) NOT NULL,
	photo VARCHAR(500),
	cpf VARCHAR(11) NOT NULL,
	rg VARCHAR(15) NOT NULL,
	license VARCHAR(20) NOT NULL,
	email VARCHAR(100) NOT NULL,
	phone1 VARCHAR(20),
	phone2 VARCHAR(20),
	birth_date DATE,
	zip_code VARCHAR(9) NOT NULL,
	city VARCHAR(100) NOT NULL,
	complement VARCHAR(100),
	number_house INTEGER NOT NULL,
	uf CHAR(2) NOT NULL,
	active BOOLEAN,
	create_date DATE,
	update_date DATE,
	create_user VARCHAR(100) NOT NULL,
	update_user VARCHAR(100),
 	district_id INTEGER,
	street_id INTEGER,
	CONSTRAINT PK_DRIVER PRIMARY KEY (id),
	CONSTRAINT FK_DISTRICT_DRIVER FOREIGN KEY (district_id) REFERENCES district (id),
	CONSTRAINT FK_STREET_DRIVER FOREIGN KEY (street_id) REFERENCES street (id)

)ENGINE=InnoDB CHARSET=utf8mb4;


-- ANEXO MOTORISTA
CREATE TABLE IF NOT EXISTS driver_append (

	id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
	description VARCHAR(500) NOT NULL,
	append VARCHAR(500) NOT NULL,
	create_date DATE,
	update_date DATE,
	create_user VARCHAR(100) NOT NULL,
	update_user VARCHAR(100),
	driver_id INTEGER,
	CONSTRAINT PK_DRIVERAPPEND PRIMARY KEY (id),
 	CONSTRAINT FK_DRIVER_APPEND FOREIGN KEY (driver_id) REFERENCES driver (id)

)ENGINE=InnoDB CHARSET=utf8mb4;
-------------------------------------------------------------------

