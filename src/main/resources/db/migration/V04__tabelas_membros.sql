/*
	Autor: José Zeck Alonso

	Segunda versão do flyway, destinada a excluir a tabela de teste do flyway  e criar as tabelas cadastrais
*/
-- MEMBROS
CREATE TABLE IF NOT EXISTS member_association(

	id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
	name VARCHAR(100) NOT NULL,
	photo VARCHAR(500),
	cpf VARCHAR(11) NOT NULL,
	rg VARCHAR(20) NOT NULL,
	email VARCHAR(100),
	phone1 VARCHAR(20),
	phone2 VARCHAR(20),
	birth_date DATE,
	zip_code VARCHAR(9) NOT NULL,
	city VARCHAR(100) NOT NULL,
	complement VARCHAR(100),
	number_house INTEGER NOT NULL,
	uf CHAR(2),
	create_date DATE,
	update_date DATE,
	create_user VARCHAR(100) NOT NULL,
	update_user VARCHAR(100),
	role VARCHAR(100) NOT NULL,
	active BOOLEAN,
	district_id INTEGER,
	street_id INTEGER,
	CONSTRAINT PK_MEMBER PRIMARY KEY (id),
	CONSTRAINT FK_DISTRICT_MEMBER FOREIGN KEY (district_id) REFERENCES district (id),
	CONSTRAINT FK_STREEET_MEMBER FOREIGN KEY (street_id) REFERENCES street (id)

)ENGINE=InnoDB CHARSET=utf8mb4;

-------------------------------------------------------------------


-- ANEXO MEMBROS
CREATE TABLE IF NOT EXISTS member_append (

	id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
	description VARCHAR(500) NOT NULL,
	append VARCHAR(500) NOT NULL,
	create_date DATE,
	update_date DATE,
	create_user VARCHAR(100) NOT NULL,
	update_user VARCHAR(100),
	member_id INTEGER,
	CONSTRAINT PK_MEMBERAPPEND PRIMARY KEY (id),
	CONSTRAINT FK_MEMBER_APPEND FOREIGN KEY (member_id) REFERENCES member_association (id)

)ENGINE=InnoDB CHARSET=utf8mb4;
-------------------------------------------------------------------


-- VEICULOS
CREATE TABLE IF NOT EXISTS vechile(

	id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
	name VARCHAR(100) NOT NULL,
	identification VARCHAR(20) NOT NULL,
	description VARCHAR(500) NOT NULL,
	model VARCHAR(100) NOT NULL,
	km_l INTEGER,
	bench INTEGER,
	active BOOLEAN,
	create_date DATE,
	update_date DATE,
	create_user VARCHAR(100) NOT NULL,
	update_user VARCHAR(100),
	CONSTRAINT PK_VECHILE PRIMARY KEY (id)

)ENGINE=InnoDB CHARSET=utf8mb4;


-------------------------------------------------------------------


-- ANEXO VEICULOS
CREATE TABLE IF NOT EXISTS vechile_append (

	id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
	description VARCHAR(500) NOT NULL,
	append VARCHAR(500) NOT NULL,
	create_date DATE,
	update_date DATE,
	create_user VARCHAR(100) NOT NULL,
	update_user VARCHAR(100),
	vechile_id INTEGER,
	CONSTRAINT PK_VECHILEAPPEND PRIMARY KEY (id),
	CONSTRAINT FK_VECHILE_APPEND FOREIGN KEY (vechile_id) REFERENCES vechile (id)

)ENGINE=InnoDB CHARSET=utf8mb4;
-------------------------------------------------------------------


-- PREFERENCIAS
CREATE TABLE IF NOT EXISTS preferences(

	id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
	email VARCHAR(100),
	km_min DECIMAL(10,2),
	km_max DECIMAL(10,2),
	hour_out TIME,
	hour_regress TIME,
	hour_coming TIME,
	locale_out VARCHAR(100),
	locale_regress VARCHAR(100),
	ammount DECIMAL(10,2),
	contract_value DECIMAL(10,2),
	contract_recission DECIMAL(10,2),
	contribution_bank DECIMAL(10,2),
	communication_rate DECIMAL(10,2),
	communication_rate_date INTEGER,
	penalty DECIMAL(10,2),
	calculation_type VARCHAR(100),
	expiration_date INTEGER,
	notification_create BOOLEAN,
	notification_end BOOLEAN,
	notification_stats BOOLEAN,
	create_date DATE,
	update_date DATE,
	create_user VARCHAR(100) NOT NULL,
	update_user VARCHAR(100),
	CONSTRAINT PK_PREFERENCES PRIMARY KEY (id)
	
)ENGINE=InnoDB CHARSET=utf8mb4;
-------------------------------------------------------------------


-- AUXILIO COMBUSTIVEL DA PREFEITURA
CREATE TABLE IF NOT EXISTS 	support_cityhall(

	id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
	ammount DECIMAL(10,2) NOT NULL,
	description VARCHAR(500) NOT NULL,
	mounth_reference INTEGER,
	create_date DATE,
	update_date DATE,
	create_user VARCHAR(100) NOT NULL,
	update_user VARCHAR(100),
	CONSTRAINT PK_SUPPORTCITYHALL PRIMARY KEY (id)
	
)ENGINE=InnoDB CHARSET=utf8mb4;
-------------------------------------------------------------------


-- ANEXO AUXILIO COMBUSTIVEL DA PREFEIRUTRA
CREATE TABLE IF NOT EXISTS support_cityhall_append (

	id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
	description VARCHAR(500) NOT NULL,
	append VARCHAR(500) NOT NULL,
	create_date DATE,
	update_date DATE,
	create_user VARCHAR(100) NOT NULL,
	update_user VARCHAR(100),
	support_id INTEGER,
	CONSTRAINT PK_SUPPORTCITYHALLAPPEND PRIMARY KEY (id),
	CONSTRAINT FK_SUPPORT_CITYHALL_APPEND FOREIGN KEY (support_id) REFERENCES support_cityhall (id)

)ENGINE=InnoDB CHARSET=utf8mb4;
-------------------------------------------------------------------


-- CAIXA
CREATE TABLE IF NOT EXISTS bank(

	id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
	sale DECIMAL(10,2) NOT NULL,
	exercise_bank VARCHAR(4) NOT NULL,
	active BOOLEAN,
	create_date DATETIME,
	update_date DATETIME,
	create_user VARCHAR(100) NOT NULL,
	update_user VARCHAR(100),
	CONSTRAINT PK_BANK PRIMARY KEY (id)

)ENGINE=InnoDB CHARSET=utf8mb4;
-------------------------------------------------------------------


-- HISTORICO DE CAIXA
CREATE TABLE IF NOT EXISTS history_bank(

	id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
	id_transaction INTEGER NOT NULL,
	type_transaction VARCHAR(100) NOT NULL,
	protocol_transaction VARCHAR(100) NOT NULL,
	amount_transaction DECIMAL(10,2) NOT NULL,
	exercise_bank VARCHAR(4) NOT NULL,
	sale_old DECIMAL(10,2) NOT NULL,
	sale_new DECIMAL(10,2) NOT NULL,
	create_date DATETIME,
	update_date DATETIME,
	create_user VARCHAR(100) NOT NULL,
	update_user VARCHAR(100),
	bank_id INTEGER NOT NULL,
	CONSTRAINT PK_HISTORY PRIMARY KEY (id),
	CONSTRAINT FK_BANK_HISTORY FOREIGN KEY (bank_id) REFERENCES bank (id)

)ENGINE=InnoDB CHARSET=utf8mb4;
-------------------------------------------------------------------


-- RECEITAS
CREATE TABLE IF NOT EXISTS recipe(

	id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
	name VARCHAR (100) NOT NULL UNIQUE,
	description VARCHAR(500) NOT NULL,
	create_date DATE,
	update_date DATE,
	create_user VARCHAR(100) NOT NULL,
	update_user VARCHAR(100),
	CONSTRAINT PK_RECIPE PRIMARY KEY (id)

)ENGINE=InnoDB CHARSET=utf8mb4;
-------------------------------------------------------------------


-- ENTRADA
CREATE TABLE IF NOT EXISTS input(

	id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
	type_transaction VARCHAR(100) NOT NULL,
	amount_transaction DECIMAL(10,2) NOT NULL,
	protocol_transaction VARCHAR(100) NOT NULL,
	description VARCHAR(500) NOT NULL,
	create_date DATETIME,
	update_date DATETIME,
	create_user VARCHAR(100) NOT NULL,
	update_user VARCHAR(100),
	recipe_id INTEGER NOT NULL,
	CONSTRAINT PK_INPUT PRIMARY KEY (id),
	CONSTRAINT FK_RECIPE_INPUT FOREIGN KEY (recipe_id) REFERENCES recipe (id)

)ENGINE=InnoDB CHARSET=utf8mb4;
-------------------------------------------------------------------


-- SAIDA
CREATE TABLE IF NOT EXISTS output(

	id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
	type_transaction VARCHAR(100) NOT NULL,
	amount_transaction DECIMAL(10,2) NOT NULL,
	protocol_transaction VARCHAR(100) NOT NULL,
	description VARCHAR(500) NOT NULL,
	create_date DATETIME,
	update_date DATETIME,
	create_user VARCHAR(100) NOT NULL,
	update_user VARCHAR(100),
	recipe_id INTEGER NOT NULL,
	CONSTRAINT PK_OUTPUT PRIMARY KEY (id),
	CONSTRAINT FK_RECIPE_OUTPUT FOREIGN KEY (recipe_id) REFERENCES recipe (id)

)ENGINE=InnoDB CHARSET=utf8mb4;
-------------------------------------------------------------------

