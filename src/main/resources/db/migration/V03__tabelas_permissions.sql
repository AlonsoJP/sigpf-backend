/*
	Autor: José Zeck Alonso

	Segunda versão do flyway, destinada a excluir a tabela de teste do flyway  e criar as tabelas cadastrais
*/
-- PERMISSÃO
CREATE TABLE IF NOT EXISTS permission(

	id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
	description VARCHAR(50) NOT NULL,
	CONSTRAINT PK_PERMISSION PRIMARY KEY (id)
	
)ENGINE=InnoDB CHARSET=utf8mb4;
-------------------------------------------------------------------

-- USUARIO/PERMISSAO
CREATE TABLE IF NOT EXISTS user_permission (

	user_id INTEGER NOT NULL,
	permission_id INTEGER NOT NULL,
	CONSTRAINT PK_USERPERMISSION PRIMARY KEY (user_id, permission_id),
	CONSTRAINT FK_USER_USERPERMISSION FOREIGN KEY (user_id) REFERENCES users (id),
	CONSTRAINT FK_PERMISSION_USERPERMISSION FOREIGN KEY (permission_id) REFERENCES permission (id)
	

)ENGINE=InnoDB CHARSET=utf8mb4;
-------------------------------------------------------------------