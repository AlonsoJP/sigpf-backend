
/*
	Autor: José Zeck Alonso

	Versão 5 do flyway. 
	
	Descrição: Inserir tabela de versionamento do sistema, com id, descrição, usuário e data de criação.
*/


-- CRIAÇÃO DA TABELA VERSÃO
CREATE TABLE IF NOT EXISTS version(

	id INTEGER NOT NULL UNIQUE auto_increment,
    description VARCHAR(500),
    version VARCHAR (10),
    create_dev VARCHAR(100),
    create_date_time DATETIME,

	CONSTRAINT PK_VERSION primary key (id)
)ENGINE=InnoDB CHARSET=utf8mb4;

-------------------------------------------------------------------

-- 1.2.1
-- VERSÃO DO SISTEMA 1
-- VERSÃO DE SERVIÇO 2
-- VERSÃO DE CORREÇÃO 1

INSERT INTO version () VALUES (

	0, "Inserção de versionamento do sistema, presente na rodapé. Exemplo: (Versão do Sistema . Versão do Serviço . Versão de correção", "1.2.1", "zeck", current_timestamp()

);

