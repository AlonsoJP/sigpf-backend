package br.com.sigpf.entity;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "university")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class UniversityEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "name", nullable = false, length = 100)
	@NotBlank
	private String name;

	@Column(name = "cnpj", nullable = false, length = 14)
	@NotBlank
	private String cnpj;

	@Column(name = "email", length = 100)
	@NotBlank
	private String email;

	@Column(name = "phone", length = 20)
	private String phone;

	@Column(name = "zip_code", nullable = false, length = 9)
	@Size(min = 8, max = 8)
	private String zip_code;

	@Column(name = "city", nullable = false, length = 100)
	@Size(min = 5, max = 100)
	@NotBlank
	private String city;

	@Column(name = "complement", length = 100)
	@Size(max = 100)
	private String complement;

	@Column(name = "number_house", nullable = false)
	private Integer number_house;

	@Column(name = "uf", nullable = false, length = 2)
	@Size(min = 2, max = 2)
	private String uf;

	@Column(name = "create_date", updatable = false, nullable = false)
	@CreationTimestamp
	private LocalDate createDate;

	@Column(name = "update_date", insertable = false)
	@CreationTimestamp
	private LocalDate updateDate;

	@Column(name = "create_user", updatable = false, nullable = false)
	private String createUser;

	@Column(name = "update_user")
	private String updateUser;

	@ManyToOne
	@JoinColumn(name = "district_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_DISTRICT_ADDRESS"))
	private DistrictEntity district;

	@ManyToOne
	@JoinColumn(name = "street_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_STREET_ADDRESS"))
	private StreetEntity street;

	@OneToMany(mappedBy = "university", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<UniversityAppendEntity> universityAppend;

}
