package br.com.sigpf.entity;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;

import br.com.sigpf.enums.RoleTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "member_association")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class MemberAssociationEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "name", nullable = false, length = 100)
	@Size(min = 5, max = 100)
	@NotBlank
	private String name;

	@Column(name = "photo", nullable = true, length = 500)
	private String photo;

	@Column(name = "cpf", nullable = false, length = 11)
	@Size(min = 11, max = 11)
	@NotBlank
	private String cpf;

	@Column(name = "rg", nullable = false, length = 20)
	@NotBlank
	private String rg;

	@Column(name = "email", nullable = false, length = 100)
	@Size(min = 10, max = 100)
	@NotBlank
	private String email;

	@Column(name = "phone1", nullable = true, length = 20)
	private String phone1;

	@Column(name = "phone2", nullable = true, length = 20)
	private String phone2;

	@Column(name = "birth_date", insertable = true, updatable = true)
	private LocalDate birth_date;

	@Column(name = "zip_code", nullable = false, length = 9)
	@Size(min = 8, max = 8)
	private String zip_code;

	@Column(name = "city", nullable = false, length = 100)
	@Size(min = 5, max = 100)
	@NotBlank
	private String city;

	@Column(name = "complement", length = 100)
	@Size(max = 100)
	private String complement;

	@Column(name = "number_house", nullable = false)
	private String number_house;

	@Column(name = "uf", nullable = false, length = 2)
	@Size(min = 2, max = 2)
	private String uf;

	@Column(name = "create_date", updatable = false, nullable = false)
	@CreationTimestamp
	private LocalDate createDate;

	@Column(name = "update_date", insertable = false)
	@CreationTimestamp
	private LocalDate updateDate;

	@Column(name = "create_user", updatable = false, nullable = false)
	private String createUser;

	@Column(name = "update_user")
	private String updateUser;

	@Enumerated(EnumType.STRING)
	private RoleTypeEnum role;

//	@Enumerated(EnumType.STRING)
//	private SubRoleTypeEnum sub_role;

	@Column(name = "active")
	private Boolean active;

	@ManyToOne
	@JoinColumn(name = "district_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_DISTRICT_MEMBER"))
	private DistrictEntity district;

	@ManyToOne
	@JoinColumn(name = "street_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_STREEET_MEMBER"))
	private StreetEntity street;

	@OneToMany(mappedBy = "member_association", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<MemberAssociationAppendEntity> append;

}
