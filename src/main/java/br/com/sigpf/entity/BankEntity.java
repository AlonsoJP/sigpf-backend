package br.com.sigpf.entity;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "bank")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class BankEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "sale", nullable = false)
	private Double sale;

	@Column(name = "exercise_bank", nullable = false, length = 4)
	@Size(min = 4, max = 4)
	@NotBlank
	private String exercise_bank;

	@Column(name = "create_date", updatable = false, nullable = false)
//	@CreationTimestamp
//	@Temporal(TemporalType.TIMESTAMP)
	private LocalDateTime createDate;

	@Column(name = "update_date", insertable = false)
//	@CreationTimestamp
//	@Temporal(TemporalType.TIMESTAMP)
	private LocalDateTime updateDate;

	@Column(name = "create_user", updatable = false, nullable = false)
	private String createUser;

	@Column(name = "update_user")
	private String updateUser;

	@Column(name = "active")
	private Boolean active;

	@OneToMany(mappedBy = "bank", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<HistoryBankEntity> append;

}
