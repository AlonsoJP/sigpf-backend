package br.com.sigpf.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import br.com.sigpf.enums.TypeTransactionEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "history_bank")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class HistoryBankEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "id_transaction", nullable = false)
	private Long id_transaction;

	@Column(name = "type_transaction", nullable = false, length = 100)
	@Enumerated(EnumType.STRING)
	private TypeTransactionEnum type_transaction;

	@Column(name = "protocol_transaction", nullable = false, length = 100)
	private String protocol_transaction;

	@Column(name = "amount_transaction", nullable = false)
	private Double amount_transaction;

	@Column(name = "exercise_bank", nullable = false, length = 4)
	@Size(min = 4, max = 4)
	@NotBlank
	private String exercise_bank;

	@Column(name = "sale_old", nullable = false)
	private Double sale_old;

	@Column(name = "sale_new", nullable = false)
	private Double sale_new;

	@Column(name = "create_date", updatable = false, nullable = false)
//	@CreationTimestamp
//	@Temporal(TemporalType.TIMESTAMP)
	private LocalDateTime createDate;

	@Column(name = "update_date", insertable = false)
//	@CreationTimestamp
//	@Temporal(TemporalType.TIMESTAMP)
	private LocalDateTime updateDate;

	@Column(name = "create_user", updatable = false)
	private String createUser;

	@Column(name = "update_user")
	private String updateUser;

	@ManyToOne
	@JoinColumn(name = "bank_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_BANK_HISTORY"))
	private BankEntity bank;

}
