package br.com.sigpf.entity;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "users")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class UserEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	@NotNull
	@Size(min = 3, max = 150)
	private String name;

	@Column(name = "email")
	@NotNull
	@Size(min = 7, max = 100)
	private String email;

	@Column(name = "photo")
	private String photo;

	@Column(name = "user_name")
	@NotNull
	private String user_name;

	@Column(name = "document")
	@NotNull
	private String document;

	@Column(name = "password")
	@NotNull
	@JsonIgnore
	private String password;

	@Column(name = "create_date", updatable = false)
	@CreationTimestamp
	@NotNull
	private LocalDate createDate;

	@Column(name = "update_date", insertable = false)
	@CreationTimestamp
	private LocalDate updateDate;

	@Column(name = "create_user", updatable = false)
	@NotNull
	private String createUser;

	@Column(name = "update_user")
	private String updateUser;

	@Column(name = "active")
	private Boolean active;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "user_permission", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "permission_id"))
	@JsonIgnore
	private List<PermissionEntity> permission;

}
