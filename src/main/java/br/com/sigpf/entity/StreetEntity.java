package br.com.sigpf.entity;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "street")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class StreetEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "name_street", nullable = false, length = 100)
	@NotNull
	@Size(min = 5, max = 100)
	private String name_street;

	@Column(name = "create_date", updatable = false)
	@CreationTimestamp
	@NotNull
	private LocalDate createDate;

	@Column(name = "update_date", insertable = false)
	@UpdateTimestamp
	private LocalDate updateDate;

	@Column(name = "create_user", updatable = false)
	@NotNull
	private String createUser;

	@Column(name = "update_user")
	private String updateUser;

	@OneToMany(mappedBy = "street", fetch = FetchType.LAZY)
	private List<StudentEntity> student;

	@OneToMany(mappedBy = "street", fetch = FetchType.LAZY)
	private List<DriverEntity> driver;

	@OneToMany(mappedBy = "street", fetch = FetchType.LAZY)
	private List<CompanyEntity> company;

	@OneToMany(mappedBy = "street", fetch = FetchType.LAZY)
	private List<UniversityEntity> university;

	@OneToMany(mappedBy = "street", fetch = FetchType.LAZY)
	private List<AssociationEntity> association;

	@OneToMany(mappedBy = "street", fetch = FetchType.LAZY)
	List<MemberAssociationEntity> member;
}