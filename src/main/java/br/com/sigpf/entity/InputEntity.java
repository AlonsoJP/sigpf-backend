package br.com.sigpf.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import br.com.sigpf.enums.TypeTransactionEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "input")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class InputEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "type_transaction", nullable = false, length = 100)
	@Enumerated(EnumType.STRING)
	private TypeTransactionEnum type_transaction;

	@Column(name = "amount_transaction", nullable = false)
	private Double amount_transaction;

	@Column(name = "protocol_transaction", nullable = true, length = 100)
	private String protocol_transaction;

	@Column(name = "description", nullable = false, length = 500)
	private String description;

	@Column(name = "create_date", updatable = false, nullable = false)
//	@CreationTimestamp
//	@Temporal(TemporalType.TIMESTAMP)
	private LocalDateTime createDate;

	@Column(name = "update_date", insertable = false)
	@CreationTimestamp
//	@Temporal(TemporalType.TIMESTAMP)
	private LocalDateTime updateDate;

	@Column(name = "create_user", updatable = false)
	private String createUser;

	@Column(name = "update_user")
	private String updateUser;

	@ManyToOne
	@JoinColumn(name = "recipe_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_RECIPE_INPUT"))
	private RecipeEntity recipe;

}
