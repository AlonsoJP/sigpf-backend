package br.com.sigpf.entity;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "course")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class CourseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "name_course", nullable = false, length = 100)
	@Size(min = 3, max = 100)
	private String name;

	@Column(name = "create_date", updatable = false, nullable = false)
	@CreationTimestamp
	private LocalDate createDate;

	@Column(name = "update_date", insertable = false)
	@CreationTimestamp
	private LocalDate updateDate;

	@Column(name = "create_user", updatable = false, nullable = false)
	private String createUser;

	@Column(name = "update_user")
	private String updateUser;

	@OneToMany(mappedBy = "course", fetch = FetchType.LAZY)
	private List<StudentEntity> student;
}
