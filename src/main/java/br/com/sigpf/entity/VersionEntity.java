package br.com.sigpf.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "version")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class VersionEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "description", nullable = false, length = 500)
	@Size(max = 500)
	@NotBlank
	private String description;

	@Column(name = "version", nullable = false, length = 10)
	@NotBlank
	private String version;

	@Column(name = "create_dateTime", updatable = false)
	@JsonFormat(pattern = "YYYY-MM-dd HH:mm:ss")
	private LocalDateTime createDate;

	@Column(name = "create_dev", nullable = false, length = 100)
	private String createDev;

}
