package br.com.sigpf.entity;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "recipe")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class RecipeEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "name", nullable = false, length = 100)
	private String name;

	@Column(name = "description", nullable = false, length = 500)
	private String description;

//	@Column(name = "type_recipe", nullable = false)
//	@Enumerated(EnumType.STRING)
//	private TypeTransactionEnum type_recipe;

	@Column(name = "create_date", updatable = false, nullable = false)
	@CreationTimestamp
	private LocalDate createDate;

	@Column(name = "update_date", insertable = false)
	@CreationTimestamp
	private LocalDate updateDate;

	@Column(name = "create_user", updatable = false)
	private String createUser;

	@Column(name = "update_user")
	private String updateUser;

	@OneToMany(mappedBy = "recipe", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<InputEntity> input;

	@OneToMany(mappedBy = "recipe", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<OutputEntity> output;

}
