package br.com.sigpf.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "student_append")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class StudentAppendEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "description", nullable = false, length = 500)
	private String description;

	@Column(name = "append", nullable = false, length = 500)
	private String path;

	@Column(name = "create_date", updatable = false, nullable = false)
	@CreationTimestamp
	private LocalDate createDate;

	@Column(name = "update_date", insertable = false)
	@CreationTimestamp
	private LocalDate updateDate;

	@Column(name = "create_user", updatable = false)
	private String createUser;

	@Column(name = "update_user")
	private String updateUser;

	@ManyToOne
	@JoinColumn(name = "student_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_STUDENT_STUDENTAPPEND"))
	private StudentEntity student;

}
