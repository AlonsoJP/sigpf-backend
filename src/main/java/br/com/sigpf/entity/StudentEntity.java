package br.com.sigpf.entity;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import br.com.sigpf.enums.PaymentTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "student")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class StudentEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "name", nullable = false, length = 100)
	@Size(min = 5, max = 100)
	@NotBlank
	private String name;

	@Column(name = "photo", nullable = true, length = 500)
	private String photo;

	@Column(name = "cpf", nullable = false, length = 11)
	@Size(min = 11, max = 11)
	@NotBlank
	private String cpf;

	@Column(name = "rg", nullable = false, length = 20)
	@NotBlank
	private String rg;

	@Column(name = "ra", nullable = true, length = 20)
	@NotBlank
	private String ra;

	@Column(name = "email", nullable = false, length = 100)
	@Size(min = 10, max = 100)
	@NotBlank
	private String email;

	@Column(name = "phone1", nullable = true, length = 20)
	private String phone1;

	@Column(name = "phone2", nullable = true, length = 20)
	private String phone2;

	@Column(name = "birth_date", insertable = true, updatable = true)
	private LocalDate birth_date;

	@Column(name = "user_student", nullable = true, length = 50)
	private String user_student;

	@Column(name = "zip_code", nullable = false, length = 9)
	@Size(min = 8, max = 8)
	private String zip_code;

	@Column(name = "city", nullable = false, length = 100)
	@Size(min = 5, max = 100)
	@NotBlank
	private String city;

	@Column(name = "complement", length = 100)
	@Size(max = 100)
	private String complement;

	@Column(name = "number_house", nullable = false)
	private Integer number_house;

	@Column(name = "uf", nullable = false, length = 2)
	@Size(min = 2, max = 2)
	private String uf;

	@Column(name = "create_date", updatable = false, nullable = false)
	private LocalDate createDate;

	@Column(name = "update_date", insertable = false)
	private LocalDate updateDate;

	@Column(name = "create_user", updatable = false, nullable = false)
	private String createUser;

	@Column(name = "update_user")
	private String updateUser;

	@Column(name = "payment_type", nullable = false)
	@Enumerated(EnumType.STRING)
	private PaymentTypeEnum paymentType;

	@Column(name = "active")
	private Boolean active;

	@ManyToOne
	@JoinColumn(name = "district_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_DISTRICT_STUDENT"))
	private DistrictEntity district;

	@ManyToOne
	@JoinColumn(name = "street_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_STREET_STUDENT"))
	private StreetEntity street;

	@ManyToOne
	@JoinColumn(name = "course_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_COURSE_STUDENT"))
	private CourseEntity course;

	@OneToMany(mappedBy = "student", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<StudentAppendEntity> append;

}
