package br.com.sigpf.entity;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import br.com.sigpf.enums.PaymentTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "preferences")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class PreferencesEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "email", nullable = false, length = 100)
	@Size(min = 10, max = 100)
	@NotBlank
	private String email;

	@Column(name = "km_min", nullable = false)
	private Double kmMin;

	@Column(name = "km_max", nullable = false)
	private Double kmMax;

	@Column(name = "hour_out")
	private LocalTime hourOut;

	@Column(name = "hour_regress")
	private LocalTime hourRegress;

	@Column(name = "hour_coming")
	private LocalTime hourComing;

	@Column(name = "locale_out")
	private String localeOut;

	@Column(name = "locale_regress")
	private String localeRegress;

	@Column(name = "ammount")
	private Double ammount;

	@Column(name = "contract_value")
	private Double contractValue;

	@Column(name = "contract_recission")
	private Double contractRecission;

	@Column(name = "contribution_bank")
	private Double contributionBank;

	@Column(name = "communication_rate")
	private Double communicationRate;

	@Column(name = "communication_rate_date")
	private Integer communicationRateDate;

	@Column(name = "penalty")
	private Double penality;

	@Column(name = "calculation_type", nullable = false, length = 100)
	@Enumerated(EnumType.STRING)
	private PaymentTypeEnum calculationType;

	@Column(name = "expiration_date") // TODO alterar para integer no banco
	private Integer expirationDate;

//	@Column(name = "expiration_contract") // TODO precisa ser removido, cada contrato tem sua expiração
//	private Date expirationContract;

	@Column(name = "notification_end")
	private Boolean notificationEndMounth;

	@Column(name = "notification_create")
	private Boolean notificationCreateMounth;

	@Column(name = "notification_stats") // TODO remover, não vai ser necessário
	private Boolean notificationStatsSystem;

	@Column(name = "create_date", updatable = false)
	private LocalDate createDate;

	@Column(name = "update_date", insertable = false)

	private LocalDate updateDate;

	@Column(name = "create_user", updatable = false, nullable = false)
	private String createUser;

	@Column(name = "update_user")
	private String updateUser;

}
