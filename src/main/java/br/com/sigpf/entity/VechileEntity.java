package br.com.sigpf.entity;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "vechile")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class VechileEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "name", nullable = false, length = 100)
	@Size(min = 5, max = 100)
	@NotBlank
	private String name;

	@Column(name = "identification", nullable = false, length = 100)
	@Size(min = 5, max = 100)
	@NotBlank
	private String identification;

	@Column(name = "description", nullable = false, length = 500)
	private String description;

	@Column(name = "model", length = 100)
	private String model;

	@Column(name = "km_l")
	private Integer km_l;

	@Column(name = "bench")
	private Integer bench;

	@Column(name = "create_date", updatable = false, nullable = false)
	@CreationTimestamp
	private LocalDate createDate;

	@Column(name = "update_date", insertable = false)
	@CreationTimestamp
	private LocalDate updateDate;

	@Column(name = "create_user", updatable = false, nullable = false)
	private String createUser;

	@Column(name = "update_user")
	private String updateUser;

	@Column(name = "active")
	private Boolean active;

	@OneToMany(mappedBy = "vechile", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<VechileAppendEntity> append;

}
