package br.com.sigpf.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Component;

@Component
public class Others {

	public String castLocalDateString(LocalDate localDate) {

		return localDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));

	}

}
