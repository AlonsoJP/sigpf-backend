package br.com.sigpf.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Component;

import br.com.sigpf.service.user.UserService;

@Component
public class GetInfoToken {

	@Autowired
	UserService service;

	public String opentToken(OAuth2Authentication auth) {

		return auth.getPrincipal().toString().trim();

	}

}
