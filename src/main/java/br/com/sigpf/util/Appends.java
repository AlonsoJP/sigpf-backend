package br.com.sigpf.util;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import org.springframework.web.multipart.MultipartFile;

public class Appends {

	public static String saveDocuments(MultipartFile document, String root, String path) {
		Path directoryPath = Paths.get(root, path);
		Path filePath = directoryPath.resolve(unicName(document.getOriginalFilename()));

		String finalPath = filePath.toString();

		try {

			Files.createDirectories(directoryPath);// criar diretório caso não exista
			document.transferTo(filePath.toFile());

			return finalPath;

		} catch (Exception e) {
			e.getCause().getMessage();
			return e.getMessage();
		}
	}

	// gerador de nome único
	public static String unicName(String originalName) {
		return UUID.randomUUID().toString().concat("_" + originalName);
	}

	public static void deleteDocuments(String root, String path) {

		Path directoryPath = Paths.get(root, path.substring(18));
		System.out.print(directoryPath);

		try {

			Files.deleteIfExists(directoryPath);
		} catch (Exception e) {
			e.getCause().getMessage();

		}
	}

}
