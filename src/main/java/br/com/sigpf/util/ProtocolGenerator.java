package br.com.sigpf.util;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.springframework.stereotype.Component;

@Component
public class ProtocolGenerator {

	public String generateProtocol() {

		String protocol = String.valueOf(LocalDate.now().getYear()) + String.valueOf(LocalDate.now().getMonthValue())
				+ String.valueOf(LocalDate.now().getDayOfYear()) + String.valueOf(LocalDate.now().getDayOfMonth())
				+ String.valueOf(LocalDateTime.now().getHour() + LocalDateTime.now().getMinute())
				+ String.valueOf(LocalDateTime.now().getSecond());

		return protocol;
	}

}
