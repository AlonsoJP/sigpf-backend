package br.com.sigpf.util;

import java.text.DecimalFormat;

public class Validation {

	public static Double ajusarDecimal(Double valor) {

		DecimalFormat df = new DecimalFormat("##,###,###.00");

		return Double.parseDouble(df.format(valor));
	}

}
