package br.com.sigpf.util;

import io.micrometer.core.instrument.util.StringUtils;

public class ValidationAppends {

	Boolean valid;

	public Boolean validationImage(String name) {

		nullable(name);
		verifyJpg(name);

		return valid;

	}

	public Boolean validationDocuments(String name) {
		nullable(name);
		//verifyTxt(name);

		return valid;
	}

	public void nullable(String name) {

		if (StringUtils.isEmpty(name) || name.trim().isEmpty() || name.trim() == null) {
			valid = false;
			throw new NullPointerException("Nenhum arquivo foi inserido ");
		}

	}

	public void verifyJpg(String name) {

		if ((!name.endsWith(".jpg"))) {
			valid = false;
			throw new RuntimeException("Verifique a extensão do arquivo. Somente JPG.");

		}

	}

	public void verifyPdf(String name) {

		if ((!name.endsWith(".pdf"))) {
			valid = false;
			throw new RuntimeException("Verifique a extensão do arquivo.");

		}

	}

	public void verifyWord(String name) {

		if ((!name.endsWith(".docx"))) {
			valid = false;
			throw new RuntimeException("Verifique a extensão do arquivo.");

		}

	}

	public void verifyExcel(String name) {

		if ((!name.endsWith(".xls"))) {
			valid = false;
			throw new RuntimeException("Verifique a extensão do arquivo.");

		}

	}

	public void verifyTxt(String name) {

		if ((!name.endsWith(".txt"))) {
			valid = false;
			throw new RuntimeException("Verifique a extensão do arquivo.");

		}

	}

}
