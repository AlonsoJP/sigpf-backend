package br.com.sigpf.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class ReportGenerator {

	public byte[] report(String path, Map<String, Object> parameters, List<?> list) throws Exception {

		try {
			InputStream input = new FileInputStream(path);
			JRBeanCollectionDataSource collection = new JRBeanCollectionDataSource(list);

			JasperPrint jasperPrint = JasperFillManager.fillReport(input, parameters, collection);
			return JasperExportManager.exportReportToPdf(jasperPrint);
		} catch (FileNotFoundException e) {
			throw new Exception(e.getMessage());
		}

	}

	public Map<String, Object> parameters() {

		Map<String, Object> parameters = new HashMap<>();
		parameters.put("REPORT_LOCALE", new Locale("pt", "BR"));

		return parameters;
	}

}
