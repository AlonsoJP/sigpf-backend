package br.com.sigpf.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.sigpf.entity.MemberAssociationEntity;
import br.com.sigpf.enums.RoleTypeEnum;
import br.com.sigpf.resource.MemberAssociationResource;
import lombok.var;

public class MemberAssociationConverter {

	public static MemberAssociationResource toDto(final MemberAssociationEntity entities) {

		if (entities != null) {
			final var dto = new MemberAssociationResource();

			dto.setId(entities.getId());
			dto.setName(entities.getName());
			dto.setPhoto(entities.getPhoto());
			dto.setCpf(entities.getCpf());
			dto.setRg(entities.getRg());
			dto.setEmail(entities.getEmail());
			dto.setPhone1(entities.getPhone1());
			dto.setPhone2(entities.getPhone2());
			dto.setBirthDate(entities.getBirth_date());
			dto.setZipCode(entities.getZip_code());
			dto.setCity(entities.getCity());
			dto.setComplement(entities.getComplement());
			dto.setNumberHouse(entities.getNumber_house());
			dto.setUf(entities.getUf());
			dto.setRole(entities.getRole().getDescricao());
			dto.setActive(entities.getActive());
			dto.setCreateDate(entities.getCreateDate());
			dto.setUpdateDate(entities.getUpdateDate());
			dto.setCreateUser(entities.getCreateUser());
			dto.setUpdateUser(entities.getUpdateUser());
			dto.setDistrict(DistrictConverter.toDto(entities.getDistrict()));
			dto.setStreet(StreetConverter.toDto(entities.getStreet()));

			return dto;
		} else {
			return null;
		}
	}

	public static MemberAssociationEntity toEntity(final MemberAssociationResource dto) {

		if (dto != null) {
			var entities = new MemberAssociationEntity();

			entities.setId(dto.getId());
			entities.setName(dto.getName());
			entities.setPhoto(dto.getPhoto());
			entities.setCpf(dto.getCpf());
			entities.setRg(dto.getRg());
			entities.setEmail(dto.getEmail());
			entities.setPhone1(dto.getPhone1());
			entities.setPhone2(dto.getPhone2());
			entities.setBirth_date(dto.getBirthDate());
			entities.setZip_code(dto.getZipCode());
			entities.setCity(dto.getCity());
			entities.setComplement(dto.getComplement());
			entities.setNumber_house(dto.getNumberHouse());
			entities.setUf(dto.getUf());
			entities.setRole(RoleTypeEnum.getByDescricao(dto.getRole()));
			entities.setActive(dto.getActive());
			entities.setCreateDate(dto.getCreateDate());
			entities.setUpdateDate(dto.getUpdateDate());
			entities.setCreateUser(dto.getCreateUser());
			entities.setUpdateUser(dto.getUpdateUser());
			entities.setDistrict(DistrictConverter.toEntity(dto.getDistrict()));
			entities.setStreet(StreetConverter.toEntity(dto.getStreet()));

			return entities;
		} else {

			return null;

		}

	}

	public static MemberAssociationResource toDto(final Optional<MemberAssociationEntity> optional) {
		return optional.isPresent() ? toDto(optional.get()) : null;
	}

	public static Optional<MemberAssociationResource> toOptionalDto(final Optional<MemberAssociationEntity> optional) {
		return Optional.ofNullable(toDto(optional));
	}

	public static List<MemberAssociationResource> listToDto(final List<MemberAssociationEntity> entities) {
		List<MemberAssociationResource> listResource = new ArrayList<MemberAssociationResource>();

		entities.forEach(address -> {
			listResource.add(toDto(address));
		});

		return listResource;
	}

}
