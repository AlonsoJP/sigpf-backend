package br.com.sigpf.converter;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.sigpf.entity.OutputEntity;
import br.com.sigpf.enums.TypeTransactionEnum;
import br.com.sigpf.resource.OutputResource;
import lombok.var;

public class OutputConverter {

	public static OutputResource toDto(final OutputEntity entities) {

		if (entities != null) {
			final var dto = new OutputResource();

			dto.setId(entities.getId());
			dto.setTypeTransaction(entities.getTypeTransaction().getDescricao());
			dto.setAmountTransaction(entities.getAmountTransaction());
			dto.setProtocolTransaction(entities.getProtocolTransaction());
			dto.setDescription(entities.getDescription());
			dto.setRecipe(RecipeConverter.toDto(entities.getRecipe()));
			dto.setCreateDate(entities.getCreateDate());
			dto.setUpdateDate(entities.getUpdateDate());
			dto.setCreateUser(entities.getCreateUser());
			dto.setUpdateUser(entities.getUpdateUser());

			return dto;
		} else {
			return null;
		}
	}

	public static OutputEntity toEntity(final OutputResource dto) {

		if (dto != null) {
			var entities = new OutputEntity();

			entities.setId(dto.getId());
			entities.setTypeTransaction(TypeTransactionEnum.getByDescricao(dto.getTypeTransaction()));
			entities.setAmountTransaction(dto.getAmountTransaction());
			entities.setProtocolTransaction(dto.getProtocolTransaction());
			entities.setDescription(dto.getDescription());
			entities.setRecipe(RecipeConverter.toEntity(dto.getRecipe()));
			entities.setCreateDate(LocalDateTime.now().atZone(ZoneId.systemDefault()).toLocalDateTime());
			entities.setUpdateDate(LocalDateTime.now().atZone(ZoneId.systemDefault()).toLocalDateTime());
			entities.setCreateUser(dto.getCreateUser());
			entities.setUpdateUser(dto.getUpdateUser());

			return entities;
		} else {

			return null;

		}

	}

	public static OutputResource toDto(final Optional<OutputEntity> optional) {
		return optional.isPresent() ? toDto(optional.get()) : null;
	}

	public static Optional<OutputResource> toOptionalDto(final Optional<OutputEntity> optional) {
		return Optional.ofNullable(toDto(optional));
	}

	public static List<OutputResource> listToDto(final List<OutputEntity> entities) {
		List<OutputResource> listResource = new ArrayList<OutputResource>();

		entities.forEach(address -> {
			listResource.add(toDto(address));
		});

		return listResource;
	}

}
