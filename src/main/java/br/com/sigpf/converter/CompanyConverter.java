package br.com.sigpf.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.sigpf.entity.CompanyEntity;
import br.com.sigpf.resource.CompanyResource;
import lombok.var;

public class CompanyConverter {

	public static CompanyResource toDto(final CompanyEntity entities) {

		if (entities != null) {
			final var dto = new CompanyResource();

			dto.setId(entities.getId());
			dto.setOwner(entities.getOwner());
			dto.setFantasy_name(entities.getFantasy_name());
			dto.setCnpj(entities.getCnpj());
			dto.setZip_code(entities.getZip_code());
			dto.setCity(entities.getCity());
			dto.setComplement(entities.getComplement());
			dto.setNumber_house(entities.getNumber_house());
			dto.setUf(entities.getUf());
			dto.setCreateDate(entities.getCreateDate());
			dto.setUpdateDate(entities.getUpdateDate());
			dto.setCreateUser(entities.getCreateUser());
			dto.setUpdateUser(entities.getUpdateUser());
			dto.setActive(entities.getActive());
			dto.setDistrict(DistrictConverter.toDto(entities.getDistrict()));
			dto.setStreet(StreetConverter.toDto(entities.getStreet()));

			return dto;
		} else {
			return null;
		}
	}

	public static CompanyEntity toEntity(final CompanyResource dto) {

		if (dto != null) {
			var entities = new CompanyEntity();

			entities.setId(dto.getId());
			entities.setOwner(dto.getOwner());
			entities.setFantasy_name(dto.getFantasy_name());
			entities.setCnpj(dto.getCnpj());
			entities.setZip_code(dto.getZip_code());
			entities.setCity(dto.getCity());
			entities.setComplement(dto.getComplement());
			entities.setNumber_house(dto.getNumber_house());
			entities.setUf(dto.getUf());
			entities.setCreateDate(dto.getCreateDate());
			entities.setUpdateDate(dto.getUpdateDate());
			entities.setCreateUser(dto.getCreateUser());
			entities.setUpdateUser(dto.getUpdateUser());
			entities.setActive(dto.getActive());
			entities.setDistrict(DistrictConverter.toEntity(dto.getDistrict()));
			entities.setStreet(StreetConverter.toEntity(dto.getStreet()));

			return entities;
		} else {

			return null;

		}

	}

	public static CompanyResource toDto(final Optional<CompanyEntity> optional) {
		return optional.isPresent() ? toDto(optional.get()) : null;
	}

	public static Optional<CompanyResource> toOptionalDto(final Optional<CompanyEntity> optional) {
		return Optional.ofNullable(toDto(optional));
	}

	public static List<CompanyResource> listToDto(final List<CompanyEntity> entities) {
		List<CompanyResource> listResource = new ArrayList<CompanyResource>();

		entities.forEach(address -> {
			listResource.add(toDto(address));
		});

		return listResource;
	}

}
