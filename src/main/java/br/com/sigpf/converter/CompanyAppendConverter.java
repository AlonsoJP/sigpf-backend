package br.com.sigpf.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.sigpf.entity.CompanyAppendEntity;
import br.com.sigpf.resource.CompanyAppendResource;
import lombok.var;

public class CompanyAppendConverter {
	public static CompanyAppendResource toDto(final CompanyAppendEntity entities) {

		if (entities != null) {
			final var dto = new CompanyAppendResource();

			dto.setId(entities.getId());
			dto.setDescription(entities.getDescription());
			dto.setPath(entities.getPath());
			dto.setCreate_date(entities.getCreateDate());
			dto.setUpdate_date(entities.getUpdateDate());
			dto.setCreateUser(entities.getCreateUser());
			dto.setUpdateUser(entities.getUpdateUser());
			dto.setCompany(CompanyConverter.toDto(entities.getCompany()));

			return dto;
		} else {
			return null;
		}
	}

	public static CompanyAppendEntity toEntity(final CompanyAppendResource dto) {

		if (dto != null) {
			var entities = new CompanyAppendEntity();

			entities.setId(dto.getId());
			entities.setDescription(dto.getDescription());
			entities.setPath(dto.getPath());
			entities.setCreateDate(dto.getCreate_date());
			entities.setUpdateDate(dto.getUpdate_date());
			entities.setCreateUser(dto.getCreateUser());
			entities.setUpdateUser(dto.getUpdateUser());
			entities.setCompany(CompanyConverter.toEntity(dto.getCompany()));

			return entities;
		} else {

			return null;

		}

	}

	public static CompanyAppendResource toDto(final Optional<CompanyAppendEntity> optional) {
		return optional.isPresent() ? toDto(optional.get()) : null;
	}

	public static Optional<CompanyAppendResource> toOptionalDto(final Optional<CompanyAppendEntity> optional) {
		return Optional.ofNullable(toDto(optional));
	}

	public static List<CompanyAppendResource> listToDto(final List<CompanyAppendEntity> entities) {
		List<CompanyAppendResource> listResource = new ArrayList<CompanyAppendResource>();

		entities.forEach(address -> {
			listResource.add(toDto(address));
		});

		return listResource;
	}
}
