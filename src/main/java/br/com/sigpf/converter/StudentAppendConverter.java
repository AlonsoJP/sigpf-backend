package br.com.sigpf.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.sigpf.entity.StudentAppendEntity;
import br.com.sigpf.resource.StudentAppendResource;
import lombok.var;

public class StudentAppendConverter {
	public static StudentAppendResource toDto(final StudentAppendEntity entities) {

		if (entities != null) {
			final var dto = new StudentAppendResource();

			dto.setId(entities.getId());
			dto.setDescription(entities.getDescription());
			dto.setPath(entities.getPath());
			dto.setCreate_date(entities.getCreateDate());
			dto.setUpdate_date(entities.getUpdateDate());
			dto.setCreateUser(entities.getCreateUser());
			dto.setUpdateUser(entities.getUpdateUser());
			dto.setStudent(StudentConverter.toDto(entities.getStudent()));

			return dto;
		} else {
			return null;
		}
	}

	public static StudentAppendEntity toEntity(final StudentAppendResource dto) {

		if (dto != null) {
			var entities = new StudentAppendEntity();

			entities.setId(dto.getId());
			entities.setDescription(dto.getDescription());
			entities.setPath(dto.getPath());
			entities.setCreateDate(dto.getCreate_date());
			entities.setUpdateDate(dto.getUpdate_date());
			entities.setCreateUser(dto.getCreateUser());
			entities.setUpdateUser(dto.getUpdateUser());
			entities.setStudent(StudentConverter.toEntity(dto.getStudent()));

			return entities;
		} else {

			return null;

		}

	}

	public static StudentAppendResource toDto(final Optional<StudentAppendEntity> optional) {
		return optional.isPresent() ? toDto(optional.get()) : null;
	}

	public static Optional<StudentAppendResource> toOptionalDto(final Optional<StudentAppendEntity> optional) {
		return Optional.ofNullable(toDto(optional));
	}

	public static List<StudentAppendResource> listToDto(final List<StudentAppendEntity> entities) {
		List<StudentAppendResource> listResource = new ArrayList<StudentAppendResource>();

		entities.forEach(address -> {
			listResource.add(toDto(address));
		});

		return listResource;
	}
}
