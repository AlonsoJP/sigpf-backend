package br.com.sigpf.converter;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.sigpf.entity.BankEntity;
import br.com.sigpf.resource.BankResource;
import lombok.var;

public class BankConverter {

	public static BankResource toDto(final BankEntity entities) {

		if (entities != null) {
			final var dto = new BankResource();

			dto.setId(entities.getId());
			dto.setSale(entities.getSale());
			dto.setExercise_bank(entities.getExercise_bank());
			dto.setActive(entities.getActive());
			dto.setCreateDate(entities.getCreateDate());
			dto.setUpdateDate(entities.getUpdateDate());
			dto.setCreateUser(entities.getCreateUser());
			dto.setUpdateUser(entities.getUpdateUser());

			return dto;
		} else {
			return null;
		}
	}

	public static BankEntity toEntity(final BankResource dto) {

		if (dto != null) {
			var entities = new BankEntity();

			entities.setId(dto.getId());
			entities.setSale(dto.getSale());
			entities.setExercise_bank(dto.getExercise_bank());
			entities.setActive(dto.getActive());
			entities.setCreateDate(LocalDateTime.now().atZone(ZoneId.systemDefault()).toLocalDateTime());
			entities.setUpdateDate(LocalDateTime.now().atZone(ZoneId.systemDefault()).toLocalDateTime());
			entities.setCreateUser(dto.getCreateUser());
			entities.setUpdateUser(dto.getUpdateUser());

			return entities;
		} else {

			return null;

		}

	}

	public static BankResource toDto(final Optional<BankEntity> optional) {
		return optional.isPresent() ? toDto(optional.get()) : null;
	}

	public static Optional<BankResource> toOptionalDto(final Optional<BankEntity> optional) {
		return Optional.ofNullable(toDto(optional));
	}

	public static List<BankResource> listToDto(final List<BankEntity> entities) {
		List<BankResource> listResource = new ArrayList<BankResource>();

		entities.forEach(address -> {
			listResource.add(toDto(address));
		});

		return listResource;
	}

}
