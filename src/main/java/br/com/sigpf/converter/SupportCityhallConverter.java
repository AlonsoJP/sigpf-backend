package br.com.sigpf.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.sigpf.entity.SupportCityhallEntity;
import br.com.sigpf.resource.SupportCityhallResource;
import lombok.var;

public class SupportCityhallConverter {

	public static SupportCityhallResource toDto(final SupportCityhallEntity entities) {

		if (entities != null) {
			final var dto = new SupportCityhallResource();

			dto.setId(entities.getId());
			dto.setAmmount(entities.getAmmount());
			dto.setDescription(entities.getDescription());
			dto.setMounth_reference(entities.getMounth_reference());
			dto.setCreateDate(entities.getCreateDate());
			dto.setUpdateDate(entities.getUpdateDate());
			dto.setCreateUser(entities.getCreateUser());
			dto.setUpdateUser(entities.getUpdateUser());

			return dto;
		} else {
			return null;
		}
	}

	public static SupportCityhallEntity toEntity(final SupportCityhallResource dto) {

		if (dto != null) {
			var entities = new SupportCityhallEntity();

			entities.setId(dto.getId());
			entities.setAmmount(dto.getAmmount());
			entities.setDescription(dto.getDescription());
			entities.setMounth_reference(dto.getMounth_reference());
			entities.setCreateDate(dto.getCreateDate());
			entities.setUpdateDate(dto.getUpdateDate());
			entities.setCreateUser(dto.getCreateUser());
			entities.setUpdateUser(dto.getUpdateUser());

			return entities;
		} else {

			return null;

		}

	}

	public static SupportCityhallResource toDto(final Optional<SupportCityhallEntity> optional) {
		return optional.isPresent() ? toDto(optional.get()) : null;
	}

	public static Optional<SupportCityhallResource> toOptionalDto(final Optional<SupportCityhallEntity> optional) {
		return Optional.ofNullable(toDto(optional));
	}

	public static List<SupportCityhallResource> listToDto(final List<SupportCityhallEntity> entities) {
		List<SupportCityhallResource> listResource = new ArrayList<SupportCityhallResource>();

		entities.forEach(address -> {
			listResource.add(toDto(address));
		});

		return listResource;
	}

}
