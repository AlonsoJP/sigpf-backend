package br.com.sigpf.converter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.sigpf.entity.StudentEntity;
import br.com.sigpf.enums.PaymentTypeEnum;
import br.com.sigpf.resource.StudentResource;
import lombok.var;

public class StudentConverter {

	public static StudentResource toDto(final StudentEntity entities) {

		if (entities != null) {
			final var dto = new StudentResource();

			dto.setId(entities.getId());
			dto.setName(entities.getName());
			dto.setPhoto(entities.getPhoto());
			dto.setCpf(entities.getCpf());
			dto.setRg(entities.getRg());
			dto.setRa(entities.getRa());
			dto.setEmail(entities.getEmail());
			dto.setPhone1(entities.getPhone1());
			dto.setPhone2(entities.getPhone2());
			dto.setBirth_date(entities.getBirth_date());
			dto.setUser_student(entities.getUser_student());
			dto.setZip_code(entities.getZip_code());
			dto.setCity(entities.getCity());
			dto.setComplement(entities.getComplement());
			dto.setNumber_house(entities.getNumber_house());
			dto.setUf(entities.getUf());
			dto.setCreateDate(entities.getCreateDate());
			dto.setUpdateDate(entities.getUpdateDate());
			dto.setCreateUser(entities.getCreateUser());
			dto.setUpdateUser(entities.getUpdateUser());
			dto.setActive(entities.getActive());
			dto.setPaymentType(entities.getPaymentType().getDescricao());
			dto.setDistrict(DistrictConverter.toDto(entities.getDistrict()));
			dto.setStreet(StreetConverter.toDto(entities.getStreet()));
			dto.setCourse(CourseConverter.toDto(entities.getCourse()));

			return dto;
		} else {
			return null;
		}
	}

	public static StudentEntity toEntity(final StudentResource dto) {

		if (dto != null) {
			var entities = new StudentEntity();

			entities.setId(dto.getId());
			entities.setName(dto.getName());
			entities.setPhoto(dto.getPhoto());
			entities.setCpf(dto.getCpf());
			entities.setRg(dto.getRg());
			entities.setRa(dto.getRa());
			entities.setEmail(dto.getEmail());
			entities.setPhone1(dto.getPhone1());
			entities.setPhone2(dto.getPhone2());
			entities.setBirth_date(dto.getBirth_date());
			entities.setUser_student(dto.getUser_student());
			entities.setZip_code(dto.getZip_code());
			entities.setCity(dto.getCity());
			entities.setComplement(dto.getComplement());
			entities.setNumber_house(dto.getNumber_house());
			entities.setUf(dto.getUf());
			entities.setCreateDate(LocalDate.now());
			entities.setUpdateDate(LocalDate.now());
			entities.setCreateUser(dto.getCreateUser());
			entities.setUpdateUser(dto.getUpdateUser());
			entities.setActive(dto.getActive());
			entities.setPaymentType(PaymentTypeEnum.getByDescricao(dto.getPaymentType()));
			entities.setDistrict(DistrictConverter.toEntity(dto.getDistrict()));
			entities.setStreet(StreetConverter.toEntity(dto.getStreet()));
			entities.setCourse(CourseConverter.toEntity(dto.getCourse()));

			return entities;
		} else {

			return null;

		}

	}

	public static StudentResource toDto(final Optional<StudentEntity> optional) {
		return optional.isPresent() ? toDto(optional.get()) : null;
	}

	public static Optional<StudentResource> toOptionalDto(final Optional<StudentEntity> optional) {
		return Optional.ofNullable(toDto(optional));
	}

	public static List<StudentResource> listToDto(final List<StudentEntity> entities) {
		List<StudentResource> listResource = new ArrayList<StudentResource>();

		entities.forEach(a -> {
			listResource.add(toDto(a));
		});

		return listResource;
	}

}
