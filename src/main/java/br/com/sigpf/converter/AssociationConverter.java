package br.com.sigpf.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.sigpf.entity.AssociationEntity;
import br.com.sigpf.resource.AssociationResource;
import lombok.var;

public class AssociationConverter {

	public static AssociationResource toDto(final AssociationEntity entities) {

		if (entities != null) {
			final var dto = new AssociationResource();

			dto.setId(entities.getId());
			dto.setOwner(entities.getOwner());
			dto.setFantasyName(entities.getFantasyName());
			dto.setCnpj(entities.getCnpj());
			dto.setEmail(entities.getEmail());
			dto.setPhone(entities.getPhone());
			dto.setZip_code(entities.getZip_code());
			dto.setCity(entities.getCity());
			dto.setComplement(entities.getComplement());
			dto.setNumber_house(entities.getNumber_house());
			dto.setUf(entities.getUf());
			dto.setCreateDate(entities.getCreateDate());
			dto.setUpdateDate(entities.getUpdateDate());
			dto.setCreateUser(entities.getCreateUser());
			dto.setUpdateUser(entities.getUpdateUser());
			dto.setDistrict(DistrictConverter.toDto(entities.getDistrict()));
			dto.setStreet(StreetConverter.toDto(entities.getStreet()));

			return dto;
		} else {
			return null;
		}
	}

	public static AssociationEntity toEntity(final AssociationResource dto) {

		if (dto != null) {
			var entities = new AssociationEntity();

			entities.setId(dto.getId());
			entities.setOwner(dto.getOwner());
			entities.setFantasyName(dto.getFantasyName());
			entities.setCnpj(dto.getCnpj());
			entities.setEmail(dto.getEmail());
			entities.setPhone(dto.getPhone());
			entities.setZip_code(dto.getZip_code());
			entities.setCity(dto.getCity());
			entities.setComplement(dto.getComplement());
			entities.setNumber_house(dto.getNumber_house());
			entities.setUf(dto.getUf());
			entities.setCreateDate(dto.getCreateDate());
			entities.setUpdateDate(dto.getUpdateDate());
			entities.setCreateUser(dto.getCreateUser());
			entities.setUpdateUser(dto.getUpdateUser());
			entities.setDistrict(DistrictConverter.toEntity(dto.getDistrict()));
			entities.setStreet(StreetConverter.toEntity(dto.getStreet()));

			return entities;
		} else {

			return null;

		}

	}

	public static AssociationResource toDto(final Optional<AssociationEntity> optional) {
		return optional.isPresent() ? toDto(optional.get()) : null;
	}

	public static Optional<AssociationResource> toOptionalDto(final Optional<AssociationEntity> optional) {
		return Optional.ofNullable(toDto(optional));
	}

	public static List<AssociationResource> listToDto(final List<AssociationEntity> entities) {
		List<AssociationResource> listResource = new ArrayList<AssociationResource>();

		entities.forEach(address -> {
			listResource.add(toDto(address));
		});

		return listResource;
	}

}
