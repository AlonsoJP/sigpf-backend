package br.com.sigpf.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.sigpf.entity.DriverAppendEntity;
import br.com.sigpf.resource.DriverAppendResource;
import lombok.var;

public class DriverAppendConverter {

	public static DriverAppendResource toDto(final DriverAppendEntity entities) {

		if (entities != null) {
			final var dto = new DriverAppendResource();

			dto.setId(entities.getId());
			dto.setDescription(entities.getDescription());
			dto.setPath(entities.getPath());
			dto.setCreate_date(entities.getCreateDate());
			dto.setUpdate_date(entities.getUpdateDate());
			dto.setCreateUser(entities.getCreateUser());
			dto.setUpdateUser(entities.getUpdateUser());
			dto.setDriver(DriverConverter.toDto(entities.getDriver()));

			return dto;
		} else {
			return null;
		}
	}

	public static DriverAppendEntity toEntity(final DriverAppendResource dto) {

		if (dto != null) {
			var entities = new DriverAppendEntity();

			entities.setId(dto.getId());
			entities.setDescription(dto.getDescription());
			entities.setPath(dto.getPath());
			entities.setCreateDate(dto.getCreate_date());
			entities.setUpdateDate(dto.getUpdate_date());
			entities.setCreateUser(dto.getCreateUser());
			entities.setUpdateUser(dto.getUpdateUser());
			entities.setDriver(DriverConverter.toEntity(dto.getDriver()));

			return entities;
		} else {

			return null;

		}

	}

	public static DriverAppendResource toDto(final Optional<DriverAppendEntity> optional) {
		return optional.isPresent() ? toDto(optional.get()) : null;
	}

	public static Optional<DriverAppendResource> toOptionalDto(final Optional<DriverAppendEntity> optional) {
		return Optional.ofNullable(toDto(optional));
	}

	public static List<DriverAppendResource> listToDto(final List<DriverAppendEntity> entities) {
		List<DriverAppendResource> listResource = new ArrayList<DriverAppendResource>();

		entities.forEach(address -> {
			listResource.add(toDto(address));
		});

		return listResource;
	}

}
