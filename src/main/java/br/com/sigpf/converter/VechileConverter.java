package br.com.sigpf.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.sigpf.entity.VechileEntity;
import br.com.sigpf.resource.VechileResource;
import lombok.var;

public class VechileConverter {

	public static VechileResource toDto(final VechileEntity entities) {

		if (entities != null) {
			final var dto = new VechileResource();

			dto.setId(entities.getId());
			dto.setName(entities.getName());
			dto.setIdentification(entities.getIdentification());
			dto.setDescription(entities.getDescription());
			dto.setModel(entities.getModel());
			dto.setKm_l(entities.getKm_l());
			dto.setBench(entities.getBench());
			dto.setActive(entities.getActive());
			dto.setCreateDate(entities.getCreateDate());
			dto.setUpdateDate(entities.getUpdateDate());
			dto.setCreateUser(entities.getCreateUser());
			dto.setUpdateUser(entities.getUpdateUser());

			return dto;
		} else {
			return null;
		}
	}

	public static VechileEntity toEntity(final VechileResource dto) {

		if (dto != null) {
			var entities = new VechileEntity();

			entities.setId(dto.getId());
			entities.setName(dto.getName());
			entities.setIdentification(dto.getIdentification());
			entities.setDescription(dto.getDescription());
			entities.setModel(dto.getModel());
			entities.setKm_l(dto.getKm_l());
			entities.setBench(dto.getBench());
			entities.setActive(dto.getActive());
			entities.setCreateDate(dto.getCreateDate());
			entities.setUpdateDate(dto.getUpdateDate());
			entities.setCreateUser(dto.getCreateUser());
			entities.setUpdateUser(dto.getUpdateUser());

			return entities;
		} else {

			return null;

		}

	}

	public static VechileResource toDto(final Optional<VechileEntity> optional) {
		return optional.isPresent() ? toDto(optional.get()) : null;
	}

	public static Optional<VechileResource> toOptionalDto(final Optional<VechileEntity> optional) {
		return Optional.ofNullable(toDto(optional));
	}

	public static List<VechileResource> listToDto(final List<VechileEntity> entities) {
		List<VechileResource> listResource = new ArrayList<VechileResource>();

		entities.forEach(address -> {
			listResource.add(toDto(address));
		});

		return listResource;
	}

}
