package br.com.sigpf.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.sigpf.entity.DriverEntity;
import br.com.sigpf.resource.DriverResource;
import lombok.var;

public class DriverConverter {

	public static DriverResource toDto(final DriverEntity entities) {

		if (entities != null) {
			final var dto = new DriverResource();

			dto.setId(entities.getId());
			dto.setName(entities.getName());
			dto.setPhoto(entities.getPhoto());
			dto.setCpf(entities.getCpf());
			dto.setRg(entities.getRg());
			dto.setLicense(entities.getLicense());
			dto.setEmail(entities.getEmail());
			dto.setPhone1(entities.getPhone1());
			dto.setPhone2(entities.getPhone2());
			dto.setBirth_date(entities.getBirth_date());
			dto.setZip_code(entities.getZip_code());
			dto.setCity(entities.getCity());
			dto.setComplement(entities.getComplement());
			dto.setNumber_house(entities.getNumber_house());
			dto.setUf(entities.getUf());
			dto.setActive(entities.getActive());
			dto.setCreateDate(entities.getCreateDate());
			dto.setUpdateDate(entities.getUpdateDate());
			dto.setCreateUser(entities.getCreateUser());
			dto.setUpdateUser(entities.getUpdateUser());
			dto.setDistrict(DistrictConverter.toDto(entities.getDistrict()));
			dto.setStreet(StreetConverter.toDto(entities.getStreet()));

			return dto;
		} else {
			return null;
		}
	}

	public static DriverEntity toEntity(final DriverResource dto) {

		if (dto != null) {
			var entities = new DriverEntity();

			entities.setId(dto.getId());
			entities.setName(dto.getName());
			entities.setPhoto(dto.getPhoto());
			entities.setCpf(dto.getCpf());
			entities.setRg(dto.getRg());
			entities.setLicense(dto.getLicense());
			entities.setEmail(dto.getEmail());
			entities.setPhone1(dto.getPhone1());
			entities.setPhone2(dto.getPhone2());
			entities.setBirth_date(dto.getBirth_date());
			entities.setZip_code(dto.getZip_code());
			entities.setCity(dto.getCity());
			entities.setComplement(dto.getComplement());
			entities.setNumber_house(dto.getNumber_house());
			entities.setUf(dto.getUf());
			entities.setActive(dto.getActive());
			entities.setCreateDate(dto.getCreateDate());
			entities.setUpdateDate(dto.getUpdateDate());
			entities.setCreateUser(dto.getCreateUser());
			entities.setUpdateUser(dto.getUpdateUser());
			entities.setDistrict(DistrictConverter.toEntity(dto.getDistrict()));
			entities.setStreet(StreetConverter.toEntity(dto.getStreet()));

			return entities;
		} else {

			return null;

		}

	}

	public static DriverResource toDto(final Optional<DriverEntity> optional) {
		return optional.isPresent() ? toDto(optional.get()) : null;
	}

	public static Optional<DriverResource> toOptionalDto(final Optional<DriverEntity> optional) {
		return Optional.ofNullable(toDto(optional));
	}

	public static List<DriverResource> listToDto(final List<DriverEntity> entities) {
		List<DriverResource> listResource = new ArrayList<DriverResource>();

		entities.forEach(address -> {
			listResource.add(toDto(address));
		});

		return listResource;
	}

}
