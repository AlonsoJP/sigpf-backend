package br.com.sigpf.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.sigpf.entity.UniversityEntity;
import br.com.sigpf.resource.UniversityResource;
import lombok.var;

public class UniversityConverter {
	public static UniversityResource toDto(final UniversityEntity entities) {

		if (entities != null) {
			final var dto = new UniversityResource();

			dto.setId(entities.getId());
			dto.setName(entities.getName());
			dto.setPhone(entities.getPhone());
			dto.setCnpj(entities.getCnpj());
			dto.setEmail(entities.getEmail());
			dto.setZip_code(entities.getZip_code());
			dto.setCity(entities.getCity());
			dto.setComplement(entities.getComplement());
			dto.setNumber_house(entities.getNumber_house());
			dto.setUf(entities.getUf());
			dto.setCreateDate(entities.getCreateDate());
			dto.setUpdateDate(entities.getUpdateDate());
			dto.setCreateUser(entities.getCreateUser());
			dto.setUpdateUser(entities.getUpdateUser());
			dto.setDistrict(DistrictConverter.toDto(entities.getDistrict()));
			dto.setStreet(StreetConverter.toDto(entities.getStreet()));

			return dto;
		} else {
			return null;
		}
	}

	public static UniversityEntity toEntity(final UniversityResource dto) {

		if (dto != null) {
			var entities = new UniversityEntity();

			entities.setId(dto.getId());
			entities.setName(dto.getName());
			entities.setPhone(dto.getPhone());
			entities.setCnpj(dto.getCnpj());
			entities.setEmail(dto.getEmail());
			entities.setZip_code(dto.getZip_code());
			entities.setCity(dto.getCity());
			entities.setComplement(dto.getComplement());
			entities.setNumber_house(dto.getNumber_house());
			entities.setUf(dto.getUf());
			entities.setCreateDate(dto.getCreateDate());
			entities.setUpdateDate(dto.getUpdateDate());
			entities.setCreateUser(dto.getCreateUser());
			entities.setUpdateUser(dto.getUpdateUser());
			entities.setDistrict(DistrictConverter.toEntity(dto.getDistrict()));
			entities.setStreet(StreetConverter.toEntity(dto.getStreet()));

			return entities;
		} else {

			return null;

		}

	}

	public static UniversityResource toDto(final Optional<UniversityEntity> optional) {
		return optional.isPresent() ? toDto(optional.get()) : null;
	}

	public static Optional<UniversityResource> toOptionalDto(final Optional<UniversityEntity> optional) {
		return Optional.ofNullable(toDto(optional));
	}

	public static List<UniversityResource> listToDto(final List<UniversityEntity> entities) {
		List<UniversityResource> listResource = new ArrayList<UniversityResource>();

		entities.forEach(address -> {
			listResource.add(toDto(address));
		});

		return listResource;
	}
}
