package br.com.sigpf.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.sigpf.entity.UniversityAppendEntity;
import br.com.sigpf.resource.UniversityAppendResource;
import lombok.var;

public class UniversityAppendConverter {

	public static UniversityAppendResource toDto(final UniversityAppendEntity entities) {

		if (entities != null) {
			final var dto = new UniversityAppendResource();

			dto.setId(entities.getId());
			dto.setDescription(entities.getDescription());
			dto.setPath(entities.getPath());
			dto.setCreate_date(entities.getCreateDate());
			dto.setUpdate_date(entities.getUpdateDate());
			dto.setCreateUser(entities.getCreateUser());
			dto.setUpdateUser(entities.getUpdateUser());
			dto.setUniversity(UniversityConverter.toDto(entities.getUniversity()));

			return dto;
		} else {
			return null;
		}
	}

	public static UniversityAppendEntity toEntity(final UniversityAppendResource dto) {

		if (dto != null) {
			var entities = new UniversityAppendEntity();

			entities.setId(dto.getId());
			entities.setDescription(dto.getDescription());
			entities.setPath(dto.getPath());
			entities.setCreateDate(dto.getCreate_date());
			entities.setUpdateDate(dto.getUpdate_date());
			entities.setCreateUser(dto.getCreateUser());
			entities.setUpdateUser(dto.getUpdateUser());
			entities.setUniversity(UniversityConverter.toEntity(dto.getUniversity()));

			return entities;
		} else {

			return null;

		}

	}

	public static UniversityAppendResource toDto(final Optional<UniversityAppendEntity> optional) {
		return optional.isPresent() ? toDto(optional.get()) : null;
	}

	public static Optional<UniversityAppendResource> toOptionalDto(final Optional<UniversityAppendEntity> optional) {
		return Optional.ofNullable(toDto(optional));
	}

	public static List<UniversityAppendResource> listToDto(final List<UniversityAppendEntity> entities) {
		List<UniversityAppendResource> listResource = new ArrayList<UniversityAppendResource>();

		entities.forEach(address -> {
			listResource.add(toDto(address));
		});

		return listResource;
	}

}
