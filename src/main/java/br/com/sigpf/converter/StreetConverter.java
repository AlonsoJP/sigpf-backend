package br.com.sigpf.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.sigpf.entity.StreetEntity;
import br.com.sigpf.resource.StreetResource;
import lombok.var;

public class StreetConverter {

	public static StreetResource toDto(final StreetEntity entity) {

		if (entity != null) {
			final var dto = new StreetResource();

			dto.setId(entity.getId());
			dto.setName_street(entity.getName_street());
			dto.setCreateDate(entity.getCreateDate());
			dto.setUpdateDate(entity.getUpdateDate());
			dto.setCreateUser(entity.getCreateUser());
			dto.setUpdateUser(entity.getUpdateUser());

			return dto;
		} else {
			return null;
		}
	}

	public static StreetEntity toEntity(final StreetResource dto) {

		if (dto != null) {
			var entities = new StreetEntity();

			entities.setId(dto.getId());
			entities.setName_street(dto.getName_street());
			entities.setCreateDate(dto.getCreateDate());
			entities.setUpdateDate(dto.getUpdateDate());
			entities.setCreateUser(dto.getCreateUser());
			entities.setUpdateUser(dto.getUpdateUser());

			return entities;
		} else {

			return null;

		}

	}

	public static StreetResource toDto(final Optional<StreetEntity> optional) {
		return optional.isPresent() ? toDto(optional.get()) : null;
	}

	public static Optional<StreetResource> toOptionalDto(final Optional<StreetEntity> optional) {
		return Optional.ofNullable(toDto(optional));
	}

	public static List<StreetResource> listToDto(final List<StreetEntity> entities) {
		List<StreetResource> listResource = new ArrayList<StreetResource>();

		entities.forEach(street -> {
			listResource.add(toDto(street));
		});

		return listResource;
	}
}
