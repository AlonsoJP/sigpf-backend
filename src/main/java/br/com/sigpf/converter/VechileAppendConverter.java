package br.com.sigpf.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.sigpf.entity.VechileAppendEntity;
import br.com.sigpf.resource.VechileAppendResource;
import lombok.var;

public class VechileAppendConverter {

	public static VechileAppendResource toDto(final VechileAppendEntity entities) {

		if (entities != null) {
			final var dto = new VechileAppendResource();

			dto.setId(entities.getId());
			dto.setDescription(entities.getDescription());
			dto.setPath(entities.getPath());
			dto.setCreate_date(entities.getCreateDate());
			dto.setUpdate_date(entities.getUpdateDate());
			dto.setCreateUser(entities.getCreateUser());
			dto.setUpdateUser(entities.getUpdateUser());
			dto.setVechile(VechileConverter.toDto(entities.getVechile()));
			return dto;
		} else {
			return null;
		}
	}

	public static VechileAppendEntity toEntity(final VechileAppendResource dto) {

		if (dto != null) {
			var entities = new VechileAppendEntity();

			entities.setId(dto.getId());
			entities.setDescription(dto.getDescription());
			entities.setPath(dto.getPath());
			entities.setCreateDate(dto.getCreate_date());
			entities.setUpdateDate(dto.getUpdate_date());
			entities.setCreateUser(dto.getCreateUser());
			entities.setUpdateUser(dto.getUpdateUser());
			entities.setVechile(VechileConverter.toEntity(dto.getVechile()));
			return entities;
		} else {

			return null;

		}

	}

	public static VechileAppendResource toDto(final Optional<VechileAppendEntity> optional) {
		return optional.isPresent() ? toDto(optional.get()) : null;
	}

	public static Optional<VechileAppendResource> toOptionalDto(final Optional<VechileAppendEntity> optional) {
		return Optional.ofNullable(toDto(optional));
	}

	public static List<VechileAppendResource> listToDto(final List<VechileAppendEntity> entities) {
		List<VechileAppendResource> listResource = new ArrayList<VechileAppendResource>();

		entities.forEach(address -> {
			listResource.add(toDto(address));
		});

		return listResource;
	}

}
