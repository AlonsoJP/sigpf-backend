package br.com.sigpf.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.sigpf.entity.SupportCityhallAppendEntity;
import br.com.sigpf.resource.SupportCityhallAppendResource;
import lombok.var;

public class SupportCityhallAppendConverter {

	public static SupportCityhallAppendResource toDto(final SupportCityhallAppendEntity entities) {

		if (entities != null) {
			final var dto = new SupportCityhallAppendResource();

			dto.setId(entities.getId());
			dto.setDescription(entities.getDescription());
			dto.setPath(entities.getPath());
			dto.setCreateDate(entities.getCreateDate());
			dto.setUpdateDate(entities.getUpdateDate());
			dto.setCreateUser(entities.getCreateUser());
			dto.setUpdateUser(entities.getUpdateUser());
			dto.setSupport_cityhall(SupportCityhallConverter.toDto(entities.getSupport_cityhall()));
			return dto;
		} else {
			return null;
		}
	}

	public static SupportCityhallAppendEntity toEntity(final SupportCityhallAppendResource dto) {

		if (dto != null) {
			var entities = new SupportCityhallAppendEntity();

			entities.setId(dto.getId());
			entities.setDescription(dto.getDescription());
			entities.setPath(dto.getPath());
			entities.setCreateDate(dto.getCreateDate());
			entities.setUpdateDate(dto.getUpdateDate());
			entities.setCreateUser(dto.getCreateUser());
			entities.setUpdateUser(dto.getUpdateUser());
			entities.setSupport_cityhall(SupportCityhallConverter.toEntity(dto.getSupport_cityhall()));
			return entities;
		} else {

			return null;

		}

	}

	public static SupportCityhallAppendResource toDto(final Optional<SupportCityhallAppendEntity> optional) {
		return optional.isPresent() ? toDto(optional.get()) : null;
	}

	public static Optional<SupportCityhallAppendResource> toOptionalDto(
			final Optional<SupportCityhallAppendEntity> optional) {
		return Optional.ofNullable(toDto(optional));
	}

	public static List<SupportCityhallAppendResource> listToDto(final List<SupportCityhallAppendEntity> entities) {
		List<SupportCityhallAppendResource> listResource = new ArrayList<SupportCityhallAppendResource>();

		entities.forEach(address -> {
			listResource.add(toDto(address));
		});

		return listResource;
	}

}
