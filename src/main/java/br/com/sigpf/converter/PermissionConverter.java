package br.com.sigpf.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.sigpf.entity.PermissionEntity;
import br.com.sigpf.resource.PermissionResource;
import lombok.var;

public class PermissionConverter {

	public static PermissionResource toDto(final PermissionEntity entities) {

		if (entities != null) {
			final var dto = new PermissionResource();

			dto.setId(entities.getId());
			dto.setDescription(entities.getDescription());

			return dto;
		} else {
			return null;
		}
	}

	public static PermissionEntity toEntity(final PermissionResource dto) {

		if (dto != null) {
			var entities = new PermissionEntity();

			entities.setId(dto.getId());
			entities.setDescription(dto.getDescription());

			return entities;
		} else {

			return null;

		}

	}

	public static PermissionResource toDto(final Optional<PermissionEntity> optional) {
		return optional.isPresent() ? toDto(optional.get()) : null;
	}

	public static Optional<PermissionResource> toOptionalDto(final Optional<PermissionEntity> optional) {
		return Optional.ofNullable(toDto(optional));
	}

	public static List<PermissionResource> listToDto(final List<PermissionEntity> entities) {
		List<PermissionResource> listResource = new ArrayList<PermissionResource>();

		entities.forEach(address -> {
			listResource.add(toDto(address));
		});

		return listResource;
	}

}
