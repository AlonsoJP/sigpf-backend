package br.com.sigpf.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.sigpf.entity.CourseEntity;
import br.com.sigpf.resource.CourseResource;
import lombok.var;

public class CourseConverter {

	public static CourseResource toDto(final CourseEntity entities) {

		if (entities != null) {
			final var dto = new CourseResource();

			dto.setId(entities.getId());
			dto.setName(entities.getName());
			dto.setCreateDate(entities.getCreateDate());
			dto.setUpdateDate(entities.getUpdateDate());
			dto.setCreateUser(entities.getCreateUser());
			dto.setUpdateUser(entities.getUpdateUser());

			return dto;
		} else {
			return null;
		}
	}

	public static CourseEntity toEntity(final CourseResource dto) {

		if (dto != null) {
			var entities = new CourseEntity();

			entities.setId(dto.getId());
			entities.setName(dto.getName());
			entities.setCreateDate(dto.getCreateDate());
			entities.setUpdateDate(dto.getUpdateDate());
			entities.setCreateUser(dto.getCreateUser());
			entities.setUpdateUser(dto.getUpdateUser());

			return entities;
		} else {

			return null;

		}

	}

	public static CourseResource toDto(final Optional<CourseEntity> optional) {
		return optional.isPresent() ? toDto(optional.get()) : null;
	}

	public static Optional<CourseResource> toOptionalDto(final Optional<CourseEntity> optional) {
		return Optional.ofNullable(toDto(optional));
	}

	public static List<CourseResource> listToDto(final List<CourseEntity> entities) {
		List<CourseResource> listResource = new ArrayList<CourseResource>();

		entities.forEach(course -> {
			listResource.add(toDto(course));
		});

		return listResource;
	}

}
