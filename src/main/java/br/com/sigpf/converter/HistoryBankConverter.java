package br.com.sigpf.converter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.sigpf.entity.HistoryBankEntity;
import br.com.sigpf.enums.TypeTransactionEnum;
import br.com.sigpf.resource.HistoryBankResource;
import lombok.var;

public class HistoryBankConverter {

	public static HistoryBankResource toDto(final HistoryBankEntity entities) {

		if (entities != null) {
			final var dto = new HistoryBankResource();

			dto.setId(entities.getId());
			dto.setId_transaction(entities.getId_transaction());
			dto.setType_transaction(entities.getType_transaction().getDescricao());
			dto.setProtocol_transaction(entities.getProtocol_transaction());
			dto.setAmount_transaction(entities.getAmount_transaction());
			dto.setExercise_bank(entities.getExercise_bank());
			dto.setSale_old(entities.getSale_old());
			dto.setSale_new(entities.getSale_new());
			dto.setCreateDate(entities.getCreateDate());
			dto.setUpdateDate(entities.getUpdateDate());
			dto.setCreateUser(entities.getCreateUser());
			dto.setUpdateUser(entities.getUpdateUser());
			dto.setBank(BankConverter.toDto(entities.getBank()));

			return dto;
		} else {
			return null;
		}
	}

	public static HistoryBankEntity toEntity(final HistoryBankResource dto) {

		if (dto != null) {
			var entities = new HistoryBankEntity();

			entities.setId(dto.getId());
			entities.setId_transaction(dto.getId_transaction());
			entities.setType_transaction(TypeTransactionEnum.getByDescricao(dto.getType_transaction()));
			entities.setProtocol_transaction(dto.getProtocol_transaction());
			entities.setAmount_transaction(dto.getAmount_transaction());
			entities.setExercise_bank(dto.getExercise_bank());
			entities.setSale_old(dto.getSale_old());
			entities.setSale_new(dto.getSale_new());
			entities.setCreateDate(LocalDateTime.now());
			entities.setUpdateDate(LocalDateTime.now());
			entities.setCreateUser(dto.getCreateUser());
			entities.setUpdateUser(dto.getUpdateUser());
			entities.setBank(BankConverter.toEntity(dto.getBank()));

			return entities;
		} else {

			return null;

		}

	}

	public static HistoryBankResource toDto(final Optional<HistoryBankEntity> optional) {
		return optional.isPresent() ? toDto(optional.get()) : null;
	}

	public static Optional<HistoryBankResource> toOptionalDto(final Optional<HistoryBankEntity> optional) {
		return Optional.ofNullable(toDto(optional));
	}

	public static List<HistoryBankResource> listToDto(final List<HistoryBankEntity> entities) {
		List<HistoryBankResource> listResource = new ArrayList<HistoryBankResource>();

		entities.forEach(address -> {
			listResource.add(toDto(address));
		});

		return listResource;
	}

}
