package br.com.sigpf.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.sigpf.entity.RecipeEntity;
import br.com.sigpf.resource.RecipeResource;
import lombok.var;

public class RecipeConverter {

	public static RecipeResource toDto(final RecipeEntity entities) {

		if (entities != null) {
			final var dto = new RecipeResource();

			dto.setId(entities.getId());
			dto.setName(entities.getName());
			dto.setDescription(entities.getDescription());
//			dto.setType_recipe(entities.getType_recipe().getDescricao());
			dto.setCreateDate(entities.getCreateDate());
			dto.setUpdateDate(entities.getUpdateDate());
			dto.setCreateUser(entities.getCreateUser());
			dto.setUpdateUser(entities.getUpdateUser());

			return dto;
		} else {
			return null;
		}
	}

	public static RecipeEntity toEntity(final RecipeResource dto) {

		if (dto != null) {
			var entities = new RecipeEntity();

			entities.setId(dto.getId());
			entities.setName(dto.getName());
			entities.setDescription(dto.getDescription());
//			entities.setType_recipe(TypeTransactionEnum.getByDescricao(dto.getType_recipe()));
			entities.setCreateDate(dto.getCreateDate());
			entities.setUpdateDate(dto.getUpdateDate());
			entities.setCreateUser(dto.getCreateUser());
			entities.setUpdateUser(dto.getUpdateUser());

			return entities;
		} else {

			return null;

		}

	}

	public static RecipeResource toDto(final Optional<RecipeEntity> optional) {
		return optional.isPresent() ? toDto(optional.get()) : null;
	}

	public static Optional<RecipeResource> toOptionalDto(final Optional<RecipeEntity> optional) {
		return Optional.ofNullable(toDto(optional));
	}

	public static List<RecipeResource> listToDto(final List<RecipeEntity> entities) {
		List<RecipeResource> listResource = new ArrayList<RecipeResource>();

		entities.forEach(address -> {
			listResource.add(toDto(address));
		});

		return listResource;
	}

}
