package br.com.sigpf.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.sigpf.entity.AssociationAppendEntity;
import br.com.sigpf.resource.AssociationAppendResource;
import lombok.var;

public class AssociationAppendConverter {

	public static AssociationAppendResource toDto(final AssociationAppendEntity entities) {

		if (entities != null) {
			final var dto = new AssociationAppendResource();

			dto.setId(entities.getId());
			dto.setDescription(entities.getDescription());
			dto.setPath(entities.getPath());
			dto.setCreate_date(entities.getCreateDate());
			dto.setUpdate_date(entities.getUpdateDate());
			dto.setCreateUser(entities.getCreateUser());
			dto.setUpdateUser(entities.getUpdateUser());
			dto.setAssociation(AssociationConverter.toDto(entities.getAssociation()));

			return dto;
		} else {
			return null;
		}
	}

	public static AssociationAppendEntity toEntity(final AssociationAppendResource dto) {

		if (dto != null) {
			var entities = new AssociationAppendEntity();

			entities.setId(dto.getId());
			entities.setDescription(dto.getDescription());
			entities.setPath(dto.getPath());
			entities.setCreateDate(dto.getCreate_date());
			entities.setUpdateDate(dto.getUpdate_date());
			entities.setCreateUser(dto.getCreateUser());
			entities.setUpdateUser(dto.getUpdateUser());
			entities.setAssociation(AssociationConverter.toEntity(dto.getAssociation()));

			return entities;
		} else {

			return null;

		}

	}

	public static AssociationAppendResource toDto(final Optional<AssociationAppendEntity> optional) {
		return optional.isPresent() ? toDto(optional.get()) : null;
	}

	public static Optional<AssociationAppendResource> toOptionalDto(final Optional<AssociationAppendEntity> optional) {
		return Optional.ofNullable(toDto(optional));
	}

	public static List<AssociationAppendResource> listToDto(final List<AssociationAppendEntity> entities) {
		List<AssociationAppendResource> listResource = new ArrayList<AssociationAppendResource>();

		entities.forEach(address -> {
			listResource.add(toDto(address));
		});

		return listResource;
	}

}
