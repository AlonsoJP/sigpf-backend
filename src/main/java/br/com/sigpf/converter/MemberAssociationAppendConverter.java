package br.com.sigpf.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.sigpf.entity.MemberAssociationAppendEntity;
import br.com.sigpf.resource.MemberAssociationAppendResource;
import lombok.var;

public class MemberAssociationAppendConverter {

	public static MemberAssociationAppendResource toDto(final MemberAssociationAppendEntity entities) {

		if (entities != null) {
			final var dto = new MemberAssociationAppendResource();

			dto.setId(entities.getId());
			dto.setDescription(entities.getDescription());
			dto.setPath(entities.getPath());
			dto.setCreate_date(entities.getCreateDate());
			dto.setUpdate_date(entities.getUpdateDate());
			dto.setCreateUser(entities.getCreateUser());
			dto.setUpdateUser(entities.getUpdateUser());
			dto.setMemberAssociation(MemberAssociationConverter.toDto(entities.getMember_association()));

			return dto;
		} else {
			return null;
		}
	}

	public static MemberAssociationAppendEntity toEntity(final MemberAssociationAppendResource dto) {

		if (dto != null) {
			var entities = new MemberAssociationAppendEntity();

			entities.setId(dto.getId());
			entities.setDescription(dto.getDescription());
			entities.setPath(dto.getPath());
			entities.setCreateDate(dto.getCreate_date());
			entities.setUpdateDate(dto.getUpdate_date());
			entities.setCreateUser(dto.getCreateUser());
			entities.setUpdateUser(dto.getUpdateUser());
			entities.setMember_association(MemberAssociationConverter.toEntity(dto.getMemberAssociation()));

			return entities;
		} else {

			return null;

		}

	}

	public static MemberAssociationAppendResource toDto(final Optional<MemberAssociationAppendEntity> optional) {
		return optional.isPresent() ? toDto(optional.get()) : null;
	}

	public static Optional<MemberAssociationAppendResource> toOptionalDto(
			final Optional<MemberAssociationAppendEntity> optional) {
		return Optional.ofNullable(toDto(optional));
	}

	public static List<MemberAssociationAppendResource> listToDto(final List<MemberAssociationAppendEntity> entities) {
		List<MemberAssociationAppendResource> listResource = new ArrayList<MemberAssociationAppendResource>();

		entities.forEach(address -> {
			listResource.add(toDto(address));
		});

		return listResource;
	}

}
