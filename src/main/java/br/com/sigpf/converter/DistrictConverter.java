package br.com.sigpf.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.sigpf.entity.DistrictEntity;
import br.com.sigpf.resource.DistrictResource;
import lombok.var;

public class DistrictConverter {
	public static DistrictResource toDto(final DistrictEntity entity) {

		if (entity != null) {
			final var dto = new DistrictResource();

			dto.setId(entity.getId());
			dto.setName_district(entity.getName_district());
			dto.setCreateDate(entity.getCreateDate());
			dto.setUpdateDate(entity.getUpdateDate());
			dto.setCreateUser(entity.getCreateUser());
			dto.setUpdateUser(entity.getUpdateUser());

			return dto;
		} else {
			return null;
		}
	}

	public static DistrictEntity toEntity(final DistrictResource dto) {

		if (dto != null) {
			var entities = new DistrictEntity();

			entities.setId(dto.getId());
			entities.setName_district(dto.getName_district());
			entities.setCreateDate(dto.getCreateDate());
			entities.setUpdateDate(dto.getUpdateDate());
			entities.setCreateUser(dto.getCreateUser());
			entities.setUpdateUser(dto.getUpdateUser());

			return entities;
		} else {

			return null;

		}

	}

	public static DistrictResource toDto(final Optional<DistrictEntity> optional) {
		return optional.isPresent() ? toDto(optional.get()) : null;
	}

	public static Optional<DistrictResource> toOptionalDto(final Optional<DistrictEntity> optional) {
		return Optional.ofNullable(toDto(optional));
	}

	public static List<DistrictResource> listToDto(final List<DistrictEntity> entities) {
		List<DistrictResource> listResource = new ArrayList<DistrictResource>();

		entities.forEach(district -> {
			listResource.add(toDto(district));
		});

		return listResource;
	}

}