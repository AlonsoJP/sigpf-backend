package br.com.sigpf.converter;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.sigpf.entity.InputEntity;
import br.com.sigpf.enums.TypeTransactionEnum;
import br.com.sigpf.resource.InputResource;
import lombok.var;

public class InputConverter {

	public static InputResource toDto(final InputEntity entities) {
		if (entities != null) {
			final var dto = new InputResource();

			dto.setId(entities.getId());
//			dto.setType_transaction(TypeTransactionEnum.getByDescricao(entities.getType_transaction()).getId());
			dto.setType_transaction(entities.getType_transaction().getDescricao());
			dto.setAmount_transaction(entities.getAmount_transaction());
			dto.setProtocol_transaction(entities.getProtocol_transaction());
			dto.setDescription(entities.getDescription());
			dto.setCreateDate(entities.getCreateDate());
			dto.setUpdateDate(entities.getUpdateDate());
			dto.setCreateUser(entities.getCreateUser());
			dto.setUpdateUser(entities.getUpdateUser());
			dto.setRecipe(RecipeConverter.toDto(entities.getRecipe()));

			return dto;
		} else {
			return null;
		}
	}

	public static InputEntity toEntity(final InputResource dto) {

		if (dto != null) {
			var entities = new InputEntity();

			entities.setId(dto.getId());
//			entities.setType_transaction(TypeTransactionEnum.getById(dto.getType_transaction()).getDescricao()); TODO
			entities.setType_transaction(TypeTransactionEnum.getByDescricao(dto.getType_transaction()));
			entities.setAmount_transaction(dto.getAmount_transaction());
			entities.setProtocol_transaction(dto.getProtocol_transaction());
			entities.setDescription(dto.getDescription());
			entities.setCreateDate(LocalDateTime.now().atZone(ZoneId.systemDefault()).toLocalDateTime());
			entities.setUpdateDate(LocalDateTime.now().atZone(ZoneId.systemDefault()).toLocalDateTime());
			entities.setCreateUser(dto.getCreateUser());
			entities.setUpdateUser(dto.getUpdateUser());
			entities.setRecipe(RecipeConverter.toEntity(dto.getRecipe()));

			return entities;
		} else {

			return null;

		}

	}

	public static InputResource toDto(final Optional<InputEntity> optional) {
		return optional.isPresent() ? toDto(optional.get()) : null;
	}

	public static Optional<InputResource> toOptionalDto(final Optional<InputEntity> optional) {
		return Optional.ofNullable(toDto(optional));
	}

	public static List<InputResource> listToDto(final List<InputEntity> entities) {
		List<InputResource> listResource = new ArrayList<InputResource>();

		entities.forEach(address -> {
			listResource.add(toDto(address));
		});

		return listResource;
	}

}
