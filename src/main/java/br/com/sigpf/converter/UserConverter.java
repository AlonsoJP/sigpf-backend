package br.com.sigpf.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.sigpf.entity.UserEntity;
import br.com.sigpf.resource.UserResource;
import lombok.var;

public class UserConverter {

	public static UserResource toDto(final UserEntity entities) {

		if (entities != null) {
			final var dto = new UserResource();

			dto.setId(entities.getId());
			dto.setName(entities.getName());
			dto.setEmail(entities.getEmail());
			dto.setPhoto(entities.getPhoto());
			dto.setUser_name(entities.getUser_name());
			dto.setDocument(entities.getDocument());
			dto.setCreateDate(entities.getCreateDate());
			dto.setUpdateDate(entities.getUpdateDate());
			dto.setCreateUser(entities.getCreateUser());
			dto.setUpdateUser(entities.getUpdateUser());
			dto.setActive(entities.getActive());
			return dto;
		} else {
			return null;
		}
	}

	public static UserEntity toEntity(final UserResource dto) {

		if (dto != null) {
			var entities = new UserEntity();

			entities.setId(dto.getId());
			entities.setName(dto.getName());
			entities.setEmail(dto.getEmail());
			entities.setPhoto(dto.getPhoto());
			entities.setUser_name(dto.getUser_name());
			entities.setDocument(dto.getDocument());
			entities.setCreateDate(dto.getCreateDate());
			entities.setUpdateDate(dto.getUpdateDate());
			entities.setCreateUser(dto.getCreateUser());
			entities.setUpdateUser(dto.getUpdateUser());
			entities.setActive(dto.getActive());

			return entities;
		} else {

			return null;

		}

	}

	public static UserResource toDto(final Optional<UserEntity> optional) {
		return optional.isPresent() ? toDto(optional.get()) : null;
	}

	public static Optional<UserResource> toOptionalDto(final Optional<UserEntity> optional) {
		return Optional.ofNullable(toDto(optional));
	}

	public static List<UserResource> listToDto(final List<UserEntity> entities) {
		List<UserResource> listResource = new ArrayList<UserResource>();

		entities.forEach(address -> {
			listResource.add(toDto(address));
		});

		return listResource;
	}

}
