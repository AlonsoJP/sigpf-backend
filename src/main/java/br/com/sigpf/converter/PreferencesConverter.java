package br.com.sigpf.converter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.sigpf.entity.PreferencesEntity;
import br.com.sigpf.enums.PaymentTypeEnum;
import br.com.sigpf.resource.PreferencesResource;
import lombok.var;

public class PreferencesConverter {

	public static PreferencesResource toDto(final PreferencesEntity entities) {

		if (entities != null) {
			final var dto = new PreferencesResource();

			dto.setId(entities.getId());
			dto.setEmail(entities.getEmail());
			dto.setKmMin(entities.getKmMin());
			dto.setKmMax(entities.getKmMax());
			dto.setHourOut(entities.getHourOut());
			dto.setHourRegress(entities.getHourRegress());
			dto.setHourComing(entities.getHourComing());
			dto.setLocaleOut(entities.getLocaleOut());
			dto.setLocaleRegress(entities.getLocaleRegress());
			dto.setAmmount(entities.getAmmount());
			dto.setContractValue(entities.getContractValue());
			dto.setContractRecission(entities.getContractRecission());
			dto.setContributionBank(entities.getContributionBank());
			dto.setCommunicationRate(entities.getCommunicationRate());
			dto.setCommunicationRateDate(entities.getCommunicationRateDate());
			dto.setPenality(entities.getPenality());
			dto.setCalculationType(entities.getCalculationType().getDescricao());
			dto.setExpirationDate(entities.getExpirationDate());
			dto.setNotificationEndMounth(entities.getNotificationEndMounth());
			dto.setNotificationCreateMounth(entities.getNotificationCreateMounth());
			dto.setNotificationStatsSystem(entities.getNotificationStatsSystem());
			dto.setCreateDate(entities.getCreateDate());
			dto.setUpdateDate(entities.getUpdateDate());
			dto.setCreateUser(entities.getCreateUser());
			dto.setUpdateUser(entities.getUpdateUser());

			return dto;
		} else {
			return null;
		}
	}

	public static PreferencesEntity toEntity(final PreferencesResource dto) {

		if (dto != null) {
			var entities = new PreferencesEntity();

			entities.setId(dto.getId());
			entities.setEmail(dto.getEmail());
			entities.setKmMin(dto.getKmMin());
			entities.setKmMax(dto.getKmMax());
			entities.setHourOut(dto.getHourOut());
			entities.setHourRegress(dto.getHourRegress());
			entities.setHourComing(dto.getHourComing());
			entities.setLocaleOut(dto.getLocaleOut());
			entities.setLocaleRegress(dto.getLocaleRegress());
			entities.setAmmount(dto.getAmmount());
			entities.setContractValue(dto.getContractValue());
			entities.setContractRecission(dto.getContractRecission());
			entities.setContributionBank(dto.getContributionBank());
			entities.setCommunicationRate(dto.getCommunicationRate());
			entities.setCommunicationRateDate(dto.getCommunicationRateDate());
			entities.setPenality(dto.getPenality());
			entities.setCalculationType(PaymentTypeEnum.getByDescricao(dto.getCalculationType()));
			entities.setExpirationDate(dto.getExpirationDate());
			entities.setNotificationEndMounth(dto.getNotificationEndMounth());
			entities.setNotificationCreateMounth(dto.getNotificationCreateMounth());
			entities.setNotificationStatsSystem(dto.getNotificationStatsSystem());
			entities.setCreateDate(LocalDate.now());
			entities.setUpdateDate(LocalDate.now());
			entities.setCreateUser(dto.getCreateUser());
			entities.setUpdateUser(dto.getUpdateUser());

			return entities;
		} else {

			return null;

		}

	}

	public static PreferencesResource toDto(final Optional<PreferencesEntity> optional) {
		return optional.isPresent() ? toDto(optional.get()) : null;
	}

	public static Optional<PreferencesResource> toOptionalDto(final Optional<PreferencesEntity> optional) {
		return Optional.ofNullable(toDto(optional));
	}

	public static List<PreferencesResource> listToDto(final List<PreferencesEntity> entities) {
		List<PreferencesResource> listResource = new ArrayList<PreferencesResource>();

		entities.forEach(address -> {
			listResource.add(toDto(address));
		});

		return listResource;
	}

}
