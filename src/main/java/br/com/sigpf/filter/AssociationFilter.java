package br.com.sigpf.filter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "associationFilter")
@XmlAccessorType(XmlAccessType.FIELD)
public class AssociationFilter {

	private Long id;
	private String owner;
	private String fantasyName;
	private String cnpj;

}
