package br.com.sigpf.filter;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "memberAssociationFilter")
@XmlAccessorType(XmlAccessType.FIELD)
public class MemberAssociationFilter {

	private Long id;
	private String name;
	private String cpf;
	private String rg;
	private String email;
	private Date birthDate;
	private String zipCode;
	private String city;
	private String complement;
	private String numberHouse;
	private String uf;
	private Date createDate;
	private Date updateDate;
	private String createUser;
	private String updateUser;
	private String role;
	private String subRole;
	private Boolean active;
}
