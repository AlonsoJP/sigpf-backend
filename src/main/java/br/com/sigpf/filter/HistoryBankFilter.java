package br.com.sigpf.filter;

import java.time.LocalDateTime;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "historyBankFilter")
@XmlAccessorType(XmlAccessType.FIELD)
public class HistoryBankFilter {

	private Long id;
	private Long id_transaction;
	private String type_transaction;
	private String protocol_transaction;
	private Double amount_transaction;
	private String exercise_bank;
	private Double sale_old;
	private Double sale_new;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime dateBegin;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime dateEnd;
	private String createUser;
	private String updateUser;

}
