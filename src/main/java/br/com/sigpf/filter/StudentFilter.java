package br.com.sigpf.filter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "studentFilter")
@XmlAccessorType(XmlAccessType.FIELD)
public class StudentFilter {

	private Long id;
	private String name;
	private String cpf;
	private String rg;
	private String ra;
	private String email;

	private Long courseId;
	private String nameCourse;

}
