package br.com.sigpf.filter;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "supportCityhallFilter")
@XmlAccessorType(XmlAccessType.FIELD)
public class SupportCityhallFilter {

	private Long id;
	private double ammount;
	private String description;
	private String mounth_reference;
	private Date createDate;
	private Date updateDate;
	private String createUser;
	private String updateUser;

}
