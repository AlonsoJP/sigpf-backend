package br.com.sigpf.filter;

import java.time.LocalDate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "recipeFilter")
@XmlAccessorType(XmlAccessType.FIELD)
public class RecipeFilter {

	private Long id;
	private String name;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dateBegin;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dateEnd;

}
