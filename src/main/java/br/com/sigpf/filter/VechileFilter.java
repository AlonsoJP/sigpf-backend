package br.com.sigpf.filter;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "vechileFilter")
@XmlAccessorType(XmlAccessType.FIELD)
public class VechileFilter {

	private Long id;
	private String name;
	private String identification;
	private String description;
	private String model;
	private Integer km_l;
	private Integer bench;
	private Date createDate;
	private Date updateDate;
	private String createUser;
	private String updateUser;
	private Boolean active;

}
