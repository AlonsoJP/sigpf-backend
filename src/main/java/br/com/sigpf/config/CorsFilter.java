//package br.com.sigpf.config;
//
//import java.io.IOException;
//
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.springframework.core.Ordered;
//import org.springframework.core.annotation.Order;
//import org.springframework.stereotype.Component;
//import org.springframework.web.filter.OncePerRequestFilter;
//
//@Component
//@Order(Ordered.HIGHEST_PRECEDENCE)
//public class CorsFilter extends OncePerRequestFilter {
//
//	// private static final String originPermit = "http://localhost:8080";
//	// private static final String originPermit2 = "http://localhost";
//
//	@Override
//	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
//			throws ServletException, IOException {
//		// response.addHeader("Access-Control-Allow-Origin", originPermit);
//		// response.addHeader("Access-Control-Allow-Origin", originPermit2);// SE
//		// DESEJAR SER ACESSADO POR QUALQUER LUGAR,
//		// USASR *
//
//		if (request.getHeader("Access-Control-Request-Method") != null && "OPTIONS".equals(request.getMethod())) {
//			response.addHeader("Access-Control-Allow-Origin", "http://localhost:8080");
//			response.addHeader("Access-Control-Allow-Credentials", "true");
//			response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
//			response.addHeader("Access-Control-Allow-Headers",
//					"X-Requested-With, Content-Type, Authorization, Accept, Origin");
//			response.addHeader("Access-Control-Max-Age", "1");
//		} else {
//
//			filterChain.doFilter(request, response);
//		}
//
//	}
//
//}
