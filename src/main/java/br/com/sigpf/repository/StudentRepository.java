package br.com.sigpf.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.query.Param;

import br.com.sigpf.entity.StudentEntity;
import br.com.sigpf.enums.PaymentTypeEnum;

public interface StudentRepository extends JpaRepository<StudentEntity, Long>, JpaSpecificationExecutor<StudentEntity> {

	public Long countByActiveAndPaymentType(@Param("active") Boolean active,
			@Param("payment_type") PaymentTypeEnum payment_type);

	public Long countByActive(@Param("active") Boolean active);

}
