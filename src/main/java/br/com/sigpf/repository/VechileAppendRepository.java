package br.com.sigpf.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import br.com.sigpf.entity.VechileAppendEntity;

public interface VechileAppendRepository
		extends JpaRepository<VechileAppendEntity, Long>, JpaSpecificationExecutor<VechileAppendEntity> {

}
