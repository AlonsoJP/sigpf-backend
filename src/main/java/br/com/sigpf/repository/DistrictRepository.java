package br.com.sigpf.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import br.com.sigpf.entity.DistrictEntity;

public interface DistrictRepository
		extends JpaRepository<DistrictEntity, Long>, JpaSpecificationExecutor<DistrictEntity> {

}
