package br.com.sigpf.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import br.com.sigpf.entity.StudentAppendEntity;

public interface StudentAppendRepository
		extends JpaRepository<StudentAppendEntity, Long>, JpaSpecificationExecutor<StudentAppendEntity> {

}
