package br.com.sigpf.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import br.com.sigpf.entity.StreetEntity;

public interface StreetRepository
		extends JpaRepository<StreetEntity, Long>, JpaSpecificationExecutor<StreetEntity> {

}
