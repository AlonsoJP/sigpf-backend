package br.com.sigpf.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import br.com.sigpf.entity.InputEntity;

public interface InputRepository extends JpaRepository<InputEntity, Long>, JpaSpecificationExecutor<InputEntity> {

	@Query(value = "select sum(input.amount_transaction) from input where input.type_transaction='ENTRADA'", nativeQuery = true)
	Double findSumEntry();

	@Query(value = "select sum(input.amount_transaction) from input where input.type_transaction='CANCELADO'", nativeQuery = true)
	Double findSumCancel();

	@Query(value = "select sum(input.amount_transaction) from input where input.type_transaction='ENTRADA' and date(input.create_date) = curdate()", nativeQuery = true)
	Double findSumCurrentDayOk();

	@Query(value = "select sum(input.amount_transaction) from input where input.type_transaction='CANCELADO' and date(input.create_date) = curdate()", nativeQuery = true)
	Double findSumCurrentDayCancel();

	@Query(value = "select sum(input.amount_transaction) from input where input.type_transaction='ENTRADA' and month(input.create_date) = month(curdate())", nativeQuery = true)
	Double findSumCurrentMonthOk();

	@Query(value = "select sum(input.amount_transaction) from input where input.type_transaction='CANCELADO' and month(input.create_date) = month(curdate())", nativeQuery = true)
	Double findSumCurrentMonthCancel();

	@Query(value = "select sum(input.amount_transaction) from input where input.type_transaction='ENTRADA' and month(input.create_date) = month(curdate())-1", nativeQuery = true)
	Double findSumLastMonthOk();

	@Query(value = "select sum(input.amount_transaction) from input where input.type_transaction='CANCELADO' and month(input.create_date) = month(curdate())-1", nativeQuery = true)
	Double findSumLastMonthCancel();

	@Query(value = "select sum(input.amount_transaction) from input where input.type_transaction='ENTRADA' and year(input.create_date) = year(curdate())", nativeQuery = true)
	Double findSumCurrentYearOk();

	@Query(value = "select sum(input.amount_transaction) from input where input.type_transaction='CANCELADO' and year(input.create_date) = year(curdate())", nativeQuery = true)
	Double findSumCurrentYearCancel();

}
