package br.com.sigpf.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import br.com.sigpf.entity.CompanyAppendEntity;

public interface CompanyAppendRepository
		extends JpaRepository<CompanyAppendEntity, Long>, JpaSpecificationExecutor<CompanyAppendEntity> {

}
