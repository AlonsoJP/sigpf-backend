package br.com.sigpf.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import br.com.sigpf.entity.DriverAppendEntity;

public interface DriverAppendRepository
		extends JpaRepository<DriverAppendEntity, Long>, JpaSpecificationExecutor<DriverAppendEntity> {

}
