package br.com.sigpf.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import br.com.sigpf.entity.VechileEntity;

public interface VechileRepository extends JpaRepository<VechileEntity, Long>, JpaSpecificationExecutor<VechileEntity> {

}
