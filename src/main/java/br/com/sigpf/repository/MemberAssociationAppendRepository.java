package br.com.sigpf.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import br.com.sigpf.entity.MemberAssociationAppendEntity;

public interface MemberAssociationAppendRepository extends JpaRepository<MemberAssociationAppendEntity, Long>,
		JpaSpecificationExecutor<MemberAssociationAppendEntity> {

}
