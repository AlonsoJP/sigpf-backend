package br.com.sigpf.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import br.com.sigpf.entity.UniversityEntity;

public interface UniversityRepository
		extends JpaRepository<UniversityEntity, Long>, JpaSpecificationExecutor<UniversityEntity> {

}
