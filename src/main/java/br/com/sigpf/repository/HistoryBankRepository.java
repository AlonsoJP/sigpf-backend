package br.com.sigpf.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import br.com.sigpf.entity.HistoryBankEntity;

public interface HistoryBankRepository
		extends JpaRepository<HistoryBankEntity, Long>, JpaSpecificationExecutor<HistoryBankEntity> {

}
