package br.com.sigpf.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import br.com.sigpf.entity.AssociationAppendEntity;

public interface AssociationAppendRepository
		extends JpaRepository<AssociationAppendEntity, Long>, JpaSpecificationExecutor<AssociationAppendEntity> {

}
