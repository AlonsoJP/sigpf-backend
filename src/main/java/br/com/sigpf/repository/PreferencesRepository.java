package br.com.sigpf.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import br.com.sigpf.entity.PreferencesEntity;

public interface PreferencesRepository
		extends JpaRepository<PreferencesEntity, Long>, JpaSpecificationExecutor<PreferencesEntity> {

}
