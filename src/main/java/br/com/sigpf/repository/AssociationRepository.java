package br.com.sigpf.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import br.com.sigpf.entity.AssociationEntity;

public interface AssociationRepository
		extends JpaRepository<AssociationEntity, Long>, JpaSpecificationExecutor<AssociationEntity> {

}
