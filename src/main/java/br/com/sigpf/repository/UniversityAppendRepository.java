package br.com.sigpf.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import br.com.sigpf.entity.UniversityAppendEntity;

public interface UniversityAppendRepository
		extends JpaRepository<UniversityAppendEntity, Long>, JpaSpecificationExecutor<UniversityAppendEntity> {

}
