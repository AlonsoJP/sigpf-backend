package br.com.sigpf.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import br.com.sigpf.entity.SupportCityhallEntity;

public interface SupportCityhallRepository
		extends JpaRepository<SupportCityhallEntity, Long>, JpaSpecificationExecutor<SupportCityhallEntity> {

}
