package br.com.sigpf.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import br.com.sigpf.entity.VersionEntity;

public interface VersionRepository extends JpaRepository<VersionEntity, Long>, JpaSpecificationExecutor<VersionEntity> {

	public VersionEntity findTopByOrderByIdDesc();

	public List<VersionEntity> findTop10ByOrderByIdDesc();

}
