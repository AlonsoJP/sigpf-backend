package br.com.sigpf.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import br.com.sigpf.entity.OutputEntity;

public interface OutputRepository extends JpaRepository<OutputEntity, Long>, JpaSpecificationExecutor<OutputEntity> {

	@Query(value = "select sum(output.amount_transaction) from output where output.type_transaction='SAIDA'", nativeQuery = true)
	Double findSumEntry();

	@Query(value = "select sum(output.amount_transaction) from output where output.type_transaction='CANCELADO'", nativeQuery = true)
	Double findSumCancel();

	@Query(value = "select sum(output.amount_transaction) from output where output.type_transaction='SAIDA' and date(output.create_date) = curdate()", nativeQuery = true)
	Double findSumCurrentDayOk();

	@Query(value = "select sum(output.amount_transaction) from output where output.type_transaction='CANCELADO' and date(output.create_date) = curdate()", nativeQuery = true)
	Double findSumCurrentDayCancel();

	@Query(value = "select sum(output.amount_transaction) from output where output.type_transaction='SAIDA' and month(output.create_date) = month(curdate())", nativeQuery = true)
	Double findSumCurrentMonthOk();

	@Query(value = "select sum(output.amount_transaction) from output where output.type_transaction='CANCELADO' and month(output.create_date) = month(curdate())", nativeQuery = true)
	Double findSumCurrentMonthCancel();

	@Query(value = "select sum(output.amount_transaction) from output where output.type_transaction='SAIDA' and month(output.create_date) = month(curdate())-1", nativeQuery = true)
	Double findSumLastMonthOk();

	@Query(value = "select sum(output.amount_transaction) from output where output.type_transaction='CANCELADO' and month(output.create_date) = month(curdate())-1", nativeQuery = true)
	Double findSumLastMonthCancel();

	@Query(value = "select sum(output.amount_transaction) from output where output.type_transaction='SAIDA' and year(output.create_date) = year(curdate())", nativeQuery = true)
	Double findSumCurrentYearOk();

	@Query(value = "select sum(output.amount_transaction) from output where output.type_transaction='CANCELADO' and year(output.create_date) = year(curdate())", nativeQuery = true)
	Double findSumCurrentYearCancel();

}
