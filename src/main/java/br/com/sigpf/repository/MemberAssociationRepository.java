package br.com.sigpf.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import br.com.sigpf.entity.MemberAssociationEntity;

public interface MemberAssociationRepository
		extends JpaRepository<MemberAssociationEntity, Long>, JpaSpecificationExecutor<MemberAssociationEntity> {

}
