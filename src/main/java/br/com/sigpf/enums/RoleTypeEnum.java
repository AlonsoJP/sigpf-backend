package br.com.sigpf.enums;

import org.apache.commons.lang3.StringUtils;

public enum RoleTypeEnum {

	PRESIDENTE(1, "Presidente"),
	VICEPRESIDENTE(2, "Vice-Presidente"),
	PRIMEIROSECRETARIO(3,"Primeiro-Secretário"),
	SEGUNDOSECRETARIO(4,"Segundo-Secretário"),
	PRIMEIROTESOUREIRO(5, "Primeiro-Tesoureiro"), 
	SEGUNDOTESOUREIRO(6, "Segundo-Tesoureiro");

	private int id;
	private String descricao;

	private RoleTypeEnum(int id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}

	public static RoleTypeEnum getById(int id) {
		for (RoleTypeEnum type : RoleTypeEnum.values()) {
			if (type.id == id) {
				return type;
			}
		}
		return null;
	}

	public static RoleTypeEnum getByDescricao(String desc) {
		if (StringUtils.isNotEmpty(desc)) {
			for (RoleTypeEnum type : RoleTypeEnum.values()) {
				if (type.getDescricao().equals(desc)) {
					return type;
				}
			}
		}
		return null;
	}

	public String getDescricao() {
		return descricao;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}

}
