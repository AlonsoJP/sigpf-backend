package br.com.sigpf.enums;

import org.apache.commons.lang3.StringUtils;

public enum TypeTransactionEnum {

	ENTRADA(1, "Entrada"), SAIDA(2, "Saída"), CANCELADO(3, "Cancelado");

	private int id;
	private String descricao;

	private TypeTransactionEnum(int id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}

	public static TypeTransactionEnum getById(int id) {
		for (TypeTransactionEnum type : TypeTransactionEnum.values()) {
			if (type.id == id) {
				return type;
			}
		}
		return null;
	}

	public static TypeTransactionEnum getByDescricao(String desc) {
		if (StringUtils.isNotEmpty(desc)) {
			for (TypeTransactionEnum type : TypeTransactionEnum.values()) {
				if (type.getDescricao().equals(desc)) {
					return type;
				}
			}
		}
		return null;
	}

	public String getDescricao() {
		return descricao;
	}

	public int getId() {
		return id;
	}

}
