package br.com.sigpf.enums;

import org.apache.commons.lang3.StringUtils;

public enum PaymentTypeEnum {

	INTEGRAL(1, "Integral"), PARCIAL(2, "Parcial");

	private int id;
	private String descricao;

	private PaymentTypeEnum(int id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}

	public static PaymentTypeEnum getById(int id) {
		for (PaymentTypeEnum type : PaymentTypeEnum.values()) {
			if (type.id == id) {
				return type;
			}
		}
		return null;
	}

	public static PaymentTypeEnum getByDescricao(String desc) {
		if (StringUtils.isNotEmpty(desc)) {
			for (PaymentTypeEnum type : PaymentTypeEnum.values()) {
				if (type.getDescricao().equals(desc)) {
					return type;
				}
			}
		}
		return null;
	}

	public String getDescricao() {
		return descricao;
	}

	public int getId() {
		return id;
	}

}
