package br.com.sigpf.enums;

import org.apache.commons.lang3.StringUtils;

public enum SubRoleTypeEnum {

	VICE(1, "Vice"), PRIMEIRO(2, "Primeiro"), SEGUNDO(3, "Segundo"), UNICO(4, "Unico");

	private int id;
	private String descricao;

	private SubRoleTypeEnum(int id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}

	public static SubRoleTypeEnum getById(int id) {
		for (SubRoleTypeEnum type : SubRoleTypeEnum.values()) {
			if (type.id == id) {
				return type;
			}
		}
		return null;
	}

	public static SubRoleTypeEnum getByDescricao(String desc) {
		if (StringUtils.isNotEmpty(desc)) {
			for (SubRoleTypeEnum type : SubRoleTypeEnum.values()) {
				if (type.getDescricao().equals(desc)) {
					return type;
				}
			}
		}
		return null;
	}

	public String getDescricao() {
		return descricao;
	}

	public int getId() {
		return id;
	}

}
