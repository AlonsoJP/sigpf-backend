package br.com.sigpf.resource;

import java.time.LocalDate;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class UniversityAppendResource {

	private Long id;
	private String description;
	private String path;
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate create_date;
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate update_date;
	private String createUser;
	private String updateUser;
	private UniversityResource university;

}
