package br.com.sigpf.resource;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class StudentResource {

	private Long id;
	private String name;
	private String photo;
	private String cpf;
	private String rg;
	private String ra;
	private String email;
	private String phone1;
	private String phone2;
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate birth_date;
	private String user_student;
	private String zip_code;
	private String city;
	private String complement;
	private Integer number_house;
	private String uf;
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate createDate;
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate updateDate;
	private String createUser;
	private String updateUser;
	private Boolean active;
	private String paymentType;
	private CourseResource course;
	private DistrictResource district;
	private StreetResource street;
	private List<StudentAppendResource> student;

}
