package br.com.sigpf.resource;

import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class InputResource {

	private Long id;
	private String type_transaction;
	private Double amount_transaction;
	private String protocol_transaction;
	private String description;
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDateTime createDate;
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDateTime updateDate;
	private String createUser;
	private String updateUser;
	private RecipeResource recipe;

}
