package br.com.sigpf.resource;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class MemberAssociationResource {

	private Long id;
	private String name;
	private String photo;
	private String cpf;
	private String rg;
	private String email;
	private String phone1;
	private String phone2;
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate birthDate;
	private String zipCode;
	private String city;
	private String complement;
	private String numberHouse;
	private String uf;
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate createDate;
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate updateDate;
	private String createUser;
	private String updateUser;
	private String role;
//	private String subRole;
	private Boolean active;
	private DistrictResource district;
	private StreetResource street;
	private List<MemberAssociationAppendResource> append;

}
