package br.com.sigpf.resource;

import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class HistoryBankResource {

	private Long id;
	private Long id_transaction;
	private String type_transaction;
	private String protocol_transaction;
	private Double amount_transaction;
	private String exercise_bank;
	private Double sale_old;
	private Double sale_new;
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDateTime createDate;
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDateTime updateDate;
	private String createUser;
	private String updateUser;
	private BankResource bank;

}
