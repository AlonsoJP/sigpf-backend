package br.com.sigpf.resource;

import java.time.LocalDate;
import java.time.LocalTime;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class PreferencesResource {

	private Long id;
	private String email;
	private Double kmMin;
	private Double kmMax;
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalTime hourOut;
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalTime hourRegress;
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalTime hourComing;
	private String localeOut;
	private String localeRegress;
	private Double ammount;
	private Double contractValue;
	private Double contractRecission;
	private Double contributionBank;
	private Double communicationRate;
	private Integer communicationRateDate;
	private Double penality;
	private String calculationType;
	private Integer expirationDate;
	private Boolean notificationEndMounth;
	private Boolean notificationCreateMounth;
	private Boolean notificationStatsSystem;// TODO remover do banco e daqui
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate createDate;
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate updateDate;
	private String createUser;
	private String updateUser;

}
