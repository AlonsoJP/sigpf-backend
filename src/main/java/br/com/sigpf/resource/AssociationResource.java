package br.com.sigpf.resource;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class AssociationResource {

	private Long id;
	private String owner;
	private String fantasyName;
	private String cnpj;
	private String email;
	private String phone;
	private String zip_code;
	private String city;
	private String complement;
	private String number_house;
	private String uf;
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate createDate;
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate updateDate;
	private String createUser;
	private String updateUser;
	private DistrictResource district;
	private StreetResource street;
	private List<AssociationAppendResource> association;

}
