package br.com.sigpf.resource;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class DriverResource {

	private Long id;
	private String name;
	private String photo;
	private String cpf;
	private String rg;
	private String license;
	private String email;
	private String phone1;
	private String phone2;
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate birth_date;
	private String zip_code;
	private String city;
	private String complement;
	private Integer number_house;
	private String uf;
	private Boolean active;
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate createDate;
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate updateDate;
	private String createUser;
	private String updateUser;
	private DistrictResource district;
	private StreetResource street;
	private List<DriverAppendResource> driverAppend;

}
