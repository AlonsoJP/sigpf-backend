package br.com.sigpf.resource;

import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class BankResource {

	private Long id;
	private Double sale;
	private String exercise_bank;
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDateTime createDate;
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDateTime updateDate;
	private String createUser;
	private String updateUser;
	private Boolean active;
	private List<HistoryBankResource> append;

}
