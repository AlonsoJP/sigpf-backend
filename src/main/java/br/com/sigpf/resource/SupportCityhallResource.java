package br.com.sigpf.resource;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class SupportCityhallResource {

	private Long id;
	private Double ammount;
	private String description;
	private Integer mounth_reference;
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate createDate;
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate updateDate;
	private String createUser;
	private String updateUser;
	private List<SupportCityhallAppendResource> append;

}
