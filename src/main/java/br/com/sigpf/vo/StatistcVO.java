package br.com.sigpf.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class StatistcVO {

	private Double calcTotal;
	private Double MensalityBase;
	private Double totalIntegralWithContributionWhitoutParcial;
	private Double totalIntegralWithContributionWithParcial;
	private Double totalIntegralWithoutContributionWithoutParcial;
	private Double totalParcialWithContribution;
	private Double estimateContribution;
	private Long totalStudentsActiveAndIntegral;
	private Long totalStudentsActiveAndParcial;
	private Long totalStudentsActive;
	private Long totalStudentsInactive;
	private Long totalStudents;

}
