package br.com.sigpf.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class ValueStatisticsVO {

	private Double totalGeral;
	private Double totalGeralCancel;
	private Double currentDay;
	private Double currentDayCancel;
	private Double currentMonth;
	private Double currentMonthCancel;
	private Double lastMonth;
	private Double lastMonthCancel;
	private Double currentYear;
	private Double currentYearCancel;

}
