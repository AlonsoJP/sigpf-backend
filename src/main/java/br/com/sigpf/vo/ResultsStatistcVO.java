package br.com.sigpf.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class ResultsStatistcVO {

	private Double gainDay;
	private Double gainResultDay;
	private Double gainMonth;
	private Double gainResultMonth;
	private Double gainYear;
	private Double gainResultYear;
	private Double lossDay;
	private Double lossMonth;
	private Double lossYear;

}
