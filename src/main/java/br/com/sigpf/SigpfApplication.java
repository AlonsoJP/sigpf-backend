package br.com.sigpf;

/**
 * @author Zeck
 * 
 * @version 1.0
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@SpringBootApplication
@EnableResourceServer
@EnableScheduling
public class SigpfApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(SigpfApplication.class, args);

	}

}
