package br.com.sigpf.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import br.com.sigpf.entity.DriverAppendEntity;
import br.com.sigpf.entity.DriverEntity;

@SuppressWarnings("serial")
public class DriverAppendSpecification {

	private static final String ID = "id";
	private static final String DESCRIPTION = "description";
	private static final String DRIVER_ID = "id";

	public static Specification<DriverAppendEntity> isNotNullId() {
		return new Specification<DriverAppendEntity>() {

			@Override
			public Predicate toPredicate(Root<DriverAppendEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {

				return cb.isNotNull(root.get(ID));

			}
		};
	}

	public static Specification<DriverAppendEntity> equalId(Long id) {
		return new Specification<DriverAppendEntity>() {

			@Override
			public Predicate toPredicate(Root<DriverAppendEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(ID), id);
			}
		};
	}

	public static Specification<DriverAppendEntity> likeDescription(String description) {
		return new Specification<DriverAppendEntity>() {

			@Override
			public Predicate toPredicate(Root<DriverAppendEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(DESCRIPTION), StringUtils.join("%", description, "%"));
			}

		};
	}

	// referentes a tabela relacionada
	public static Specification<DriverAppendEntity> equalDriver(Long driverId) {
		return new Specification<DriverAppendEntity>() {
			@Override
			public Predicate toPredicate(Root<DriverAppendEntity> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Join<DriverAppendEntity, DriverEntity> jCourse = root.join("driver", JoinType.INNER);
				return cb.equal(jCourse.get(DRIVER_ID), driverId);
			}
		};
	}

}
