package br.com.sigpf.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import br.com.sigpf.entity.PreferencesEntity;

@SuppressWarnings("serial")
public class PreferencesSpecification {

	private static final String ID = "id";

	public static Specification<PreferencesEntity> isNotNullId() {
		return new Specification<PreferencesEntity>() {

			@Override
			public Predicate toPredicate(Root<PreferencesEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {

				return cb.isNotNull(root.get(ID));

			}
		};
	}

	public static Specification<PreferencesEntity> equalId(Long id) {
		return new Specification<PreferencesEntity>() {

			@Override
			public Predicate toPredicate(Root<PreferencesEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(ID), id);
			}
		};
	}
}
