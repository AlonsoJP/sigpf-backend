package br.com.sigpf.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import br.com.sigpf.entity.BankEntity;

@SuppressWarnings("serial")
public class BankSpecification {

	private static final String ID = "id";
	private static final String SALE = "sale";
	private static final String EXERCISEBANNK = "exercise_bank";

	public static Specification<BankEntity> isNotNullId() {
		return new Specification<BankEntity>() {

			@Override
			public Predicate toPredicate(Root<BankEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {

				return cb.isNotNull(root.get(ID));

			}
		};
	}

	public static Specification<BankEntity> equalId(Long id) {
		return new Specification<BankEntity>() {

			@Override
			public Predicate toPredicate(Root<BankEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(ID), id);
			}
		};
	}

	public static Specification<BankEntity> likeSale(String sale) {
		return new Specification<BankEntity>() {

			@Override
			public Predicate toPredicate(Root<BankEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(SALE), StringUtils.join("%", sale, "%"));
			}

		};
	}

	public static Specification<BankEntity> likeExercise(String exercise) {
		return new Specification<BankEntity>() {

			@Override
			public Predicate toPredicate(Root<BankEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(EXERCISEBANNK), StringUtils.join("%", exercise, "%"));
			}

		};
	}

}
