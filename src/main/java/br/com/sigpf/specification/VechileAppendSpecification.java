package br.com.sigpf.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import br.com.sigpf.entity.VechileAppendEntity;
import br.com.sigpf.entity.VechileEntity;

@SuppressWarnings("serial")
public class VechileAppendSpecification {

	private static final String ID = "id";
	private static final String DESCRIPTION = "description";
	private static final String VECHILE_ID = "id";

	public static Specification<VechileAppendEntity> isNotNullId() {
		return new Specification<VechileAppendEntity>() {

			@Override
			public Predicate toPredicate(Root<VechileAppendEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {

				return cb.isNotNull(root.get(ID));

			}
		};
	}

	public static Specification<VechileAppendEntity> equalId(Long id) {
		return new Specification<VechileAppendEntity>() {

			@Override
			public Predicate toPredicate(Root<VechileAppendEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(ID), id);
			}
		};
	}

	public static Specification<VechileAppendEntity> likeDescription(String description) {
		return new Specification<VechileAppendEntity>() {

			@Override
			public Predicate toPredicate(Root<VechileAppendEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(DESCRIPTION), StringUtils.join("%", description, "%"));
			}

		};
	}

	// referentes a tabela relacionada
	public static Specification<VechileAppendEntity> equalVechile(Long vechileId) {
		return new Specification<VechileAppendEntity>() {
			@Override
			public Predicate toPredicate(Root<VechileAppendEntity> root, CriteriaQuery<?> query,
					CriteriaBuilder cb) {
				Join<VechileAppendEntity, VechileEntity> jCourse = root.join("vechile", JoinType.INNER);
				return cb.equal(jCourse.get(VECHILE_ID), vechileId);
			}
		};
	}

}
