package br.com.sigpf.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import br.com.sigpf.entity.UniversityAppendEntity;
import br.com.sigpf.entity.UniversityEntity;

@SuppressWarnings("serial")
public class UniversityAppendSpecification {

	private static final String ID = "id";
	private static final String DESCRIPTION = "description";
	private static final String UNIVERSITY_ID = "id";

	public static Specification<UniversityAppendEntity> isNotNullId() {
		return new Specification<UniversityAppendEntity>() {

			@Override
			public Predicate toPredicate(Root<UniversityAppendEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {

				return cb.isNotNull(root.get(ID));

			}
		};
	}

	public static Specification<UniversityAppendEntity> equalId(Long id) {
		return new Specification<UniversityAppendEntity>() {

			@Override
			public Predicate toPredicate(Root<UniversityAppendEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(ID), id);
			}
		};
	}

	public static Specification<UniversityAppendEntity> likeDescription(String description) {
		return new Specification<UniversityAppendEntity>() {

			@Override
			public Predicate toPredicate(Root<UniversityAppendEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(DESCRIPTION), StringUtils.join("%", description, "%"));
			}

		};
	}

	// referentes a tabela relacionada
	public static Specification<UniversityAppendEntity> equalUniversity(Long universityId) {
		return new Specification<UniversityAppendEntity>() {
			@Override
			public Predicate toPredicate(Root<UniversityAppendEntity> root, CriteriaQuery<?> query,
					CriteriaBuilder cb) {
				Join<UniversityAppendEntity, UniversityEntity> jCourse = root.join("university", JoinType.INNER);
				return cb.equal(jCourse.get(UNIVERSITY_ID), universityId);
			}
		};
	}
}