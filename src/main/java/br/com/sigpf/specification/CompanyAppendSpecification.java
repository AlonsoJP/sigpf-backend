package br.com.sigpf.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import br.com.sigpf.entity.CompanyAppendEntity;
import br.com.sigpf.entity.CompanyEntity;

@SuppressWarnings("serial")
public class CompanyAppendSpecification {
	private static final String ID = "id";
	private static final String DESCRIPTION = "description";
	private static final String COMPANY_ID = "id";

	public static Specification<CompanyAppendEntity> isNotNullId() {
		return new Specification<CompanyAppendEntity>() {

			@Override
			public Predicate toPredicate(Root<CompanyAppendEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {

				return cb.isNotNull(root.get(ID));

			}
		};
	}

	public static Specification<CompanyAppendEntity> equalId(Long id) {
		return new Specification<CompanyAppendEntity>() {

			@Override
			public Predicate toPredicate(Root<CompanyAppendEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(ID), id);
			}
		};
	}

	public static Specification<CompanyAppendEntity> likeDescription(String description) {
		return new Specification<CompanyAppendEntity>() {

			@Override
			public Predicate toPredicate(Root<CompanyAppendEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(DESCRIPTION), StringUtils.join("%", description, "%"));
			}

		};
	}

	// referentes a tabela relacionada
	public static Specification<CompanyAppendEntity> equalCompany(Long companyId) {
		return new Specification<CompanyAppendEntity>() {
			@Override
			public Predicate toPredicate(Root<CompanyAppendEntity> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Join<CompanyAppendEntity, CompanyEntity> jCourse = root.join("company", JoinType.INNER);
				return cb.equal(jCourse.get(COMPANY_ID), companyId);
			}
		};
	}
}
