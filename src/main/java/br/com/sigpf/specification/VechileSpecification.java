package br.com.sigpf.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import br.com.sigpf.entity.VechileEntity;

@SuppressWarnings("serial")
public class VechileSpecification {

	private static final String ID = "id";
	private static final String NAME = "name";
	private static final String IDENTIFICATION = "identification";
	private static final String ACTIVE = "active";

	public static Specification<VechileEntity> isNotNullId() {
		return new Specification<VechileEntity>() {

			@Override
			public Predicate toPredicate(Root<VechileEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {

				return cb.isNotNull(root.get(ID));

			}
		};
	}

	public static Specification<VechileEntity> equalId(Long id) {
		return new Specification<VechileEntity>() {

			@Override
			public Predicate toPredicate(Root<VechileEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(ID), id);
			}
		};
	}

	public static Specification<VechileEntity> likeName(String name) {
		return new Specification<VechileEntity>() {

			@Override
			public Predicate toPredicate(Root<VechileEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(NAME), StringUtils.join("%", name, "%"));
			}

		};
	}

	public static Specification<VechileEntity> likeIdentification(String spec) {
		return new Specification<VechileEntity>() {

			@Override
			public Predicate toPredicate(Root<VechileEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(IDENTIFICATION), StringUtils.join("%", spec, "%"));
			}

		};
	}

	public static Specification<VechileEntity> equalActive(Boolean active) {
		return new Specification<VechileEntity>() {

			@Override
			public Predicate toPredicate(Root<VechileEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(ACTIVE), active);
			}

		};
	}

}
