package br.com.sigpf.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import br.com.sigpf.entity.CompanyEntity;

@SuppressWarnings("serial")
public class CompanySpecification {

	private static final String ID = "id";
	private static final String OWNER = "owner";
	private static final String FANTASYNAME = "fantay_name";
	private static final String CNPJ = "cnpj";

	public static Specification<CompanyEntity> isNotNullId() {
		return new Specification<CompanyEntity>() {

			@Override
			public Predicate toPredicate(Root<CompanyEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {

				return cb.isNotNull(root.get(ID));

			}
		};
	}

	public static Specification<CompanyEntity> equalId(Long id) {
		return new Specification<CompanyEntity>() {

			@Override
			public Predicate toPredicate(Root<CompanyEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(ID), id);
			}
		};
	}

	public static Specification<CompanyEntity> likeOwner(String owner) {
		return new Specification<CompanyEntity>() {

			@Override
			public Predicate toPredicate(Root<CompanyEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(OWNER), StringUtils.join("%", owner, "%"));
			}

		};
	}

	public static Specification<CompanyEntity> likeFantasyName(String fantasyName) {
		return new Specification<CompanyEntity>() {

			@Override
			public Predicate toPredicate(Root<CompanyEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(FANTASYNAME), StringUtils.join("%", fantasyName, "%"));
			}

		};
	}

	public static Specification<CompanyEntity> likeCnpj(String cnpj) {
		return new Specification<CompanyEntity>() {

			@Override
			public Predicate toPredicate(Root<CompanyEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(CNPJ), StringUtils.join("%", cnpj, "%"));
			}

		};
	}

}
