package br.com.sigpf.specification;

import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import br.com.sigpf.entity.HistoryBankEntity;
import br.com.sigpf.entity.MemberAssociationEntity;

@SuppressWarnings("serial")
public class MemberAssociationSpecification {

	private static final String ID = "id";
	private static final String NAME = "name";
	private static final String CPF = "cpf";
	private static final String RG = "rg";
	private static final String EMAIL = "email";
	private static final String CREATEDATE = "createDate";
	private static final String UPDATEDATE = "updateDate";
	private static final String CREATEUSER = "createUser";
	private static final String UPDATEUSER = "updateUser";
	private static final String ROLE = "role";
	private static final String SUBROLE = "subRole";
	private static final String ACTIVE = "active";

	public static Specification<MemberAssociationEntity> isNotNullId() {
		return new Specification<MemberAssociationEntity>() {

			@Override
			public Predicate toPredicate(Root<MemberAssociationEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {

				return cb.isNotNull(root.get(ID));

			}
		};
	}

	public static Specification<MemberAssociationEntity> equalId(Long id) {
		return new Specification<MemberAssociationEntity>() {

			@Override
			public Predicate toPredicate(Root<MemberAssociationEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(ID), id);
			}
		};
	}

	public static Specification<MemberAssociationEntity> likeName(String name) {
		return new Specification<MemberAssociationEntity>() {

			@Override
			public Predicate toPredicate(Root<MemberAssociationEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(NAME), StringUtils.join("%", name, "%"));
			}

		};
	}

	public static Specification<MemberAssociationEntity> likeCpf(String cpf) {
		return new Specification<MemberAssociationEntity>() {

			@Override
			public Predicate toPredicate(Root<MemberAssociationEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(CPF), StringUtils.join("%", cpf, "%"));
			}

		};
	}

	public static Specification<MemberAssociationEntity> likeRg(String rg) {
		return new Specification<MemberAssociationEntity>() {

			@Override
			public Predicate toPredicate(Root<MemberAssociationEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(RG), StringUtils.join("%", rg, "%"));
			}

		};
	}

	public static Specification<MemberAssociationEntity> likeEmail(String email) {
		return new Specification<MemberAssociationEntity>() {

			@Override
			public Predicate toPredicate(Root<MemberAssociationEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(EMAIL), StringUtils.join("%", email, "%"));
			}

		};
	}

	public static Specification<MemberAssociationEntity> likeRole(String role) {
		return new Specification<MemberAssociationEntity>() {

			@Override
			public Predicate toPredicate(Root<MemberAssociationEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(ROLE), StringUtils.join("%", role, "%"));
			}

		};
	}

	public static Specification<MemberAssociationEntity> likeSubRole(String subRole) {
		return new Specification<MemberAssociationEntity>() {

			@Override
			public Predicate toPredicate(Root<MemberAssociationEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(SUBROLE), StringUtils.join("%", subRole, "%"));
			}

		};
	}

	public static Specification<MemberAssociationEntity> equalActive(Boolean active) {
		return new Specification<MemberAssociationEntity>() {

			@Override
			public Predicate toPredicate(Root<MemberAssociationEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(ACTIVE), active);
			}

		};
	}

	public static Specification<MemberAssociationEntity> likeCreateDate(Date createDate) {
		return new Specification<MemberAssociationEntity>() {

			@Override
			public Predicate toPredicate(Root<MemberAssociationEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(CREATEDATE), createDate);
			}

		};
	}

	public static Specification<MemberAssociationEntity> likeUpdateDate(Date updateDate) {
		return new Specification<MemberAssociationEntity>() {

			@Override
			public Predicate toPredicate(Root<MemberAssociationEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(UPDATEDATE), updateDate);
			}

		};
	}

	public static Specification<MemberAssociationEntity> likeCreateUser(String createUser) {
		return new Specification<MemberAssociationEntity>() {

			@Override
			public Predicate toPredicate(Root<MemberAssociationEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(CREATEUSER), StringUtils.join("%", createUser, "%"));
			}

		};
	}

	public static Specification<MemberAssociationEntity> likeUpdateUser(String updateUser) {
		return new Specification<MemberAssociationEntity>() {

			@Override
			public Predicate toPredicate(Root<MemberAssociationEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(UPDATEUSER), StringUtils.join("%", updateUser, "%"));
			}

		};
	}

	public static Specification<HistoryBankEntity> betweenCreateDate(Date createDateBegin, Date createDateEnd) {
		return new Specification<HistoryBankEntity>() {

			@Override
			public Predicate toPredicate(Root<HistoryBankEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.between(root.get(CREATEDATE), createDateBegin, createDateEnd);
			}

		};
	}

}