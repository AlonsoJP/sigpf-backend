package br.com.sigpf.specification;

import java.time.LocalDateTime;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import br.com.sigpf.entity.InputEntity;
import br.com.sigpf.enums.TypeTransactionEnum;

@SuppressWarnings("serial")
public class InputSpecification {

	private static final String ID = "id";
	private static final String TYPETRANSACTION = "type_transaction";
	private static final String AMOUNTTRANSACTION = "amount_transaction";
	private static final String PROTOCOLTRANSACTION = "protocol_transaction";
	private static final String DESCRIPTION = "description";
	private static final String CREATEUSER = "createUser";
	private static final String UPDATEUSER = "updateUser";
	private static final String DATE = "createDate";

	public static Specification<InputEntity> isNotNullId() {
		return new Specification<InputEntity>() {

			@Override
			public Predicate toPredicate(Root<InputEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {

				return cb.isNotNull(root.get(ID));

			}
		};
	}

	public static Specification<InputEntity> equalId(Long id) {
		return new Specification<InputEntity>() {

			@Override
			public Predicate toPredicate(Root<InputEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(ID), id);
			}
		};
	}

	public static Specification<InputEntity> equalAmount(Double value) {
		return new Specification<InputEntity>() {

			@Override
			public Predicate toPredicate(Root<InputEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(AMOUNTTRANSACTION), value);
			}
		};
	}

	public static Specification<InputEntity> likeTypeTransaction(String typeTransaction) {
		return new Specification<InputEntity>() {

			@Override
			public Predicate toPredicate(Root<InputEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(TYPETRANSACTION),
						StringUtils.join("%", TypeTransactionEnum.getByDescricao(typeTransaction).toString(), "%"));
			}

		};
	}

	public static Specification<InputEntity> likeAmountTransaction(Double amount_transaction) {
		return new Specification<InputEntity>() {

			@Override
			public Predicate toPredicate(Root<InputEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(AMOUNTTRANSACTION), amount_transaction);
			}

		};
	}

	public static Specification<InputEntity> likeProtocolTransaction(String protocol_transaction) {
		return new Specification<InputEntity>() {

			@Override
			public Predicate toPredicate(Root<InputEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(PROTOCOLTRANSACTION), StringUtils.join("%", protocol_transaction, "%"));
			}

		};
	}

	public static Specification<InputEntity> likeDescription(String description) {
		return new Specification<InputEntity>() {

			@Override
			public Predicate toPredicate(Root<InputEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(DESCRIPTION), StringUtils.join("%", description, "%"));
			}

		};
	}

	public static Specification<InputEntity> likeCreateUser(String createUser) {
		return new Specification<InputEntity>() {

			@Override
			public Predicate toPredicate(Root<InputEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(CREATEUSER), StringUtils.join("%", createUser, "%"));
			}

		};
	}

	public static Specification<InputEntity> likeUpdateUser(String updateUser) {
		return new Specification<InputEntity>() {

			@Override
			public Predicate toPredicate(Root<InputEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(UPDATEUSER), StringUtils.join("%", updateUser, "%"));
			}

		};
	}

	public static Specification<InputEntity> createDateLessThanOrEqual(LocalDateTime date) {
		return new Specification<InputEntity>() {
			@Override
			public Predicate toPredicate(Root<InputEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.lessThanOrEqualTo(root.get(DATE), date);
			}

		};
	}

	public static Specification<InputEntity> createGreaterThanOrEqual(LocalDateTime date) {
		return new Specification<InputEntity>() {
			@Override
			public Predicate toPredicate(Root<InputEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.greaterThanOrEqualTo(root.get(DATE), date);
			}

		};
	}

}
