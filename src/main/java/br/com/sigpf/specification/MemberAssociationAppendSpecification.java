package br.com.sigpf.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import br.com.sigpf.entity.MemberAssociationAppendEntity;
import br.com.sigpf.entity.MemberAssociationEntity;

@SuppressWarnings("serial")
public class MemberAssociationAppendSpecification {

	private static final String ID = "id";
	private static final String DESCRIPTION = "description";
	private static final String MEMBER_ID = "id";

	public static Specification<MemberAssociationAppendEntity> isNotNullId() {
		return new Specification<MemberAssociationAppendEntity>() {

			@Override
			public Predicate toPredicate(Root<MemberAssociationAppendEntity> root, CriteriaQuery<?> cq,
					CriteriaBuilder cb) {

				return cb.isNotNull(root.get(ID));

			}
		};
	}

	public static Specification<MemberAssociationAppendEntity> equalId(Long id) {
		return new Specification<MemberAssociationAppendEntity>() {

			@Override
			public Predicate toPredicate(Root<MemberAssociationAppendEntity> root, CriteriaQuery<?> cq,
					CriteriaBuilder cb) {
				return cb.equal(root.get(ID), id);
			}
		};
	}

	public static Specification<MemberAssociationAppendEntity> likeDescription(String description) {
		return new Specification<MemberAssociationAppendEntity>() {

			@Override
			public Predicate toPredicate(Root<MemberAssociationAppendEntity> root, CriteriaQuery<?> cq,
					CriteriaBuilder cb) {
				return cb.like(root.get(DESCRIPTION), StringUtils.join("%", description, "%"));
			}

		};
	}

	// referentes a tabela relacionada
	public static Specification<MemberAssociationAppendEntity> equalMember(Long memberId) {
		return new Specification<MemberAssociationAppendEntity>() {
			@Override
			public Predicate toPredicate(Root<MemberAssociationAppendEntity> root, CriteriaQuery<?> query,
					CriteriaBuilder cb) {
				Join<MemberAssociationAppendEntity, MemberAssociationEntity> jCourse = root.join("member_association", JoinType.INNER);
				return cb.equal(jCourse.get(MEMBER_ID), memberId);
			}
		};
	}

}
