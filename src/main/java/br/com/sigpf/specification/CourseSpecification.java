package br.com.sigpf.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import br.com.sigpf.entity.CourseEntity;

@SuppressWarnings("serial")
public class CourseSpecification {

	private static final String CODIGO = "codigo";
	private static final String NAME = "name";
	private static final String AREA = "area";

	public static Specification<CourseEntity> isNotNullId() {
		return new Specification<CourseEntity>() {

			@Override
			public Predicate toPredicate(Root<CourseEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {

				return cb.isNotNull(root.get(CODIGO));

			}
		};
	}

	public static Specification<CourseEntity> equalId(Long id) {
		return new Specification<CourseEntity>() {

			@Override
			public Predicate toPredicate(Root<CourseEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(CODIGO), id);
			}
		};
	}

	public static Specification<CourseEntity> likeName(String name) {
		return new Specification<CourseEntity>() {

			@Override
			public Predicate toPredicate(Root<CourseEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(NAME), StringUtils.join("%", name, "%"));
			}

		};
	}

	public static Specification<CourseEntity> likeArea(String area) {
		return new Specification<CourseEntity>() {

			@Override
			public Predicate toPredicate(Root<CourseEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(AREA), StringUtils.join("%", area, "%"));
			}

		};
	}

}
