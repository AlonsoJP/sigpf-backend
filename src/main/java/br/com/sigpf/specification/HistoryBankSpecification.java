package br.com.sigpf.specification;

import java.time.LocalDateTime;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import br.com.sigpf.entity.HistoryBankEntity;

@SuppressWarnings("serial")
public class HistoryBankSpecification {

	private static final String ID = "id";
	private static final String IDTRANSACTION = "id_transaction";
	private static final String TYPETRANSACTION = "type_transaction";
	private static final String PROTOCOLLTRANSACTION = "protocol_transaction";
	private static final String AMOUNTTRANSACTION = "amount_transaction";
	private static final String EXERCISEBANK = "exercise_bank";
	private static final String SALEOLD = "sale_old";
	private static final String SALENEW = "sale_new";
	private static final String DATE = "createDate";
	private static final String CREATEUSER = "createUser";
	private static final String UPDATEUSER = "updateUser";

	public static Specification<HistoryBankEntity> isNotNullId() {
		return new Specification<HistoryBankEntity>() {

			@Override
			public Predicate toPredicate(Root<HistoryBankEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {

				return cb.isNotNull(root.get(ID));

			}
		};
	}

	public static Specification<HistoryBankEntity> equalId(Long id) {
		return new Specification<HistoryBankEntity>() {

			@Override
			public Predicate toPredicate(Root<HistoryBankEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(ID), id);
			}
		};
	}

	public static Specification<HistoryBankEntity> equalAmount(Double value) {
		return new Specification<HistoryBankEntity>() {

			@Override
			public Predicate toPredicate(Root<HistoryBankEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(AMOUNTTRANSACTION), value);
			}
		};
	}

	public static Specification<HistoryBankEntity> likeIdTransaction(String id_transaction) {
		return new Specification<HistoryBankEntity>() {

			@Override
			public Predicate toPredicate(Root<HistoryBankEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(IDTRANSACTION), StringUtils.join("%", id_transaction, "%"));
			}

		};
	}

	public static Specification<HistoryBankEntity> likeTypeTransaction(String type_transaction) {
		return new Specification<HistoryBankEntity>() {

			@Override
			public Predicate toPredicate(Root<HistoryBankEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(TYPETRANSACTION), StringUtils.join("%", type_transaction, "%"));
			}

		};
	}

	public static Specification<HistoryBankEntity> likeProtocolTransaction(String protocol_transaction) {
		return new Specification<HistoryBankEntity>() {

			@Override
			public Predicate toPredicate(Root<HistoryBankEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(PROTOCOLLTRANSACTION), StringUtils.join("%", protocol_transaction, "%"));
			}

		};
	}

	public static Specification<HistoryBankEntity> likeExerciseBank(String exercise_bank) {
		return new Specification<HistoryBankEntity>() {

			@Override
			public Predicate toPredicate(Root<HistoryBankEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(EXERCISEBANK), StringUtils.join("%", exercise_bank, "%"));
			}

		};
	}

	public static Specification<HistoryBankEntity> likeSaleOld(String sale_old) {
		return new Specification<HistoryBankEntity>() {

			@Override
			public Predicate toPredicate(Root<HistoryBankEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(SALEOLD), StringUtils.join("%", sale_old, "%"));
			}

		};
	}

	public static Specification<HistoryBankEntity> likeSaleNew(String sale_new) {
		return new Specification<HistoryBankEntity>() {

			@Override
			public Predicate toPredicate(Root<HistoryBankEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(SALENEW), StringUtils.join("%", sale_new, "%"));
			}

		};
	}

	public static Specification<HistoryBankEntity> likeCreateUser(String createUser) {
		return new Specification<HistoryBankEntity>() {

			@Override
			public Predicate toPredicate(Root<HistoryBankEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(CREATEUSER), StringUtils.join("%", createUser, "%"));
			}

		};
	}

	public static Specification<HistoryBankEntity> likeUpdateUser(String updateUser) {
		return new Specification<HistoryBankEntity>() {

			@Override
			public Predicate toPredicate(Root<HistoryBankEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(UPDATEUSER), StringUtils.join("%", updateUser, "%"));
			}

		};
	}

	public static Specification<HistoryBankEntity> createDateLessThanOrEqual(LocalDateTime date) {
		return new Specification<HistoryBankEntity>() {
			@Override
			public Predicate toPredicate(Root<HistoryBankEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.lessThanOrEqualTo(root.get(DATE), date);
			}

		};
	}

	public static Specification<HistoryBankEntity> createGreaterThanOrEqual(LocalDateTime date) {
		return new Specification<HistoryBankEntity>() {
			@Override
			public Predicate toPredicate(Root<HistoryBankEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.greaterThanOrEqualTo(root.get(DATE), date);
			}

		};
	}

}
