package br.com.sigpf.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import br.com.sigpf.entity.DriverEntity;

@SuppressWarnings("serial")
public class DriverSpecification {

	private static final String ID = "id";
	private static final String NAME = "name";
	private static final String CPF = "cpf";
	private static final String RG = "rg";
	private static final String LICENSE = "license";

	public static Specification<DriverEntity> isNotNullId() {
		return new Specification<DriverEntity>() {

			@Override
			public Predicate toPredicate(Root<DriverEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {

				return cb.isNotNull(root.get(ID));

			}
		};
	}

	public static Specification<DriverEntity> equalId(Long id) {
		return new Specification<DriverEntity>() {

			@Override
			public Predicate toPredicate(Root<DriverEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(ID), id);
			}
		};
	}

	public static Specification<DriverEntity> likeName(String name) {
		return new Specification<DriverEntity>() {

			@Override
			public Predicate toPredicate(Root<DriverEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(NAME), StringUtils.join("%", name, "%"));
			}

		};
	}

	public static Specification<DriverEntity> likeCpf(String cpf) {
		return new Specification<DriverEntity>() {

			@Override
			public Predicate toPredicate(Root<DriverEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(CPF), StringUtils.join("%", cpf, "%"));
			}

		};
	}

	public static Specification<DriverEntity> likeRg(String rg) {
		return new Specification<DriverEntity>() {

			@Override
			public Predicate toPredicate(Root<DriverEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(RG), StringUtils.join("%", rg, "%"));
			}

		};
	}

	public static Specification<DriverEntity> likeLicense(String license) {
		return new Specification<DriverEntity>() {

			@Override
			public Predicate toPredicate(Root<DriverEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(LICENSE), StringUtils.join("%", license, "%"));
			}

		};
	}

}
