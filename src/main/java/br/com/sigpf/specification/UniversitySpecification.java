package br.com.sigpf.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import br.com.sigpf.entity.UniversityEntity;

@SuppressWarnings("serial")
public class UniversitySpecification {

	private static final String ID = "id";
	private static final String NAME = "name";
	private static final String CNPJ = "cnpj";

	public static Specification<UniversityEntity> isNotNullId() {
		return new Specification<UniversityEntity>() {

			@Override
			public Predicate toPredicate(Root<UniversityEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {

				return cb.isNotNull(root.get(ID));

			}
		};
	}

	public static Specification<UniversityEntity> equalId(Long id) {
		return new Specification<UniversityEntity>() {

			@Override
			public Predicate toPredicate(Root<UniversityEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(ID), id);
			}
		};
	}

	public static Specification<UniversityEntity> likeName(String name) {
		return new Specification<UniversityEntity>() {

			@Override
			public Predicate toPredicate(Root<UniversityEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(NAME), StringUtils.join("%", name, "%"));
			}

		};
	}

	public static Specification<UniversityEntity> likeCnpj(String cnpj) {
		return new Specification<UniversityEntity>() {

			@Override
			public Predicate toPredicate(Root<UniversityEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(CNPJ), StringUtils.join("%", cnpj, "%"));
			}

		};
	}

}
