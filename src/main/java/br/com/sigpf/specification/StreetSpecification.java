package br.com.sigpf.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import br.com.sigpf.entity.StreetEntity;

@SuppressWarnings("serial")
public class StreetSpecification {

	private static final String CODIGO = "codigo";
	private static final String NAME = "name_street";

	public static Specification<StreetEntity> isNotNullId() {
		return new Specification<StreetEntity>() {

			@Override
			public Predicate toPredicate(Root<StreetEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {

				return cb.isNotNull(root.get(CODIGO));

			}
		};
	}

	public static Specification<StreetEntity> equalId(Long id) {
		return new Specification<StreetEntity>() {

			@Override
			public Predicate toPredicate(Root<StreetEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(CODIGO), id);
			}
		};
	}

	public static Specification<StreetEntity> likeName(String name) {
		return new Specification<StreetEntity>() {

			@Override
			public Predicate toPredicate(Root<StreetEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(NAME), StringUtils.join("%", name, "%"));
			}

		};
	}

}
