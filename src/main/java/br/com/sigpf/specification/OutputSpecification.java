package br.com.sigpf.specification;

import java.time.LocalDateTime;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import br.com.sigpf.entity.OutputEntity;

@SuppressWarnings("serial")
public class OutputSpecification {

	private static final String ID = "id";
	private static final String TYPETRANSACTION = "typeTransaction";
	private static final String AMOUNTTRANSACTION = "amountTransaction";
	private static final String PROTOCOLTRANSACTION = "protocolTransaction";
	private static final String DESCRITPTION = "description";
	private static final String DATE = "createDate";
	private static final String CREATEUSER = "createUser";
	private static final String UPDATEUSER = "updateUser";

	public static Specification<OutputEntity> isNotNullId() {
		return new Specification<OutputEntity>() {

			@Override
			public Predicate toPredicate(Root<OutputEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {

				return cb.isNotNull(root.get(ID));

			}
		};
	}

	public static Specification<OutputEntity> equalId(Long id) {
		return new Specification<OutputEntity>() {

			@Override
			public Predicate toPredicate(Root<OutputEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(ID), id);
			}
		};
	}

	public static Specification<OutputEntity> equalAmount(Double value) {
		return new Specification<OutputEntity>() {

			@Override
			public Predicate toPredicate(Root<OutputEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(AMOUNTTRANSACTION), value);
			}
		};
	}

	public static Specification<OutputEntity> likeTypeTransaction(String typeTransaction) {
		return new Specification<OutputEntity>() {

			@Override
			public Predicate toPredicate(Root<OutputEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(TYPETRANSACTION), StringUtils.join("%", typeTransaction, "%"));
			}

		};
	}

	public static Specification<OutputEntity> likeAmountTransaction(String amountTransaction) {
		return new Specification<OutputEntity>() {

			@Override
			public Predicate toPredicate(Root<OutputEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(AMOUNTTRANSACTION), StringUtils.join("%", amountTransaction, "%"));
			}

		};
	}

	public static Specification<OutputEntity> likeProtocolTransaction(String protocolTransaction) {
		return new Specification<OutputEntity>() {

			@Override
			public Predicate toPredicate(Root<OutputEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(PROTOCOLTRANSACTION), StringUtils.join("%", protocolTransaction, "%"));
			}

		};
	}

	public static Specification<OutputEntity> likeDescription(String description) {
		return new Specification<OutputEntity>() {

			@Override
			public Predicate toPredicate(Root<OutputEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(DESCRITPTION), StringUtils.join("%", description, "%"));
			}

		};
	}

	public static Specification<OutputEntity> likeCreateUser(String createUser) {
		return new Specification<OutputEntity>() {

			@Override
			public Predicate toPredicate(Root<OutputEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(CREATEUSER), StringUtils.join("%", createUser, "%"));
			}

		};
	}

	public static Specification<OutputEntity> likeUpdateUser(String updateUser) {
		return new Specification<OutputEntity>() {

			@Override
			public Predicate toPredicate(Root<OutputEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(UPDATEUSER), StringUtils.join("%", updateUser, "%"));
			}

		};
	}

	public static Specification<OutputEntity> createDateLessThanOrEqual(LocalDateTime date) {
		return new Specification<OutputEntity>() {
			@Override
			public Predicate toPredicate(Root<OutputEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.lessThanOrEqualTo(root.get(DATE), date);
			}

		};
	}

	public static Specification<OutputEntity> createGreaterThanOrEqual(LocalDateTime date) {
		return new Specification<OutputEntity>() {
			@Override
			public Predicate toPredicate(Root<OutputEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.greaterThanOrEqualTo(root.get(DATE), date);
			}

		};
	}

}
