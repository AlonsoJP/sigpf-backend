package br.com.sigpf.specification;

import java.time.LocalDate;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import br.com.sigpf.entity.RecipeEntity;

@SuppressWarnings("serial")
public class RecipeSpecification {

	private static final String ID = "id";
	private static final String NAME = "name";
	private static final String DATE = "createDate";

	public static Specification<RecipeEntity> isNotNullId() {
		return new Specification<RecipeEntity>() {

			@Override
			public Predicate toPredicate(Root<RecipeEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {

				return cb.isNotNull(root.get(ID));

			}
		};
	}

	public static Specification<RecipeEntity> equalId(Long id) {
		return new Specification<RecipeEntity>() {

			@Override
			public Predicate toPredicate(Root<RecipeEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(ID), id);
			}
		};
	}

	public static Specification<RecipeEntity> likeName(String name) {
		return new Specification<RecipeEntity>() {

			@Override
			public Predicate toPredicate(Root<RecipeEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(NAME), StringUtils.join("%", name, "%"));
			}

		};
	}

	public static Specification<RecipeEntity> createDateLessThanOrEqual(LocalDate date) {
		return new Specification<RecipeEntity>() {
			@Override
			public Predicate toPredicate(Root<RecipeEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.lessThanOrEqualTo(root.get(DATE), date);
			}

		};
	}

	public static Specification<RecipeEntity> createGreaterThanOrEqual(LocalDate date) {
		return new Specification<RecipeEntity>() {
			@Override
			public Predicate toPredicate(Root<RecipeEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.greaterThanOrEqualTo(root.get(DATE), date);
			}

		};
	}

}
