package br.com.sigpf.specification;

import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import br.com.sigpf.entity.SupportCityhallEntity;

@SuppressWarnings("serial")
public class SupportCityhallSpecification {

	private static final String ID = "id";
	private static final String MOUNTHREFERENCE = "mounth_reference";
	private static final String CREATEDATE = "createDate";
	private static final String UPDATEDATE = "updateDate";
	private static final String CREATEUSER = "createUser";
	private static final String UPDATEUSER = "updateUser";

	public static Specification<SupportCityhallEntity> isNotNullId() {
		return new Specification<SupportCityhallEntity>() {

			@Override
			public Predicate toPredicate(Root<SupportCityhallEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {

				return cb.isNotNull(root.get(ID));

			}
		};
	}

	public static Specification<SupportCityhallEntity> equalId(Long id) {
		return new Specification<SupportCityhallEntity>() {

			@Override
			public Predicate toPredicate(Root<SupportCityhallEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(ID), id);
			}
		};
	}

	public static Specification<SupportCityhallEntity> likeMounthReference(String spec) {
		return new Specification<SupportCityhallEntity>() {

			@Override
			public Predicate toPredicate(Root<SupportCityhallEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(MOUNTHREFERENCE), StringUtils.join("%", spec, "%"));
			}

		};
	}

	public static Specification<SupportCityhallEntity> likeCreateDate(Date createDate) {
		return new Specification<SupportCityhallEntity>() {

			@Override
			public Predicate toPredicate(Root<SupportCityhallEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(CREATEDATE), createDate);
			}

		};
	}

	public static Specification<SupportCityhallEntity> likeUpdateDate(Date updateDate) {
		return new Specification<SupportCityhallEntity>() {

			@Override
			public Predicate toPredicate(Root<SupportCityhallEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(UPDATEDATE), updateDate);
			}

		};
	}

	public static Specification<SupportCityhallEntity> likeCreateUser(String createUser) {
		return new Specification<SupportCityhallEntity>() {

			@Override
			public Predicate toPredicate(Root<SupportCityhallEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(CREATEUSER), StringUtils.join("%", createUser, "%"));
			}

		};
	}

	public static Specification<SupportCityhallEntity> likeUpdateUser(String updateUser) {
		return new Specification<SupportCityhallEntity>() {

			@Override
			public Predicate toPredicate(Root<SupportCityhallEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(UPDATEUSER), StringUtils.join("%", updateUser, "%"));
			}

		};
	}

	public static Specification<SupportCityhallEntity> betweenCreateDate(Date createDateBegin, Date createDateEnd) {
		return new Specification<SupportCityhallEntity>() {

			@Override
			public Predicate toPredicate(Root<SupportCityhallEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.between(root.get(CREATEDATE), createDateBegin, createDateEnd);
			}

		};
	}

}
