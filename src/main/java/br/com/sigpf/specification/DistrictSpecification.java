package br.com.sigpf.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import br.com.sigpf.entity.DistrictEntity;

@SuppressWarnings("serial")
public class DistrictSpecification {

	private static final String CODIGO = "cod_district";
	private static final String NAME = "name_district";

	public static Specification<DistrictEntity> isNotNullId() {
		return new Specification<DistrictEntity>() {

			@Override
			public Predicate toPredicate(Root<DistrictEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {

				return cb.isNotNull(root.get(CODIGO));

			}
		};
	}

	public static Specification<DistrictEntity> equalId(Long id) {
		return new Specification<DistrictEntity>() {

			@Override
			public Predicate toPredicate(Root<DistrictEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(CODIGO), id);
			}
		};
	}

	public static Specification<DistrictEntity> likeName(String name) {
		return new Specification<DistrictEntity>() {

			@Override
			public Predicate toPredicate(Root<DistrictEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(NAME), StringUtils.join("%", name, "%"));
			}

		};
	}

}
