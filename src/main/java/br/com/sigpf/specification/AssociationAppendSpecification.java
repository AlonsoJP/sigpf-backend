package br.com.sigpf.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import br.com.sigpf.entity.AssociationAppendEntity;
import br.com.sigpf.entity.AssociationEntity;

@SuppressWarnings("serial")
public class AssociationAppendSpecification {

	private static final String ID = "id";
	private static final String DESCRIPTION = "description";
	private static final String ASSOCIATION_ID = "id";

	public static Specification<AssociationAppendEntity> isNotNullId() {
		return new Specification<AssociationAppendEntity>() {

			@Override
			public Predicate toPredicate(Root<AssociationAppendEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {

				return cb.isNotNull(root.get(ID));

			}
		};
	}

	public static Specification<AssociationAppendEntity> equalId(Long id) {
		return new Specification<AssociationAppendEntity>() {

			@Override
			public Predicate toPredicate(Root<AssociationAppendEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(ID), id);
			}
		};
	}

	public static Specification<AssociationAppendEntity> likeDescription(String description) {
		return new Specification<AssociationAppendEntity>() {

			@Override
			public Predicate toPredicate(Root<AssociationAppendEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(DESCRIPTION), StringUtils.join("%", description, "%"));
			}

		};
	}

	// referentes a tabela relacionada
	public static Specification<AssociationAppendEntity> equalAssociation(Long associationId) {
		return new Specification<AssociationAppendEntity>() {
			@Override
			public Predicate toPredicate(Root<AssociationAppendEntity> root, CriteriaQuery<?> query,
					CriteriaBuilder cb) {
				Join<AssociationAppendEntity, AssociationEntity> jCourse = root.join("association", JoinType.INNER);
				return cb.equal(jCourse.get(ASSOCIATION_ID), associationId);
			}
		};
	}
}
