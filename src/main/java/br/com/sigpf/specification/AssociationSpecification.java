package br.com.sigpf.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import br.com.sigpf.entity.AssociationEntity;

@SuppressWarnings("serial")
public class AssociationSpecification {

	private static final String ID = "id";
	private static final String OWNER = "owner";
	private static final String FANTASYNAME = "fantasyName";
	private static final String CNPJ = "cnpj";

	public static Specification<AssociationEntity> isNotNullId() {
		return new Specification<AssociationEntity>() {

			@Override
			public Predicate toPredicate(Root<AssociationEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {

				return cb.isNotNull(root.get(ID));

			}
		};
	}

	public static Specification<AssociationEntity> equalId(Long id) {
		return new Specification<AssociationEntity>() {

			@Override
			public Predicate toPredicate(Root<AssociationEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(ID), id);
			}
		};
	}

	public static Specification<AssociationEntity> likeOwner(String owner) {
		return new Specification<AssociationEntity>() {

			@Override
			public Predicate toPredicate(Root<AssociationEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(OWNER), StringUtils.join("%", owner, "%"));
			}

		};
	}

	public static Specification<AssociationEntity> likeFantasyName(String fantasyName) {
		return new Specification<AssociationEntity>() {

			@Override
			public Predicate toPredicate(Root<AssociationEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(FANTASYNAME), StringUtils.join("%", fantasyName, "%"));
			}

		};
	}

	public static Specification<AssociationEntity> likeCnpj(String cnpj) {
		return new Specification<AssociationEntity>() {

			@Override
			public Predicate toPredicate(Root<AssociationEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(CNPJ), StringUtils.join("%", cnpj, "%"));
			}

		};
	}

}
