package br.com.sigpf.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import br.com.sigpf.entity.SupportCityhallAppendEntity;
import br.com.sigpf.entity.SupportCityhallEntity;

@SuppressWarnings("serial")
public class SupportCityhallAppendSpecification {

	private static final String ID = "id";
	private static final String DESCRIPTION = "description";
	private static final String SUPPORT_ID = "id";

	public static Specification<SupportCityhallAppendEntity> isNotNullId() {
		return new Specification<SupportCityhallAppendEntity>() {

			@Override
			public Predicate toPredicate(Root<SupportCityhallAppendEntity> root, CriteriaQuery<?> cq,
					CriteriaBuilder cb) {

				return cb.isNotNull(root.get(ID));

			}
		};
	}

	public static Specification<SupportCityhallAppendEntity> equalId(Long id) {
		return new Specification<SupportCityhallAppendEntity>() {

			@Override
			public Predicate toPredicate(Root<SupportCityhallAppendEntity> root, CriteriaQuery<?> cq,
					CriteriaBuilder cb) {
				return cb.equal(root.get(ID), id);
			}
		};
	}

	public static Specification<SupportCityhallAppendEntity> likeDescription(String description) {
		return new Specification<SupportCityhallAppendEntity>() {

			@Override
			public Predicate toPredicate(Root<SupportCityhallAppendEntity> root, CriteriaQuery<?> cq,
					CriteriaBuilder cb) {
				return cb.like(root.get(DESCRIPTION), StringUtils.join("%", description, "%"));
			}

		};
	}

	// referentes a tabela relacionada
	public static Specification<SupportCityhallAppendEntity> equalSupport(Long associationId) {
		return new Specification<SupportCityhallAppendEntity>() {
			@Override
			public Predicate toPredicate(Root<SupportCityhallAppendEntity> root, CriteriaQuery<?> query,
					CriteriaBuilder cb) {
				Join<SupportCityhallAppendEntity, SupportCityhallEntity> jCourse = root.join("support_cityhall",
						JoinType.INNER);
				return cb.equal(jCourse.get(SUPPORT_ID), associationId);
			}
		};
	}

}
