package br.com.sigpf.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import br.com.sigpf.entity.StudentAppendEntity;
import br.com.sigpf.entity.StudentEntity;

@SuppressWarnings("serial")
public class StudentAppendSpecification {

	private static final String ID = "id";
	private static final String DESCRIPTION = "description";
	private static final String STUDENT_ID = "id";

	public static Specification<StudentAppendEntity> isNotNullId() {
		return new Specification<StudentAppendEntity>() {

			@Override
			public Predicate toPredicate(Root<StudentAppendEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {

				return cb.isNotNull(root.get(ID));

			}
		};
	}

	public static Specification<StudentAppendEntity> equalId(Long id) {
		return new Specification<StudentAppendEntity>() {

			@Override
			public Predicate toPredicate(Root<StudentAppendEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(ID), id);
			}
		};
	}

	public static Specification<StudentAppendEntity> likeDescription(String description) {
		return new Specification<StudentAppendEntity>() {

			@Override
			public Predicate toPredicate(Root<StudentAppendEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(DESCRIPTION), StringUtils.join("%", description, "%"));
			}

		};
	}

	// referentes a tabela relacionada
	public static Specification<StudentAppendEntity> equalAssociation(Long studentId) {
		return new Specification<StudentAppendEntity>() {
			@Override
			public Predicate toPredicate(Root<StudentAppendEntity> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Join<StudentAppendEntity, StudentEntity> jCourse = root.join("student", JoinType.INNER);
				return cb.equal(jCourse.get(STUDENT_ID), studentId);
			}
		};
	}
}
