package br.com.sigpf.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import br.com.sigpf.entity.CourseEntity;
import br.com.sigpf.entity.StudentEntity;

@SuppressWarnings("serial")
public class StudentSpecification {

	private static final String ID = "id";
	private static final String NAME = "name";
	private static final String CPF = "cpf";
	private static final String RG = "rg";
	private static final String RA = "ra";
	private static final String EMAIL = "email";
	private static final String NAME_COURSE = "name_course";

	public static Specification<StudentEntity> isNotNullId() {
		return new Specification<StudentEntity>() {

			@Override
			public Predicate toPredicate(Root<StudentEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {

				return cb.isNotNull(root.get(ID));

			}
		};
	}

	public static Specification<StudentEntity> equalId(Long id) {
		return new Specification<StudentEntity>() {

			@Override
			public Predicate toPredicate(Root<StudentEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.equal(root.get(ID), id);
			}
		};
	}

	public static Specification<StudentEntity> likeName(String name) {
		return new Specification<StudentEntity>() {

			@Override
			public Predicate toPredicate(Root<StudentEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(NAME), StringUtils.join("%", name, "%"));
			}

		};
	}

	public static Specification<StudentEntity> likeCpf(String cpf) {
		return new Specification<StudentEntity>() {

			@Override
			public Predicate toPredicate(Root<StudentEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(CPF), StringUtils.join("%", cpf, "%"));
			}

		};
	}

	public static Specification<StudentEntity> likeRg(String rg) {
		return new Specification<StudentEntity>() {

			@Override
			public Predicate toPredicate(Root<StudentEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(RG), StringUtils.join("%", rg, "%"));
			}

		};
	}

	public static Specification<StudentEntity> likeRa(String ra) {
		return new Specification<StudentEntity>() {

			@Override
			public Predicate toPredicate(Root<StudentEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(RA), StringUtils.join("%", ra, "%"));
			}

		};
	}

	public static Specification<StudentEntity> likeEmail(String email) {
		return new Specification<StudentEntity>() {

			@Override
			public Predicate toPredicate(Root<StudentEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				return cb.like(root.get(EMAIL), StringUtils.join("%", email, "%"));
			}

		};
	}

	// referentes a tabela relacionada
	public static Specification<StudentEntity> equalCourse(Long idCourse) {
		return new Specification<StudentEntity>() {
			@Override
			public Predicate toPredicate(Root<StudentEntity> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Join<StudentEntity, CourseEntity> jCourse = root.join("course", JoinType.INNER);
				return cb.equal(jCourse.get(ID), idCourse);
			}
		};
	}

	public static Specification<StudentEntity> likeNameCourse(String nameCourse) {
		return new Specification<StudentEntity>() {
			@Override
			public Predicate toPredicate(Root<StudentEntity> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Join<StudentEntity, CourseEntity> jCourse = root.join("course", JoinType.INNER);
				return cb.like(jCourse.get(NAME_COURSE), StringUtils.join("%", nameCourse, "%"));
			}
		};
	}

}
