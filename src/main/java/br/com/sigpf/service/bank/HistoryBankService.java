package br.com.sigpf.service.bank;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

import br.com.sigpf.converter.HistoryBankConverter;
import br.com.sigpf.entity.HistoryBankEntity;
import br.com.sigpf.filter.HistoryBankFilter;
import br.com.sigpf.repository.HistoryBankRepository;
import br.com.sigpf.resource.HistoryBankResource;
import br.com.sigpf.service.user.UserService;
import br.com.sigpf.specification.HistoryBankSpecification;
import br.com.sigpf.util.GetInfoToken;

@Service
public class HistoryBankService {

	@Autowired
	private HistoryBankRepository repository;

	@Autowired
	UserService service;

	GetInfoToken getInfoToken = new GetInfoToken();

	public static final Logger LOG = LogManager.getLogger(HistoryBankEntity.class);

	public List<HistoryBankResource> findAll() throws Exception {

		try {
			LOG.info("HistoryBankController END : findAll");

			return HistoryBankConverter.listToDto(repository.findAll());
		} catch (Exception e) {

			LOG.error("HistoryBankController ERROR : findAll");

			throw new Exception(e.getCause().getMessage());

		}

	}

	public Optional<HistoryBankResource> findOne(Long id) {
		LOG.info("HistoryBankController END : findOne");
		return HistoryBankConverter.toOptionalDto(repository.findById(id));
	}

	public Page<HistoryBankResource> findByFilter(HistoryBankFilter filter, Pageable pageable) {
		LOG.info("HistoryBankController END : findByFilter");
		return repository.findAll(getSpecification(filter), pageable)
				.map(HistoryBankEntity -> HistoryBankConverter.toDto(HistoryBankEntity));
	}

	public List<HistoryBankResource> findByFilter(HistoryBankFilter filter) {
		LOG.info("HistoryBankController END : findByFilter");
		return HistoryBankConverter.listToDto(repository.findAll(getSpecification(filter)));
	}

	public HistoryBankResource save(HistoryBankResource resource, OAuth2Authentication auth) throws Exception {
		LOG.info("HistoryBankController END : save");

		if (auth != null) {

			String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

			resource.setCreateUser(user);
		} else {
			throw new Exception("Usuario ausente ou não capturado.");
		}

		return HistoryBankConverter.toDto(repository.save(HistoryBankConverter.toEntity(resource)));

	}

	public HistoryBankResource update(HistoryBankResource resource, Long id, OAuth2Authentication auth) {

		try {
			findOne(id);

			if (auth != null) {

				String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

				resource.setUpdateUser(user);
			}

			LOG.info("HistoryBankController END : update");

		} catch (Exception e) {
			e.getCause().getMessage();
			LOG.error("HistoryBankController ERROR : update");
		}

		resource.setId(id);

		LOG.info("HistoryBankController END : update");

		return HistoryBankConverter.toDto(repository.save(HistoryBankConverter.toEntity(resource)));

	}

	public void delete(Long id) {
		try {
			findOne(id);
			repository.deleteById(id);
			LOG.info("HistoryBankController END : delete");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("HistoryBankController ERROR : delete");
		}
	}

	private Specification<HistoryBankEntity> getSpecification(HistoryBankFilter filter) {

		if (filter != null) {
			Specification<HistoryBankEntity> spec = Specification
					.where((filter.getId() == null) ? null : HistoryBankSpecification.isNotNullId());

			spec = (filter.getId() == null) ? spec : spec.and(HistoryBankSpecification.equalId(filter.getId()));

			spec = (filter.getAmount_transaction() == null) ? spec
					: spec.and(HistoryBankSpecification.equalAmount(filter.getAmount_transaction()));
			spec = (filter.getProtocol_transaction() == null) ? spec
					: spec.and(HistoryBankSpecification.likeProtocolTransaction(filter.getProtocol_transaction()));

			spec = (filter.getDateBegin() == null) ? spec
					: spec.and(HistoryBankSpecification.createDateLessThanOrEqual(filter.getDateEnd()));
			spec = (filter.getDateEnd() == null) ? spec
					: spec.and(HistoryBankSpecification.createGreaterThanOrEqual(filter.getDateBegin()));

			return spec;

		} else {
			return null;
		}

	}

}
