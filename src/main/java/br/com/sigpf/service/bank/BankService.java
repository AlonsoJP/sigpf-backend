package br.com.sigpf.service.bank;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import br.com.sigpf.converter.BankConverter;
import br.com.sigpf.entity.BankEntity;
import br.com.sigpf.filter.BankFilter;
import br.com.sigpf.repository.BankRepository;
import br.com.sigpf.resource.BankResource;
import br.com.sigpf.service.user.UserService;
import br.com.sigpf.specification.BankSpecification;
import br.com.sigpf.util.GetInfoToken;

@Service
public class BankService {

	@Autowired
	private BankRepository repository;

	@Autowired
	UserService service;

	GetInfoToken getInfoToken = new GetInfoToken();

	HistoryBankService serviceHystory;

	public static final Logger LOG = LogManager.getLogger(BankEntity.class);

	public List<BankResource> findAll() throws Exception {

		try {
			LOG.info("BankController END : findAll");

			return BankConverter.listToDto(repository.findAll());
		} catch (Exception e) {

			LOG.error("BankController ERROR : findAll");

			throw new Exception(e.getCause().getMessage());

		}

	}

	public Optional<BankResource> findOne(Long id) {
		LOG.info("BankController END : findOne");
		return BankConverter.toOptionalDto(repository.findById(id));
	}

	public Page<BankResource> findByFilter(BankFilter filter, Pageable pageable) {
		LOG.info("BankController END : findByFilter");
		return repository.findAll(getSpecification(filter), pageable)
				.map(BankEntity -> BankConverter.toDto(BankEntity));
	}

	public List<BankResource> findByFilter(BankFilter filter) {
		LOG.info("BankController END : findByFilter");
		return BankConverter.listToDto(repository.findAll(getSpecification(filter)));
	}

	public BankResource save(BankResource resource, OAuth2Authentication auth) throws Exception {
		LOG.info("BankController END : save");

		if (auth != null) {

			String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

			resource.setCreateUser(user);
		} else {
			throw new Exception("Usuario ausente ou não capturado.");
		}

		// verifiar a existencia de um caixa antes da criação de um novo.
		if (repository.findByActive(true) != null) {
			throw new HttpClientErrorException(HttpStatus.BAD_REQUEST,
					"Já existe um caixa criado para este exercício!");
		}

		return BankConverter.toDto(repository.save(BankConverter.toEntity(resource)));

	}

	public BankResource update(BankResource resource, Long id, OAuth2Authentication auth) throws Exception {

		try {
			findOne(id);

			if (auth != null) {

				String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

				resource.setUpdateUser(user);
			}

			LOG.info("BankController END : update");

		} catch (Exception e) {
			e.getCause().getMessage();
			LOG.error("BankController ERROR : update");
		}

		// verificar regras a partir da atividade
		if (resource.getActive() == false) {

			// inativar somente o que não for do exercicio corrente
			if (resource.getExercise_bank().equalsIgnoreCase(String.valueOf(LocalDate.now().getYear()))) {

				LOG.warn("BankController WARN : update");
				throw new Exception("Não é possível inativar este caixa. Exercício corrente.");

			}

		}

		// verificar regras a partir do exercicio
//		if (StringUtils.isNotEmpty(resource.getExercise_bank())) {
//			// impedir atualização de exercício
//			if (!resource.getExercise_bank().equalsIgnoreCase(String.valueOf(LocalDate.now().getYear()))) {
//
//				LOG.warn("BankController WARN : update");
//				throw new Exception("Não é possível alterar o exercício deste caixa. ");
//
//			}
//
//		}

		resource.setId(id);

		LOG.info("BankController END : update");

		return BankConverter.toDto(repository.save(BankConverter.toEntity(resource)));

	}

	public void delete(Long id) {
		try {
			findOne(id);
			repository.deleteById(id);
			LOG.info("BankController END : delete");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("BankController ERROR : delete");
		}
	}

	private Specification<BankEntity> getSpecification(BankFilter filter) {

		if (filter != null) {
			Specification<BankEntity> spec = Specification
					.where((filter.getId() == null) ? null : BankSpecification.isNotNullId());

			spec = (filter.getId() == null) ? spec : spec.and(BankSpecification.equalId(filter.getId()));

			return spec;

		} else {
			return null;
		}

	}

	public BankResource findByExercise(Boolean active) {

		BankResource find = BankConverter.toDto(repository.findByActive(active));

		return find;
	}

}
