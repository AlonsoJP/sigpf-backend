package br.com.sigpf.service.company;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import br.com.sigpf.converter.CompanyAppendConverter;
import br.com.sigpf.entity.CompanyAppendEntity;
import br.com.sigpf.filter.CompanyAppendFilter;
import br.com.sigpf.repository.CompanyAppendRepository;
import br.com.sigpf.resource.CompanyAppendResource;
import br.com.sigpf.service.user.UserService;
import br.com.sigpf.specification.CompanyAppendSpecification;
import br.com.sigpf.util.Appends;
import br.com.sigpf.util.GetInfoToken;
import br.com.sigpf.util.ValidationAppends;

@Service
public class CompanyAppendService {

	@Value("${path.appends.root}")
	private String root;

	@Value("${path.appends.company-append}")
	private String path;

	ValidationAppends validation = new ValidationAppends();

	@Autowired
	private CompanyAppendRepository repository;

	@Autowired
	UserService service;

	GetInfoToken getInfoToken = new GetInfoToken();

	public static final Logger LOG = LogManager.getLogger(CompanyAppendEntity.class);

	public Iterable<CompanyAppendEntity> findAll() throws Exception {

		try {
			LOG.info("CompanyAppend Controller END : findAll");

			return repository.findAll();
		} catch (Exception e) {
			e.getCause().getMessage();

			LOG.error("CompanyAppend ERROR : findAll");

			return null;
		}

	}

	public Optional<CompanyAppendResource> findOne(Long id) {
		return CompanyAppendConverter.toOptionalDto(repository.findById(id));
	}

	public Page<CompanyAppendResource> findByFilter(CompanyAppendFilter filter, Pageable pageable) {
		return repository.findAll(getSpecification(filter), pageable)
				.map(CompanyAppendEntity -> CompanyAppendConverter.toDto(CompanyAppendEntity));
	}

	public CompanyAppendResource save(CompanyAppendResource resource, OAuth2Authentication auth) throws Exception {

		if (auth != null) {

			String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

			resource.setCreateUser(user);
			LOG.info("CompanyAppend END : save");
		} else {
			throw new Exception("Usuario ausente ou não capturado.");
		}

		return CompanyAppendConverter.toDto(repository.save(CompanyAppendConverter.toEntity(resource)));

	}

	public String saveDocument(MultipartFile document) throws Exception {

		try {

			validation.validationDocuments(document.getOriginalFilename());

			LOG.info("CompanyAppend Controller END : uploadDocument");
			return Appends.saveDocuments(document, root, path);

		} catch (NullPointerException e) {
			LOG.error("CompanyAppend Controller NullPointerException ERROR : uploadDocument");
			throw new NullPointerException(e.getMessage());
		} catch (RuntimeException e) {
			LOG.error("CompanyAppend Controller RuntimeException ERROR : uploadDocument");
			throw new RuntimeException(e.getMessage());
		}

	}

	public CompanyAppendResource update(CompanyAppendResource resource, Long id, OAuth2Authentication auth) {

		try {
			findOne(id);

			if (auth != null) {

				String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

				resource.setUpdateUser(user);
			}

			LOG.info("CompanyAppend END : update");

		} catch (Exception e) {
			e.getCause().getMessage();
			LOG.error("CompanyAppend ERROR : update");
		}

		resource.setId(id);

		return CompanyAppendConverter.toDto(repository.save(CompanyAppendConverter.toEntity(resource)));

	}

	public void delete(Long id) {
		try {
			CompanyAppendResource resource = findOne(id).get();

			deleteDocument(resource.getPath());

			repository.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("CompanyAppend ERROR : delete");
		}
	}

	public void deleteDocument(String caminho) throws Exception {

		try {

			LOG.info("CompanyAppend Controller START : deleteDocument");
			Appends.deleteDocuments(root, caminho);
			LOG.info("CompanyAppend Controller END : deleteDocument");
		} catch (NullPointerException e) {
			LOG.error("CompanyAppend Controller NullPointerException ERROR : deleteDocument");
			throw new NullPointerException(e.getMessage());
		} catch (RuntimeException e) {
			LOG.error("CompanyAppend Controller RuntimeException ERROR : deleteDocument");
			throw new RuntimeException(e.getMessage());
		}

	}

	private Specification<CompanyAppendEntity> getSpecification(CompanyAppendFilter filter) {

		if (filter != null) {
			Specification<CompanyAppendEntity> spec = Specification
					.where((filter.getId() == null) ? null : CompanyAppendSpecification.isNotNullId());

			spec = (filter.getId() == null) ? spec : spec.and(CompanyAppendSpecification.equalId(filter.getId()));
			spec = (StringUtils.isEmpty(filter.getDescription())) ? spec
					: spec.and(CompanyAppendSpecification.likeDescription(filter.getDescription()));

			// referentes a tabela relacionada
			spec = (filter.getCompanyId() == null) ? spec
					: spec.and(CompanyAppendSpecification.equalCompany(filter.getCompanyId()));

			return spec;

		} else {
			return null;
		}

	}

}
