package br.com.sigpf.service.company;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

import br.com.sigpf.converter.CompanyConverter;
import br.com.sigpf.entity.CompanyEntity;
import br.com.sigpf.filter.CompanyFilter;
import br.com.sigpf.repository.CompanyRepository;
import br.com.sigpf.resource.CompanyResource;
import br.com.sigpf.service.user.UserService;
import br.com.sigpf.specification.CompanySpecification;
import br.com.sigpf.util.GetInfoToken;
import br.com.sigpf.util.ValidationAppends;

@Service
public class CompanyService {

	@Autowired
	private CompanyRepository repository;

	@Autowired
	UserService service;

	GetInfoToken getInfoToken = new GetInfoToken();

	ValidationAppends validation = new ValidationAppends();

	public static final Logger LOG = LogManager.getLogger(CompanyEntity.class);

	public Iterable<CompanyEntity> findAll() throws Exception {

		try {
			LOG.info("ComanyController END : findAll");

			return repository.findAll();
		} catch (Exception e) {
			e.getCause().getMessage();

			LOG.error("ComanyController ERROR : findAll");

			return null;
		}

	}

	public Optional<CompanyResource> findOne(Long id) {
		return CompanyConverter.toOptionalDto(repository.findById(id));
	}

	public Page<CompanyResource> findByFilter(CompanyFilter filter, Pageable pageable) {
		LOG.info("ComanyController END : findByFilter");
		return repository.findAll(getSpecification(filter), pageable)
				.map(CompanyEntity -> CompanyConverter.toDto(CompanyEntity));
	}

	public List<CompanyResource> findByFilter(CompanyFilter filter) {
		LOG.info("ComanyController END : findByFilter");

		return CompanyConverter.listToDto(repository.findAll(getSpecification(filter)));
	}

	public CompanyResource save(CompanyResource resource, OAuth2Authentication auth) throws Exception {

		if (auth != null) {

			String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

			resource.setCreateUser(user);
			LOG.info("StudentController END : save");
		} else {
			throw new Exception("Usuario ausente ou não capturado.");
		}

		return CompanyConverter.toDto(repository.save(CompanyConverter.toEntity(resource)));

	}

	public CompanyResource update(CompanyResource resource, Long id, OAuth2Authentication auth) {

		try {
			findOne(id);

			if (auth != null) {

				String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

				resource.setUpdateUser(user);
			}

			LOG.info("StudentController END : update");

		} catch (Exception e) {
			e.getCause().getMessage();
			LOG.error("AddressController ERROR : update");
		}

		resource.setId(id);

		return CompanyConverter.toDto(repository.save(CompanyConverter.toEntity(resource)));

	}

	public void delete(Long id) {
		try {
			findOne(id);
			repository.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("AddressController ERROR : delete");
		}
	}

	private Specification<CompanyEntity> getSpecification(CompanyFilter filter) {

		if (filter != null) {
			Specification<CompanyEntity> spec = Specification
					.where((filter.getId() == null) ? null : CompanySpecification.isNotNullId());

			spec = (filter.getId() == null) ? spec : spec.and(CompanySpecification.equalId(filter.getId()));
			spec = (StringUtils.isEmpty(filter.getOwner())) ? spec
					: spec.and(CompanySpecification.likeOwner(filter.getOwner()));
			spec = (StringUtils.isEmpty(filter.getFantasy_name())) ? spec
					: spec.and(CompanySpecification.likeFantasyName(filter.getFantasy_name()));
			spec = (StringUtils.isEmpty(filter.getCnpj())) ? spec
					: spec.and(CompanySpecification.likeCnpj(filter.getCnpj()));

			return spec;

		} else {
			return null;
		}

	}

}
