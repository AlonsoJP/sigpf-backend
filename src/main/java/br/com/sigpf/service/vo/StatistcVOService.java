package br.com.sigpf.service.vo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sigpf.repository.InputRepository;
import br.com.sigpf.repository.OutputRepository;
import br.com.sigpf.service.studenty.StudentService;
import br.com.sigpf.vo.ResultsStatistcVO;
import br.com.sigpf.vo.StatistcVO;
import br.com.sigpf.vo.ValueStatisticsVO;

@Service
public class StatistcVOService {

	@Autowired
	StudentService service;

	@Autowired
	private InputRepository inputRepository;

	@Autowired
	private OutputRepository outputRepository;

	// Dados estatísticos cadastrais, do sistema em estado ATUAL
	public StatistcVO statistcs() throws Exception {

		StatistcVO statistcs = new StatistcVO();

		statistcs.setCalcTotal(service.calculation());
		statistcs.setEstimateContribution(service.estimateContribution());
		statistcs.setTotalStudents(service.countTotal());
		statistcs.setTotalStudentsActive(service.countByActive());
		statistcs.setTotalStudentsInactive(service.countByInactive());
		statistcs.setTotalStudentsActiveAndIntegral(service.countByActiveAndTypeIntegral());
		statistcs.setTotalStudentsActiveAndParcial(service.countByActiveAndTypeParcial());
		statistcs.setTotalIntegralWithContributionWhitoutParcial(service.estimateIntegralWithAuxPerStudent());
		statistcs.setTotalIntegralWithContributionWithParcial(service.estimateIntegralWithAuxWithParcialPerStudent());
		statistcs.setTotalParcialWithContribution(service.estimateParcialWithAuxPerStudent());
		statistcs.setMensalityBase(service.mensalityBase());
		return statistcs;
	}

	public ValueStatisticsVO findStatistInputValues() {

		ValueStatisticsVO statistcs = new ValueStatisticsVO();

		statistcs.setTotalGeral(inputRepository.findSumEntry());
		statistcs.setTotalGeralCancel(inputRepository.findSumCancel());
		statistcs.setCurrentDay(inputRepository.findSumCurrentDayOk());
		statistcs.setCurrentDayCancel(inputRepository.findSumCurrentDayCancel());
		statistcs.setCurrentMonth(inputRepository.findSumCurrentMonthOk());
		statistcs.setCurrentMonthCancel(inputRepository.findSumCurrentMonthCancel());
		statistcs.setLastMonth(inputRepository.findSumLastMonthOk());
		statistcs.setLastMonthCancel(inputRepository.findSumLastMonthCancel());
		statistcs.setCurrentYear(inputRepository.findSumCurrentYearOk());
		statistcs.setCurrentYearCancel(inputRepository.findSumCurrentYearCancel());

		return statistcs;
	}

	public ValueStatisticsVO findStatistOutputValues() {

		ValueStatisticsVO statistcs = new ValueStatisticsVO();

		statistcs.setTotalGeral(outputRepository.findSumEntry());
		statistcs.setTotalGeralCancel(outputRepository.findSumCancel());
		statistcs.setCurrentDay(outputRepository.findSumCurrentDayOk());
		statistcs.setCurrentDayCancel(outputRepository.findSumCurrentDayCancel());
		statistcs.setCurrentMonth(outputRepository.findSumCurrentMonthOk());
		statistcs.setCurrentMonthCancel(outputRepository.findSumCurrentMonthCancel());
		statistcs.setLastMonth(outputRepository.findSumLastMonthOk());
		statistcs.setLastMonthCancel(outputRepository.findSumLastMonthCancel());
		statistcs.setCurrentYear(outputRepository.findSumCurrentYearOk());
		statistcs.setCurrentYearCancel(outputRepository.findSumCurrentYearCancel());

		return statistcs;
	}

	public ResultsStatistcVO results() {
		ResultsStatistcVO statiscts = new ResultsStatistcVO();

		statiscts.setGainDay(gainDay());
		statiscts.setGainMonth(gainMonth());
		statiscts.setGainYear(gainYear());
		statiscts.setLossDay(lossDay());
		statiscts.setLossMonth(lossMonth());
		statiscts.setLossYear(lossYear());
		statiscts.setGainResultDay(gainDayResult());
		statiscts.setGainResultMonth(gainMonthResult());
		statiscts.setGainResultYear(gainYearResult());

		return statiscts;

	}

	public Double gainDay() {
		Double entry = (inputRepository.findSumCurrentDayOk() != null ? inputRepository.findSumCurrentDayOk() : 0);

		return entry;
	}

	public Double gainDayResult() {
		Double entry = (inputRepository.findSumCurrentDayOk() != null ? inputRepository.findSumCurrentDayOk() : 0);
		Double out = (outputRepository.findSumCurrentDayOk() != null ? outputRepository.findSumCurrentDayOk() : 0);

		Double result = (entry - out);

		return result;
	}

	public Double gainMonth() {
		Double entry = (inputRepository.findSumCurrentMonthOk() != null ? inputRepository.findSumCurrentMonthOk() : 0);

		return entry;

	}

	public Double gainMonthResult() {
		Double entry = (inputRepository.findSumCurrentMonthOk() != null ? inputRepository.findSumCurrentMonthOk() : 0);
		Double out = (outputRepository.findSumCurrentMonthOk() != null ? outputRepository.findSumCurrentMonthOk() : 0);

		Double result = (entry - out);

		return result;

	}

	public Double gainYear() {
		Double entry = (inputRepository.findSumCurrentYearOk() != null ? inputRepository.findSumCurrentYearOk() : 0);

		return entry;

	}

	public Double gainYearResult() {
		Double entry = (inputRepository.findSumCurrentYearOk() != null ? inputRepository.findSumCurrentYearOk() : 0);
		Double out = (outputRepository.findSumCurrentYearOk() != null ? outputRepository.findSumCurrentYearOk() : 0);

		Double result = (entry - out);

		return result;

	}

	public Double lossDay() {
		Double result = (outputRepository.findSumCurrentDayOk() != null ? outputRepository.findSumCurrentDayOk() : 0);

		return result;
	}

	public Double lossMonth() {
		Double result = (outputRepository.findSumCurrentMonthOk() != null ? outputRepository.findSumCurrentMonthOk()
				: 0);

		return result;

	}

	public Double lossYear() {
		Double result = (outputRepository.findSumCurrentYearOk() != null ? outputRepository.findSumCurrentYearOk() : 0);

		return result;

	}

}
