package br.com.sigpf.service.finances;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

import br.com.sigpf.converter.SupportCityhallConverter;
import br.com.sigpf.entity.SupportCityhallEntity;
import br.com.sigpf.filter.SupportCityhallFilter;
import br.com.sigpf.repository.SupportCityhallRepository;
import br.com.sigpf.resource.SupportCityhallResource;
import br.com.sigpf.service.user.UserService;
import br.com.sigpf.specification.SupportCityhallSpecification;
import br.com.sigpf.util.GetInfoToken;

@Service
public class SupportCityhallService {

	@Autowired
	private SupportCityhallRepository repository;

	@Autowired
	UserService service;

	GetInfoToken getInfoToken = new GetInfoToken();

	public static final Logger LOG = LogManager.getLogger(SupportCityhallEntity.class);

	public List<SupportCityhallResource> findAll() throws Exception {

		try {
			LOG.info("SupportCityhallController END : findAll");

			return SupportCityhallConverter.listToDto(repository.findAll());
		} catch (Exception e) {

			LOG.error("SupportCityhallController ERROR : findAll");

			throw new Exception(e.getCause().getMessage());

		}

	}

	public Optional<SupportCityhallResource> findOne(Long id) {
		LOG.info("SupportCityhallController END : findOne");
		return SupportCityhallConverter.toOptionalDto(repository.findById(id));
	}

	public Page<SupportCityhallResource> findByFilter(SupportCityhallFilter filter, Pageable pageable) {
		LOG.info("SupportCityhallController END : findByFilter");
		return repository.findAll(getSpecification(filter), pageable)
				.map(SupportCityhallEntity -> SupportCityhallConverter.toDto(SupportCityhallEntity));
	}

	public List<SupportCityhallResource> findByFilter(SupportCityhallFilter filter) {
		LOG.info("SupportCityhallController END : findByFilter");
		return SupportCityhallConverter.listToDto(repository.findAll(getSpecification(filter)));
	}

	public SupportCityhallResource save(SupportCityhallResource resource, OAuth2Authentication auth) throws Exception {
		LOG.info("SupportCityhallController END : save");

		if (auth != null) {

			String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

			resource.setCreateUser(user);
		} else {
			throw new Exception("Usuario ausente ou não capturado.");
		}

		return SupportCityhallConverter.toDto(repository.save(SupportCityhallConverter.toEntity(resource)));

	}

	public SupportCityhallResource update(SupportCityhallResource resource, Long id, OAuth2Authentication auth) {

		try {
			findOne(id);

			if (auth != null) {

				String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

				resource.setUpdateUser(user);
			}

			LOG.info("SupportCityhallController END : update");

		} catch (Exception e) {
			e.getCause().getMessage();
			LOG.error("SupportCityhallController ERROR : update");
		}

		resource.setId(id);

		LOG.info("SupportCityhallController END : update");

		return SupportCityhallConverter.toDto(repository.save(SupportCityhallConverter.toEntity(resource)));

	}

	public void delete(Long id) {
		try {
			findOne(id);
			repository.deleteById(id);
			LOG.info("SupportCityhallController END : delete");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("SupportCityhallController ERROR : delete");
		}
	}

	private Specification<SupportCityhallEntity> getSpecification(SupportCityhallFilter filter) {

		if (filter != null) {
			Specification<SupportCityhallEntity> spec = Specification
					.where((filter.getId() == null) ? null : SupportCityhallSpecification.isNotNullId());

			spec = (filter.getId() == null) ? spec : spec.and(SupportCityhallSpecification.equalId(filter.getId()));

			return spec;

		} else {
			return null;
		}

	}

}
