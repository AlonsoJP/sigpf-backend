package br.com.sigpf.service.finances;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import br.com.sigpf.converter.SupportCityhallAppendConverter;
import br.com.sigpf.entity.SupportCityhallAppendEntity;
import br.com.sigpf.filter.SupportCityhallAppendFilter;
import br.com.sigpf.repository.SupportCityhallAppendRepository;
import br.com.sigpf.resource.SupportCityhallAppendResource;
import br.com.sigpf.service.user.UserService;
import br.com.sigpf.specification.SupportCityhallAppendSpecification;
import br.com.sigpf.util.Appends;
import br.com.sigpf.util.GetInfoToken;
import br.com.sigpf.util.ValidationAppends;

@Service
public class SupportCityhallAppendService {

	@Value("${path.appends.root}")
	private String root;

	@Value("${path.appends.support-append}")
	private String path;

	ValidationAppends validation = new ValidationAppends();

	@Autowired
	private SupportCityhallAppendRepository repository;

	@Autowired
	UserService service;

	GetInfoToken getInfoToken = new GetInfoToken();

	public static final Logger LOG = LogManager.getLogger(SupportCityhallAppendEntity.class);

	public Iterable<SupportCityhallAppendEntity> findAll() throws Exception {

		try {
			LOG.info("SupportCityhallAppend Controller END : findAll");

			return repository.findAll();
		} catch (Exception e) {
			e.getCause().getMessage();

			LOG.error("SupportCityhallAppend ERROR : findAll");

			return null;
		}

	}

	public Optional<SupportCityhallAppendResource> findOne(Long id) {
		return SupportCityhallAppendConverter.toOptionalDto(repository.findById(id));
	}

	public Page<SupportCityhallAppendResource> findByFilter(SupportCityhallAppendFilter filter, Pageable pageable) {
		return repository.findAll(getSpecification(filter), pageable)
				.map(SupportCityhallAppendEntity -> SupportCityhallAppendConverter.toDto(SupportCityhallAppendEntity));
	}

	public SupportCityhallAppendResource save(SupportCityhallAppendResource resource, OAuth2Authentication auth)
			throws Exception {

		if (auth != null) {

			String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

			resource.setCreateUser(user);
			LOG.info("SupportCityhallAppend END : save");
		} else {
			throw new Exception("Usuario ausente ou não capturado.");
		}

		return SupportCityhallAppendConverter.toDto(repository.save(SupportCityhallAppendConverter.toEntity(resource)));

	}

	public String saveDocument(MultipartFile document) throws Exception {

		try {

			validation.validationDocuments(document.getOriginalFilename());

			LOG.info("SupportCityhallAppend Controller END : uploadDocument");
			return Appends.saveDocuments(document, root, path);

		} catch (NullPointerException e) {
			LOG.error("SupportCityhallAppend Controller NullPointerException ERROR : uploadDocument");
			throw new NullPointerException(e.getMessage());
		} catch (RuntimeException e) {
			LOG.error("SupportCityhallAppend Controller RuntimeException ERROR : uploadDocument");
			throw new RuntimeException(e.getMessage());
		}

	}

	public SupportCityhallAppendResource update(SupportCityhallAppendResource resource, Long id,
			OAuth2Authentication auth) {

		try {
			findOne(id);

			if (auth != null) {

				String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

				resource.setUpdateUser(user);
			}

			LOG.info("SupportCityhallAppend END : update");

		} catch (Exception e) {
			e.getCause().getMessage();
			LOG.error("SupportCityhallAppend ERROR : update");
		}

		resource.setId(id);

		return SupportCityhallAppendConverter.toDto(repository.save(SupportCityhallAppendConverter.toEntity(resource)));

	}

	public void delete(Long id) {
		try {
			findOne(id);
			repository.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("SupportCityhallAppend ERROR : delete");
		}
	}

	private Specification<SupportCityhallAppendEntity> getSpecification(SupportCityhallAppendFilter filter) {

		if (filter != null) {
			Specification<SupportCityhallAppendEntity> spec = Specification
					.where((filter.getId() == null) ? null : SupportCityhallAppendSpecification.isNotNullId());

			spec = (filter.getId() == null) ? spec
					: spec.and(SupportCityhallAppendSpecification.equalId(filter.getId()));
			spec = (StringUtils.isEmpty(filter.getDescription())) ? spec
					: spec.and(SupportCityhallAppendSpecification.likeDescription(filter.getDescription()));

			// referentes a tabela relacionada
			spec = (filter.getSupportId() == null) ? spec
					: spec.and(SupportCityhallAppendSpecification.equalSupport(filter.getSupportId()));

			return spec;

		} else {
			return null;
		}

	}

}
