package br.com.sigpf.service.finances;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import br.com.sigpf.converter.OutputConverter;
import br.com.sigpf.entity.OutputEntity;
import br.com.sigpf.enums.TypeTransactionEnum;
import br.com.sigpf.filter.OutputFilter;
import br.com.sigpf.repository.OutputRepository;
import br.com.sigpf.resource.BankResource;
import br.com.sigpf.resource.HistoryBankResource;
import br.com.sigpf.resource.OutputResource;
import br.com.sigpf.service.bank.BankService;
import br.com.sigpf.service.bank.HistoryBankService;
import br.com.sigpf.service.user.UserService;
import br.com.sigpf.specification.OutputSpecification;
import br.com.sigpf.util.GetInfoToken;
import br.com.sigpf.util.ProtocolGenerator;

@Service
public class OutputService {

	@Autowired
	private OutputRepository repository;

	@Autowired
	UserService service;

	@Autowired
	BankService serviceBank;

	@Autowired
	HistoryBankService serviceHistory;

	GetInfoToken getInfoToken = new GetInfoToken();

	ProtocolGenerator protocolGenerator = new ProtocolGenerator();

	public static final Logger LOG = LogManager.getLogger(OutputEntity.class);

	public List<OutputResource> findAll() throws Exception {

		try {
			LOG.info("OutpuController END : findAll");

			return OutputConverter.listToDto(repository.findAll());
		} catch (Exception e) {

			LOG.error("OutpuController ERROR : findAll");

			throw new Exception(e.getCause().getMessage());

		}

	}

	public Optional<OutputResource> findOne(Long id) {
		LOG.info("OutpuController END : findOne");
		return OutputConverter.toOptionalDto(repository.findById(id));
	}

	public Page<OutputResource> findByFilter(OutputFilter filter, Pageable pageable) {
		LOG.info("OutpuController END : findByFilter");
		return repository.findAll(getSpecification(filter), pageable)
				.map(OutputEntity -> OutputConverter.toDto(OutputEntity));
	}

	public List<OutputResource> findByFilter(OutputFilter filter) {
		LOG.info("OutpuController END : findByFilter");
		return OutputConverter.listToDto(repository.findAll(getSpecification(filter)));
	}

	public OutputResource save(OutputResource resource, OAuth2Authentication auth) throws Exception {
		if (resource.getTypeTransaction().equalsIgnoreCase(TypeTransactionEnum.SAIDA.getDescricao().toString())) {

			if (resource.getAmountTransaction() > 0) {
				if (auth != null) {

					String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

					resource.setCreateUser(user);
				} else {
					LOG.error("OutpuController ERROR : save");
					throw new Exception("Usuario ausente ou não capturado.");
				}

				// inserir protocolo
				resource.setProtocolTransaction(protocolGenerator.generateProtocol());

				// atualizar o caixa através da entrada

				try {
					BankResource bank = new BankResource();

					HistoryBankResource historyResource = new HistoryBankResource();

					bank = serviceBank.findByExercise(true);

					// verificar disponibilidade do saque
					if (bank.getSale() < resource.getAmountTransaction()) {
						throw new Exception("Saldo insuficiente. Saldo disponível de: R$" + bank.getSale());
					}
					historyResource.setType_transaction(resource.getTypeTransaction());
					historyResource.setProtocol_transaction(resource.getProtocolTransaction());
					historyResource.setAmount_transaction(resource.getAmountTransaction());
					historyResource.setExercise_bank(bank.getExercise_bank());
					historyResource.setSale_old(bank.getSale());
					bank.setSale(bank.getSale() - resource.getAmountTransaction());
					historyResource.setSale_new(bank.getSale());
					historyResource.setBank(bank);

					OutputResource result = OutputConverter.toDto(repository.save(OutputConverter.toEntity(resource)));

					historyResource.setId_transaction(result.getId());

					serviceHistory.save(historyResource, auth);
					serviceBank.update(bank, bank.getId(), auth);
					LOG.info("OutpuController END : save");

					return result;
				} catch (Exception e) {

					LOG.error("OutpuController ERROR : save");
					throw new Exception(e.getMessage());

				}

			} else {
				LOG.error("OutpuController ERROR : save");
				throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Não foi encontrado nenhum valor.");

			}

		} else {
			LOG.error("OutpuController ERROR : save");
			throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Método não realiza ENTRADAS ou CANCELAMENTOS.");
		}

	}

	public OutputResource update(String resource, Long id, OAuth2Authentication auth) throws Exception {

		Optional<OutputResource> input = findOne(id);
		OutputResource outputResource = input.get();

		BankResource bank = new BankResource();

		HistoryBankResource historyResource = new HistoryBankResource();

		bank = serviceBank.findByExercise(true);

		historyResource.setSale_old(bank.getSale());
		try {

			// verificar usuario que atualizou
			if (auth != null) {

				String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

				outputResource.setUpdateUser(user);
			} else {
				throw new Exception("Usuario ausente ou não capturado.");
			}

			// verificar tipo de ESTORNO/CANCELAMENTO ou ENTRADA
			if (resource.equalsIgnoreCase(TypeTransactionEnum.SAIDA.getDescricao().toString())) {
				outputResource.setTypeTransaction(TypeTransactionEnum.SAIDA.getDescricao().toString());
				bank.setSale(bank.getSale() - outputResource.getAmountTransaction());

			}
			if (resource.equalsIgnoreCase(TypeTransactionEnum.CANCELADO.getDescricao().toString())) {

				outputResource.setTypeTransaction(TypeTransactionEnum.CANCELADO.getDescricao().toString());
				bank.setSale(bank.getSale() + outputResource.getAmountTransaction());

			}
			if (resource.equalsIgnoreCase(TypeTransactionEnum.ENTRADA.getDescricao().toString())) {

				throw new Exception("Método não realiza ENTRADAS.");

			}

			historyResource.setId_transaction(outputResource.getId());
			historyResource.setType_transaction(outputResource.getTypeTransaction());
			historyResource.setProtocol_transaction(outputResource.getProtocolTransaction());
			historyResource.setAmount_transaction(outputResource.getAmountTransaction());
			historyResource.setExercise_bank(bank.getExercise_bank());
			historyResource.setSale_new(bank.getSale());
			historyResource.setBank(bank);

			serviceHistory.save(historyResource, auth);
			serviceBank.update(bank, bank.getId(), auth);

		} catch (Exception e) {
			LOG.error("InputController ERROR : update");
			throw new Exception(e.getMessage());
		}

		outputResource.setId(id);

		LOG.info("InputController END : update");
		return OutputConverter.toDto(repository.save(OutputConverter.toEntity(outputResource)));
	}

	public void delete(Long id) {
		try {
			findOne(id);
			repository.deleteById(id);
			LOG.info("OutpuController END : delete");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("OutpuController ERROR : delete");
		}
	}

	private Specification<OutputEntity> getSpecification(OutputFilter filter) {

		if (filter != null) {
			Specification<OutputEntity> spec = Specification
					.where((filter.getId() == null) ? null : OutputSpecification.isNotNullId());

			spec = (filter.getAmountTransaction() == null) ? spec
					: spec.and(OutputSpecification.equalAmount(filter.getAmountTransaction()));

			spec = (filter.getId() == null) ? spec : spec.and(OutputSpecification.equalId(filter.getId()));
			spec = (filter.getDateBegin() == null) ? spec
					: spec.and(OutputSpecification.createDateLessThanOrEqual(filter.getDateEnd()));
			spec = (filter.getDateEnd() == null) ? spec
					: spec.and(OutputSpecification.createGreaterThanOrEqual(filter.getDateBegin()));

			return spec;

		} else {
			return null;
		}

	}
}
