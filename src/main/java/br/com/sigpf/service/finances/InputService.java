package br.com.sigpf.service.finances;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import br.com.sigpf.converter.InputConverter;
import br.com.sigpf.entity.InputEntity;
import br.com.sigpf.enums.TypeTransactionEnum;
import br.com.sigpf.filter.InputFilter;
import br.com.sigpf.repository.InputRepository;
import br.com.sigpf.resource.BankResource;
import br.com.sigpf.resource.HistoryBankResource;
import br.com.sigpf.resource.InputResource;
import br.com.sigpf.service.bank.BankService;
import br.com.sigpf.service.bank.HistoryBankService;
import br.com.sigpf.service.user.UserService;
import br.com.sigpf.specification.InputSpecification;
import br.com.sigpf.util.GetInfoToken;
import br.com.sigpf.util.ProtocolGenerator;

@Service
public class InputService {

	@Autowired
	private InputRepository repository;

	@Autowired
	UserService service;

	@Autowired
	BankService serviceBank;

	@Autowired
	HistoryBankService serviceHistory;

	GetInfoToken getInfoToken = new GetInfoToken();

	ProtocolGenerator protocolGenerator = new ProtocolGenerator();

	public static final Logger LOG = LogManager.getLogger(InputEntity.class);

	public List<InputResource> findAll() throws Exception {

		try {
			LOG.info("InputController END : findAll");

			return InputConverter.listToDto(repository.findAll());
		} catch (Exception e) {

			LOG.error("InputController ERROR : findAll");

			throw new Exception(e.getCause().getMessage());

		}

	}

	public Optional<InputResource> findOne(Long id) {
		LOG.info("InputController END : findOne");
		return InputConverter.toOptionalDto(repository.findById(id));
	}

	public Page<InputResource> findByFilter(InputFilter filter, Pageable pageable) {
		LOG.info("InputController END : findByFilter");
		return repository.findAll(getSpecification(filter), pageable)
				.map(InputEntity -> InputConverter.toDto(InputEntity));
	}

	public List<InputResource> findByFilter(InputFilter filter) {
		LOG.info("InputController END : findByFilter");
		return InputConverter.listToDto(repository.findAll(getSpecification(filter)));
	}

	public InputResource save(InputResource resource, OAuth2Authentication auth) throws Exception {

		if (resource.getType_transaction().equalsIgnoreCase(TypeTransactionEnum.ENTRADA.getDescricao().toString())) {

			if (resource.getAmount_transaction() > 0) {
				if (auth != null) {

					String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

					resource.setCreateUser(user);
				} else {
					LOG.error("InputController ERROR : save");
					throw new Exception("Usuario ausente ou não capturado.");
				}

				// inserir protocolo
				resource.setProtocol_transaction(protocolGenerator.generateProtocol());

				// atualizar o caixa através da entrada

				try {
					BankResource bank = new BankResource();

					HistoryBankResource historyResource = new HistoryBankResource();

					bank = serviceBank.findByExercise(true);

					historyResource.setType_transaction(resource.getType_transaction());
					historyResource.setProtocol_transaction(resource.getProtocol_transaction());
					historyResource.setAmount_transaction(resource.getAmount_transaction());
					historyResource.setExercise_bank(bank.getExercise_bank());
					historyResource.setSale_old(bank.getSale());
					bank.setSale(bank.getSale() + resource.getAmount_transaction());
					historyResource.setSale_new(bank.getSale());
					historyResource.setBank(bank);

					InputResource result = InputConverter.toDto(repository.save(InputConverter.toEntity(resource)));

					historyResource.setId_transaction(result.getId());

					serviceHistory.save(historyResource, auth);
					serviceBank.update(bank, bank.getId(), auth);
					System.out.println(result);
					LOG.info("InputController END : save");
					return result;
				} catch (Exception e) {

					LOG.error("InputController ERROR : save");
					throw new Exception(e.getMessage());

				}

			} else {
				LOG.error("InputController ERROR : save");
				throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Não foi encontrado nenhum valor.");

			}

		} else {
			LOG.error("InputController ERROR : save");
			throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Método não realiza ENTRADAS ou CANCELAMENTOS.");
		}
	}

	public InputResource update(String resource, Long id, OAuth2Authentication auth) throws Exception {

		Optional<InputResource> input = findOne(id);
		InputResource inputResource = input.get();

		BankResource bank = new BankResource();

		HistoryBankResource historyResource = new HistoryBankResource();

		bank = serviceBank.findByExercise(true);

		historyResource.setSale_old(bank.getSale());
		try {

			// verificar usuario que atualizou
			if (auth != null) {

				String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

				inputResource.setUpdateUser(user);
			} else {
				throw new Exception("Usuario ausente ou não capturado.");
			}

			// verificar tipo de ESTORNO/CANCELAMENTO ou ENTRADA
			if (resource.equalsIgnoreCase(TypeTransactionEnum.ENTRADA.getDescricao().toString())) {
				inputResource.setType_transaction(TypeTransactionEnum.ENTRADA.getDescricao().toString());
				bank.setSale(bank.getSale() + inputResource.getAmount_transaction());

			}
			if (resource.equalsIgnoreCase(TypeTransactionEnum.CANCELADO.getDescricao().toString())) {

				inputResource.setType_transaction(TypeTransactionEnum.CANCELADO.getDescricao().toString());
				bank.setSale(bank.getSale() - inputResource.getAmount_transaction());

			}
			if (resource.equalsIgnoreCase(TypeTransactionEnum.SAIDA.getDescricao().toString())) {

				throw new Exception("Método não realiza SAÍDAS.");

			}

			historyResource.setId_transaction(inputResource.getId());
			historyResource.setType_transaction(inputResource.getType_transaction());
			historyResource.setProtocol_transaction(inputResource.getProtocol_transaction());
			historyResource.setAmount_transaction(inputResource.getAmount_transaction());
			historyResource.setUpdateUser(inputResource.getUpdateUser());
			historyResource.setExercise_bank(bank.getExercise_bank());
			historyResource.setSale_new(bank.getSale());
			historyResource.setBank(bank);

			serviceHistory.save(historyResource, auth);
			serviceBank.update(bank, bank.getId(), auth);

		} catch (Exception e) {
			LOG.error("InputController ERROR : update");
			throw new Exception(e.getMessage());
		}

		inputResource.setId(id);

		LOG.info("InputController END : update");
		return InputConverter.toDto(repository.save(InputConverter.toEntity(inputResource)));
	}

	private Specification<InputEntity> getSpecification(InputFilter filter) {

		if (filter != null) {
			Specification<InputEntity> spec = Specification
					.where((filter.getId() == null) ? null : InputSpecification.isNotNullId());

			spec = (filter.getId() == null) ? spec : spec.and(InputSpecification.equalId(filter.getId()));// TODO não
			spec = (filter.getAmountTransaction() == null) ? spec
					: spec.and(InputSpecification.equalAmount(filter.getAmountTransaction()));

			spec = (StringUtils.isEmpty(filter.getTypeTransaction())) ? spec
					: spec.and(InputSpecification.likeTypeTransaction((filter.getTypeTransaction())));

			spec = (StringUtils.isEmpty(filter.getProtocolTransaction())) ? spec
					: spec.and(InputSpecification.likeProtocolTransaction(filter.getProtocolTransaction()));
			spec = (StringUtils.isEmpty(filter.getDescription())) ? spec
					: spec.and(InputSpecification.likeDescription(filter.getDescription()));
			spec = (StringUtils.isEmpty(filter.getCreateUser())) ? spec
					: spec.and(InputSpecification.likeCreateUser(filter.getCreateUser()));
			spec = (StringUtils.isEmpty(filter.getUpdateUser())) ? spec
					: spec.and(InputSpecification.likeUpdateUser(filter.getUpdateUser()));
			spec = (filter.getDateBegin() == null) ? spec
					: spec.and(InputSpecification.createDateLessThanOrEqual(filter.getDateEnd()));
			spec = (filter.getDateEnd() == null) ? spec
					: spec.and(InputSpecification.createGreaterThanOrEqual(filter.getDateBegin()));

			return spec;

		} else {
			return null;
		}

	}

}
