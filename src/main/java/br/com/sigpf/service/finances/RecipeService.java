package br.com.sigpf.service.finances;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

import br.com.sigpf.converter.RecipeConverter;
import br.com.sigpf.entity.RecipeEntity;
import br.com.sigpf.filter.RecipeFilter;
import br.com.sigpf.repository.RecipeRepository;
import br.com.sigpf.resource.RecipeResource;
import br.com.sigpf.service.user.UserService;
import br.com.sigpf.specification.RecipeSpecification;
import br.com.sigpf.util.GetInfoToken;

@Service
public class RecipeService {

	@Autowired
	private RecipeRepository repository;

	@Autowired
	UserService service;

	GetInfoToken getInfoToken = new GetInfoToken();

	public static final Logger LOG = LogManager.getLogger(RecipeEntity.class);

	public List<RecipeResource> findAll() throws Exception {

		try {
			LOG.info("RecipeController END : findAll");

			return RecipeConverter.listToDto(repository.findAll());
		} catch (Exception e) {

			LOG.error("RecipeController ERROR : findAll");

			throw new Exception(e.getCause().getMessage());

		}

	}

	public Optional<RecipeResource> findOne(Long id) {
		LOG.info("RecipeController END : findOne");
		return RecipeConverter.toOptionalDto(repository.findById(id));
	}

	public Page<RecipeResource> findByFilter(RecipeFilter filter, Pageable pageable) {
		LOG.info("RecipeController END : findByFilter");
		return repository.findAll(getSpecification(filter), pageable)
				.map(RecipeEntity -> RecipeConverter.toDto(RecipeEntity));
	}

	public List<RecipeResource> findByFilter(RecipeFilter filter) {
		LOG.info("RecipeController END : findByFilter");
		return RecipeConverter.listToDto(repository.findAll(getSpecification(filter)));
	}

	public RecipeResource save(RecipeResource resource, OAuth2Authentication auth) throws Exception {
		LOG.info("RecipeController END : save");

		if (auth != null) {

			String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

			resource.setCreateUser(user);
		} else {
			throw new Exception("Usuario ausente ou não capturado.");
		}

		return RecipeConverter.toDto(repository.save(RecipeConverter.toEntity(resource)));

	}

	public RecipeResource update(RecipeResource resource, Long id, OAuth2Authentication auth) {

		try {
			findOne(id);

			if (auth != null) {

				String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

				resource.setUpdateUser(user);
			}

			LOG.info("RecipeController END : update");

		} catch (Exception e) {
			e.getCause().getMessage();
			LOG.error("RecipeController ERROR : update");
		}

		resource.setId(id);

		LOG.info("RecipeController END : update");

		return RecipeConverter.toDto(repository.save(RecipeConverter.toEntity(resource)));

	}

	private Specification<RecipeEntity> getSpecification(RecipeFilter filter) {

		if (filter != null) {
			Specification<RecipeEntity> spec = Specification
					.where((filter.getId() == null) ? null : RecipeSpecification.isNotNullId());

			spec = (filter.getId() == null) ? spec : spec.and(RecipeSpecification.equalId(filter.getId()));

			spec = (filter.getDateBegin() == null) ? spec
					: spec.and(RecipeSpecification.createDateLessThanOrEqual(filter.getDateEnd()));
			spec = (filter.getDateEnd() == null) ? spec
					: spec.and(RecipeSpecification.createGreaterThanOrEqual(filter.getDateBegin()));

			return spec;

		} else {
			return null;
		}

	}

	public void delete(Long id) {
		try {
			findOne(id);
			repository.deleteById(id);
			LOG.info("RecipeController END : delete");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("RecipeController ERROR : delete");
		}
	}

}
