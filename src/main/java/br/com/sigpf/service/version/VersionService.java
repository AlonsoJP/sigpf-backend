package br.com.sigpf.service.version;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sigpf.entity.VersionEntity;
import br.com.sigpf.repository.VersionRepository;

@Service
public class VersionService {

	public static final Logger LOG = LogManager.getLogger(VersionEntity.class);

	@Autowired
	VersionRepository repository;

	public VersionEntity findLast() {

		LOG.info("StudentController END : findLast");

		return repository.findTopByOrderByIdDesc();

	}

	public List<VersionEntity> findAllDesc() {
		LOG.info("StudentController END : findAllDesc");

		return repository.findTop10ByOrderByIdDesc();

	}

}
