package br.com.sigpf.service.address;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

import br.com.sigpf.converter.DistrictConverter;
import br.com.sigpf.entity.DistrictEntity;
import br.com.sigpf.filter.DistrictFilter;
import br.com.sigpf.repository.DistrictRepository;
import br.com.sigpf.resource.DistrictResource;
import br.com.sigpf.service.user.UserService;
import br.com.sigpf.specification.DistrictSpecification;
import br.com.sigpf.util.GetInfoToken;

@Service
public class DistrictService {

	public static final Logger LOG = LogManager.getLogger(DistrictEntity.class);

	@Autowired
	private DistrictRepository repository;

	@Autowired
	UserService service;

	GetInfoToken getInfoToken = new GetInfoToken();

	public Iterable<DistrictEntity> findAll() throws Exception {

		try {
			LOG.info("DistrictController END : findAll");

			return repository.findAll();
		} catch (Exception e) {
			e.getCause().getMessage();

			LOG.error("DistrictController ERROR : findAll");

			return null;
		}

	}

	public Optional<DistrictResource> findOne(Long id) {
		return DistrictConverter.toOptionalDto(repository.findById(id));
	}

	public Page<DistrictResource> findByFilter(DistrictFilter districtFilter, Pageable pageable) {
		LOG.info("StreetController END : findByFilter");
		return repository.findAll(getSpecification(districtFilter), pageable)
				.map(DistrictEntity -> DistrictConverter.toDto(DistrictEntity));
	}

	public List<DistrictResource> findByFilter(DistrictFilter filter) {
		LOG.info("StreetController END : findByFilter");
		return DistrictConverter.listToDto(repository.findAll(getSpecification(filter)));
	}

	public DistrictResource save(DistrictResource resource, OAuth2Authentication auth) throws Exception {

		LOG.info("StreetController END : save");

		if (auth != null) {

			String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

			resource.setCreateUser(user);
		} else {
			throw new Exception("Usuario ausente ou não capturado.");
		}

		return DistrictConverter.toDto(repository.save(DistrictConverter.toEntity(resource)));

	}

	public DistrictResource update(DistrictResource resource, Long id, OAuth2Authentication auth) {

		try {
			findOne(id);

			if (auth != null) {

				String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

				resource.setUpdateUser(user);
			}

			LOG.error("DistrictController END : update");

		} catch (Exception e) {
			e.getCause().getMessage();
			LOG.error("DistrictController ERROR : update");
		}

		resource.setId(id);

		return DistrictConverter.toDto(repository.save(DistrictConverter.toEntity(resource)));

	}

	public Boolean delete(Long id) {
		try {
			findOne(id);
			repository.deleteById(id);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("DistrictController ERROR : delete");
			return false;
		}
	}

	private Specification<DistrictEntity> getSpecification(DistrictFilter districtFilter) {

		if (districtFilter != null) {
			Specification<DistrictEntity> spec = Specification
					.where((districtFilter.getId() == null) ? null : DistrictSpecification.isNotNullId());

			spec = (districtFilter.getId() == null) ? spec
					: spec.and(DistrictSpecification.equalId(districtFilter.getId()));
			spec = (StringUtils.isEmpty(districtFilter.getName_district())) ? spec
					: spec.and(DistrictSpecification.likeName(districtFilter.getName_district()));

			return spec;

		} else {
			return null;
		}

	}

}
