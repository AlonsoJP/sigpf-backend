package br.com.sigpf.service.address;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

import br.com.sigpf.converter.StreetConverter;
import br.com.sigpf.entity.StreetEntity;
import br.com.sigpf.filter.StreetFilter;
import br.com.sigpf.repository.StreetRepository;
import br.com.sigpf.resource.StreetResource;
import br.com.sigpf.service.user.UserService;
import br.com.sigpf.specification.StreetSpecification;
import br.com.sigpf.util.GetInfoToken;

@Service
public class StreetService {

	@Autowired
	private StreetRepository repository;

	@Autowired
	UserService service;

	GetInfoToken getInfoToken = new GetInfoToken();

	public static final Logger LOG = LogManager.getLogger(StreetEntity.class);

	public Iterable<StreetEntity> findAll() throws Exception {

		try {
			LOG.info("StreetController END : findAll");

			return repository.findAll();
		} catch (Exception e) {
			e.getCause().getMessage();

			LOG.error("StreetController ERROR : findAll");

			return null;
		}

	}

	public Optional<StreetResource> findOne(Long id) {
		return StreetConverter.toOptionalDto(repository.findById(id));
	}

	public Page<StreetResource> findByFilter(StreetFilter streetFilter, Pageable pageable) {
		LOG.info("StreetController END : findByFilter");
		return repository.findAll(getSpecification(streetFilter), pageable)
				.map(StreetEntity -> StreetConverter.toDto(StreetEntity));
	}

	public List<StreetResource> findByFilter(StreetFilter filter) {
		LOG.info("StreetController END : findByFilter");
		return StreetConverter.listToDto(repository.findAll(getSpecification(filter)));
	}

	public StreetResource save(StreetResource resource, OAuth2Authentication auth) throws Exception {

		if (auth != null) {

			String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

			resource.setCreateUser(user);
		} else {
			throw new Exception("Usuario ausente ou não capturado.");
		}

		return StreetConverter.toDto(repository.save(StreetConverter.toEntity(resource)));

	}

	public StreetResource update(StreetResource resource, Long id, OAuth2Authentication auth) {

		try {
			findOne(id);

			if (auth != null) {

				String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

				resource.setUpdateUser(user);
			}
		} catch (Exception e) {
			e.getCause().getMessage();
			LOG.error("StreetController ERROR : update");
		}

		resource.setId(id);

		return StreetConverter.toDto(repository.save(StreetConverter.toEntity(resource)));

	}

	public Boolean delete(Long id) {
		try {
			findOne(id);
			repository.deleteById(id);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("StreetController ERROR : delete");
			return false;
		}
	}

	private Specification<StreetEntity> getSpecification(StreetFilter streetFilter) {

		if (streetFilter != null) {
			Specification<StreetEntity> spec = Specification
					.where((streetFilter.getId() == null) ? null : StreetSpecification.isNotNullId());

			spec = (streetFilter.getId() == null) ? spec : spec.and(StreetSpecification.equalId(streetFilter.getId()));
			spec = (StringUtils.isEmpty(streetFilter.getName())) ? spec
					: spec.and(StreetSpecification.likeName(streetFilter.getName()));

			return spec;

		} else {
			return null;
		}

	}
}
