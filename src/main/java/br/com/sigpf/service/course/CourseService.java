package br.com.sigpf.service.course;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

import br.com.sigpf.controller.CourseController;
import br.com.sigpf.converter.CourseConverter;
import br.com.sigpf.entity.CourseEntity;
import br.com.sigpf.filter.CourseFilter;
import br.com.sigpf.repository.CourseRepository;
import br.com.sigpf.resource.CourseResource;
import br.com.sigpf.service.user.UserService;
import br.com.sigpf.specification.CourseSpecification;
import br.com.sigpf.util.GetInfoToken;

@Service
public class CourseService {

	@Autowired
	private CourseRepository repository;

	@Autowired
	UserService service;

	GetInfoToken getInfoToken = new GetInfoToken();

	public static final Logger LOG = LogManager.getLogger(CourseController.class);

	public Iterable<CourseEntity> findAll() throws Exception {

		try {
			LOG.info("CourseController END : findAll");

			return repository.findAll();
		} catch (Exception e) {
			e.getCause().getMessage();

			LOG.error("CourseController ERROR : findAll");

			return null;
		}

	}

	public Optional<CourseResource> findOne(Long id) {
		return CourseConverter.toOptionalDto(repository.findById(id));
	}

	public Page<CourseResource> findByFilter(CourseFilter filter, Pageable pageable) {
		LOG.info("CourseController END : findByFilter");
		return repository.findAll(getSpecification(filter), pageable)
				.map(CourseEntity -> CourseConverter.toDto(CourseEntity));
	}

	public List<CourseResource> findByFilter(CourseFilter filter) {
		LOG.info("CourseController END : findByFilter");
		return CourseConverter.listToDto(repository.findAll(getSpecification(filter)));
	}

	public CourseResource save(CourseResource resource, OAuth2Authentication auth) throws Exception {

		if (auth != null) {

			String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

			resource.setCreateUser(user);
			LOG.info("CourseController END : save");
		} else {
			throw new Exception("Usuario ausente ou não capturado.");
		}

		return CourseConverter.toDto(repository.save(CourseConverter.toEntity(resource)));

	}

	public CourseResource update(CourseResource resource, Long id, OAuth2Authentication auth) {

		try {
			findOne(id);

			if (auth != null) {

				String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

				resource.setUpdateUser(user);
			}

			LOG.info("CourseController END : update");

		} catch (Exception e) {
			e.getCause().getMessage();
			LOG.error("CourseController ERROR : update");
		}

		resource.setId(id);

		return CourseConverter.toDto(repository.save(CourseConverter.toEntity(resource)));

	}

	public Boolean delete(Long id) {
		try {
			findOne(id);
			repository.deleteById(id);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("CourseController ERROR : delete");
			return false;
		}
	}

	private Specification<CourseEntity> getSpecification(CourseFilter filter) {

		if (filter != null) {
			Specification<CourseEntity> spec = Specification
					.where((filter.getId() == null) ? null : CourseSpecification.isNotNullId());

			spec = (filter.getId() == null) ? spec : spec.and(CourseSpecification.equalId(filter.getId()));
			spec = (StringUtils.isEmpty(filter.getName())) ? spec
					: spec.and(CourseSpecification.likeName(filter.getName()));
			spec = (StringUtils.isEmpty(filter.getArea())) ? spec
					: spec.and(CourseSpecification.likeArea(filter.getArea()));

			return spec;

		} else {
			return null;
		}

	}

}
