package br.com.sigpf.service.member;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import br.com.sigpf.converter.MemberAssociationConverter;
import br.com.sigpf.entity.MemberAssociationEntity;
import br.com.sigpf.filter.MemberAssociationFilter;
import br.com.sigpf.repository.MemberAssociationRepository;
import br.com.sigpf.resource.MemberAssociationResource;
import br.com.sigpf.service.user.UserService;
import br.com.sigpf.specification.MemberAssociationSpecification;
import br.com.sigpf.util.Appends;
import br.com.sigpf.util.GetInfoToken;
import br.com.sigpf.util.ValidationAppends;

@Service
public class MemberAssociationService {

	@Autowired
	private MemberAssociationRepository repository;

	@Autowired
	UserService service;

	GetInfoToken getInfoToken = new GetInfoToken();

	ValidationAppends validation = new ValidationAppends();

	@Value("${path.appends.root}")
	private String root;

	@Value("${path.appends.member}")
	private String path;

	public static final Logger LOG = LogManager.getLogger(MemberAssociationEntity.class);

	public List<MemberAssociationResource> findAll() throws Exception {

		try {
			LOG.info("MemberAssociationController END : findAll");

			return MemberAssociationConverter.listToDto(repository.findAll());
		} catch (Exception e) {

			LOG.error("MemberAssociationController ERROR : findAll");

			throw new Exception(e.getCause().getMessage());

		}

	}

	public Optional<MemberAssociationResource> findOne(Long id) {
		LOG.info("MemberAssociationController END : findOne");
		return MemberAssociationConverter.toOptionalDto(repository.findById(id));
	}

	public Page<MemberAssociationResource> findByFilter(MemberAssociationFilter filter, Pageable pageable) {
		LOG.info("MemberAssociationController END : findByFilter");
		return repository.findAll(getSpecification(filter), pageable)
				.map(MemberAssociationEntity -> MemberAssociationConverter.toDto(MemberAssociationEntity));
	}

	public List<MemberAssociationResource> findByFilter(MemberAssociationFilter filter) {
		LOG.info("MemberAssociationController END : findByFilter");
		return MemberAssociationConverter.listToDto(repository.findAll(getSpecification(filter)));
	}

	public MemberAssociationResource save(MemberAssociationResource resource, OAuth2Authentication auth)
			throws Exception {
		LOG.info("MemberAssociationController END : save");

		if (auth != null) {

			String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

			resource.setCreateUser(user);
		} else {
			throw new Exception("Usuario ausente ou não capturado.");
		}

		return MemberAssociationConverter.toDto(repository.save(MemberAssociationConverter.toEntity(resource)));

	}

	public String savePhoto(MultipartFile photo) throws Exception {

		try {

			validation.validationImage(photo.getOriginalFilename());

			LOG.info("MemberAssociationController END : savePhoto");
			return Appends.saveDocuments(photo, root, path);

		} catch (NullPointerException e) {
			LOG.error("MemberAssociationController NullPointerException ERROR : savePhoto");
			throw new NullPointerException(e.getMessage());
		} catch (RuntimeException e) {
			LOG.error("MemberAssociationController RuntimeException ERROR : savePhoto");
			throw new RuntimeException(e.getMessage());
		}

	}

	public MemberAssociationResource update(MemberAssociationResource resource, Long id, OAuth2Authentication auth) {

		try {
			findOne(id);

			if (auth != null) {

				String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

				resource.setUpdateUser(user);
			}

			LOG.info("MemberAssociationController END : update");

		} catch (Exception e) {
			e.getCause().getMessage();
			LOG.error("MemberAssociationController ERROR : update");
		}

		resource.setId(id);

		LOG.info("MemberAssociationController END : update");

		return MemberAssociationConverter.toDto(repository.save(MemberAssociationConverter.toEntity(resource)));

	}

	public void delete(Long id) {
		try {
			findOne(id);
			repository.deleteById(id);
			LOG.info("MemberAssociationController END : delete");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("MemberAssociationController ERROR : delete");
		}
	}

	private Specification<MemberAssociationEntity> getSpecification(MemberAssociationFilter filter) {

		if (filter != null) {
			Specification<MemberAssociationEntity> spec = Specification
					.where((filter.getId() == null) ? null : MemberAssociationSpecification.isNotNullId());

			spec = (filter.getId() == null) ? spec : spec.and(MemberAssociationSpecification.equalId(filter.getId()));// TODO
																														// não
																														// esta
																														// funcionando

			return spec;

		} else {
			return null;
		}

	}

}
