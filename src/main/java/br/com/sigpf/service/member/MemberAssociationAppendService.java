package br.com.sigpf.service.member;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import br.com.sigpf.converter.MemberAssociationAppendConverter;
import br.com.sigpf.entity.MemberAssociationAppendEntity;
import br.com.sigpf.filter.MemberAssociationAppendFilter;
import br.com.sigpf.repository.MemberAssociationAppendRepository;
import br.com.sigpf.resource.MemberAssociationAppendResource;
import br.com.sigpf.service.user.UserService;
import br.com.sigpf.specification.MemberAssociationAppendSpecification;
import br.com.sigpf.util.Appends;
import br.com.sigpf.util.GetInfoToken;
import br.com.sigpf.util.ValidationAppends;

@Service
public class MemberAssociationAppendService {

	@Value("${path.appends.root}")
	private String root;

	@Value("${path.appends.member-append}")
	private String path;

	ValidationAppends validation = new ValidationAppends();

	@Autowired
	private MemberAssociationAppendRepository repository;

	@Autowired
	UserService service;

	GetInfoToken getInfoToken = new GetInfoToken();

	public static final Logger LOG = LogManager.getLogger(MemberAssociationAppendEntity.class);

	public Iterable<MemberAssociationAppendEntity> findAll() throws Exception {

		try {
			LOG.info("MemberAssociationAppendController END : findAll");

			return repository.findAll();
		} catch (Exception e) {
			e.getCause().getMessage();

			LOG.error("MemberAssociationAppendController ERROR : findAll");

			return null;
		}

	}

	public Optional<MemberAssociationAppendResource> findOne(Long id) {
		return MemberAssociationAppendConverter.toOptionalDto(repository.findById(id));
	}

	public Page<MemberAssociationAppendResource> findByFilter(MemberAssociationAppendFilter filter, Pageable pageable) {
		return repository.findAll(getSpecification(filter), pageable).map(
				MemberAssociationAppendEntity -> MemberAssociationAppendConverter.toDto(MemberAssociationAppendEntity));
	}

	public MemberAssociationAppendResource save(MemberAssociationAppendResource resource, OAuth2Authentication auth)
			throws Exception {

		if (auth != null) {

			String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

			resource.setCreateUser(user);
			LOG.info("MemberAssociationAppendController END : save");
		} else {
			throw new Exception("Usuario ausente ou não capturado.");
		}

		return MemberAssociationAppendConverter
				.toDto(repository.save(MemberAssociationAppendConverter.toEntity(resource)));

	}

	public String saveDocument(MultipartFile document) throws Exception {

		try {

			validation.validationDocuments(document.getOriginalFilename());

			LOG.info("MemberAssociationAppendController END : uploadDocument");
			return Appends.saveDocuments(document, root, path);

		} catch (NullPointerException e) {
			LOG.error("MemberAssociationAppendController NullPointerException ERROR : uploadDocument");
			throw new NullPointerException(e.getMessage());
		} catch (RuntimeException e) {
			LOG.error("MemberAssociationAppendController RuntimeException ERROR : uploadDocument");
			throw new RuntimeException(e.getMessage());
		}

	}

	public void deleteDocument(String caminho) throws Exception {

		try {

			LOG.info("MemberAssociationAppendController Controller START : deleteDocument");
			Appends.deleteDocuments(root, caminho);
			LOG.info("MemberAssociationAppendController Controller END : deleteDocument");
		} catch (NullPointerException e) {
			LOG.error("MemberAssociationAppendController Controller NullPointerException ERROR : deleteDocument");
			throw new NullPointerException(e.getMessage());
		} catch (RuntimeException e) {
			LOG.error("MemberAssociationAppendController Controller RuntimeException ERROR : deleteDocument");
			throw new RuntimeException(e.getMessage());
		}

	}

	public MemberAssociationAppendResource update(MemberAssociationAppendResource resource, Long id,
			OAuth2Authentication auth) {

		try {
			findOne(id);

			if (auth != null) {

				String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

				resource.setUpdateUser(user);
			}

			LOG.info("MemberAssociationAppendController END : update");

		} catch (Exception e) {
			e.getCause().getMessage();
			LOG.error("MemberAssociationAppendController ERROR : update");
		}

		resource.setId(id);

		return MemberAssociationAppendConverter
				.toDto(repository.save(MemberAssociationAppendConverter.toEntity(resource)));

	}

	public void delete(Long id) {
		try {
			findOne(id);
			repository.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("MemberAssociationAppendController ERROR : delete");
		}
	}

	private Specification<MemberAssociationAppendEntity> getSpecification(MemberAssociationAppendFilter filter) {

		if (filter != null) {
			Specification<MemberAssociationAppendEntity> spec = Specification
					.where((filter.getId() == null) ? null : MemberAssociationAppendSpecification.isNotNullId());

			spec = (filter.getId() == null) ? spec
					: spec.and(MemberAssociationAppendSpecification.equalId(filter.getId()));
			spec = (StringUtils.isEmpty(filter.getDescription())) ? spec
					: spec.and(MemberAssociationAppendSpecification.likeDescription(filter.getDescription()));

			// referentes a tabela relacionada
			spec = (filter.getMemberId() == null) ? spec
					: spec.and(MemberAssociationAppendSpecification.equalMember(filter.getMemberId()));

			return spec;

		} else {
			return null;
		}

	}

}
