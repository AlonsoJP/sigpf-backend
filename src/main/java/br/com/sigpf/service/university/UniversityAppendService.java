package br.com.sigpf.service.university;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import br.com.sigpf.converter.UniversityAppendConverter;
import br.com.sigpf.entity.UniversityAppendEntity;
import br.com.sigpf.filter.UniversityAppendFilter;
import br.com.sigpf.repository.UniversityAppendRepository;
import br.com.sigpf.resource.UniversityAppendResource;
import br.com.sigpf.service.user.UserService;
import br.com.sigpf.specification.UniversityAppendSpecification;
import br.com.sigpf.util.Appends;
import br.com.sigpf.util.GetInfoToken;
import br.com.sigpf.util.ValidationAppends;

@Service
public class UniversityAppendService {

	@Value("${path.appends.root}")
	private String root;

	@Value("${path.appends.university-append}")
	private String path;

	ValidationAppends validation = new ValidationAppends();

	@Autowired
	private UniversityAppendRepository repository;

	@Autowired
	UserService service;

	GetInfoToken getInfoToken = new GetInfoToken();

	public static final Logger LOG = LogManager.getLogger(UniversityAppendEntity.class);

	public Iterable<UniversityAppendEntity> findAll() throws Exception {

		try {
			LOG.info("UniversityAppend Controller END : findAll");

			return repository.findAll();
		} catch (Exception e) {
			e.getCause().getMessage();

			LOG.error("UniversityAppend ERROR : findAll");

			return null;
		}

	}

	public Optional<UniversityAppendResource> findOne(Long id) {
		return UniversityAppendConverter.toOptionalDto(repository.findById(id));
	}

	public Page<UniversityAppendResource> findByFilter(UniversityAppendFilter filter, Pageable pageable) {
		return repository.findAll(getSpecification(filter), pageable)
				.map(UniversityAppendEntity -> UniversityAppendConverter.toDto(UniversityAppendEntity));
	}

	public UniversityAppendResource save(UniversityAppendResource resource, OAuth2Authentication auth)
			throws Exception {

		if (auth != null) {

			String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

			resource.setCreateUser(user);
			LOG.info("UniversityAppend END : save");
		} else {
			throw new Exception("Usuario ausente ou não capturado.");
		}

		return UniversityAppendConverter.toDto(repository.save(UniversityAppendConverter.toEntity(resource)));

	}

	public String saveDocument(MultipartFile document) throws Exception {

		try {

			validation.validationDocuments(document.getOriginalFilename());

			LOG.info("UniversityAppend Controller END : uploadDocument");
			return Appends.saveDocuments(document, root, path);

		} catch (NullPointerException e) {
			LOG.error("UniversityAppend Controller NullPointerException ERROR : uploadDocument");
			throw new NullPointerException(e.getMessage());
		} catch (RuntimeException e) {
			LOG.error("UniversityAppend Controller RuntimeException ERROR : uploadDocument");
			throw new RuntimeException(e.getMessage());
		}

	}

	public UniversityAppendResource update(UniversityAppendResource resource, Long id, OAuth2Authentication auth) {

		try {
			findOne(id);

			if (auth != null) {

				String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

				resource.setUpdateUser(user);
			}

			LOG.info("UniversityAppend END : update");

		} catch (Exception e) {
			e.getCause().getMessage();
			LOG.error("UniversityAppend ERROR : update");
		}

		resource.setId(id);

		return UniversityAppendConverter.toDto(repository.save(UniversityAppendConverter.toEntity(resource)));

	}

	public void delete(Long id) {
		try {
			UniversityAppendResource resource = findOne(id).get();

			deleteDocument(resource.getPath());

			repository.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("UniversityAppend ERROR : delete");
		}
	}

	public void deleteDocument(String caminho) throws Exception {

		try {

			LOG.info("UniversityAppend Controller START : deleteDocument");
			Appends.deleteDocuments(root, caminho);
			LOG.info("UniversityAppend Controller END : deleteDocument");
		} catch (NullPointerException e) {
			LOG.error("UniversityAppend Controller NullPointerException ERROR : deleteDocument");
			throw new NullPointerException(e.getMessage());
		} catch (RuntimeException e) {
			LOG.error("UniversityAppend Controller RuntimeException ERROR : deleteDocument");
			throw new RuntimeException(e.getMessage());
		}

	}

	private Specification<UniversityAppendEntity> getSpecification(UniversityAppendFilter filter) {

		if (filter != null) {
			Specification<UniversityAppendEntity> spec = Specification
					.where((filter.getId() == null) ? null : UniversityAppendSpecification.isNotNullId());

			spec = (filter.getId() == null) ? spec : spec.and(UniversityAppendSpecification.equalId(filter.getId()));
			spec = (StringUtils.isEmpty(filter.getDescription())) ? spec
					: spec.and(UniversityAppendSpecification.likeDescription(filter.getDescription()));

			// referentes a tabela relacionada
			spec = (filter.getUniversityId() == null) ? spec
					: spec.and(UniversityAppendSpecification.equalUniversity(filter.getUniversityId()));

			return spec;

		} else {
			return null;
		}

	}

}
