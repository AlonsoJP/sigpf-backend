package br.com.sigpf.service.university;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

import br.com.sigpf.converter.UniversityConverter;
import br.com.sigpf.entity.UniversityEntity;
import br.com.sigpf.filter.UniversityFilter;
import br.com.sigpf.repository.UniversityRepository;
import br.com.sigpf.resource.UniversityResource;
import br.com.sigpf.service.user.UserService;
import br.com.sigpf.specification.UniversitySpecification;
import br.com.sigpf.util.GetInfoToken;

@Service
public class UniversityService {

	@Autowired
	private UniversityRepository repository;

	@Autowired
	UserService service;

	GetInfoToken getInfoToken = new GetInfoToken();

	public static final Logger LOG = LogManager.getLogger(UniversityEntity.class);

	public Iterable<UniversityEntity> findAll() throws Exception {

		try {
			LOG.info("University Controller END : findAll");

			return repository.findAll();
		} catch (Exception e) {
			e.getCause().getMessage();

			LOG.error("University Controller ERROR : findAll");

			return null;
		}

	}

	public Optional<UniversityResource> findOne(Long id) {
		return UniversityConverter.toOptionalDto(repository.findById(id));
	}

	public Page<UniversityResource> findByFilter(UniversityFilter filter, Pageable pageable) {
		return repository.findAll(getSpecification(filter), pageable)
				.map(UniversityEntity -> UniversityConverter.toDto(UniversityEntity));
	}

	public List<UniversityResource> findByFilter(UniversityFilter filter) {
		LOG.info("UniversityController END : findByFilter");
		return UniversityConverter.listToDto(repository.findAll(getSpecification(filter)));
	}

	public UniversityResource save(UniversityResource resource, OAuth2Authentication auth) throws Exception {

		if (auth != null) {

			String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

			resource.setCreateUser(user);
			LOG.info("University Controller END : save");
		} else {
			throw new Exception("Usuario ausente ou não capturado.");
		}

		return UniversityConverter.toDto(repository.save(UniversityConverter.toEntity(resource)));

	}

	public UniversityResource update(UniversityResource resource, Long id, OAuth2Authentication auth) {

		try {
			findOne(id);

			if (auth != null) {

				String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

				resource.setUpdateUser(user);
			}

			LOG.info("University Controller END : update");

		} catch (Exception e) {
			e.getCause().getMessage();
			LOG.error("University Controller ERROR : update");
		}

		resource.setId(id);

		return UniversityConverter.toDto(repository.save(UniversityConverter.toEntity(resource)));

	}

	public void delete(Long id) {
		try {
			findOne(id);
			repository.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("University Controller ERROR : delete");
		}
	}

	private Specification<UniversityEntity> getSpecification(UniversityFilter filter) {

		if (filter != null) {
			Specification<UniversityEntity> spec = Specification
					.where((filter.getId() == null) ? null : UniversitySpecification.isNotNullId());

			spec = (filter.getId() == null) ? spec : spec.and(UniversitySpecification.equalId(filter.getId()));
			spec = (StringUtils.isEmpty(filter.getName())) ? spec
					: spec.and(UniversitySpecification.likeName(filter.getName()));
			spec = (StringUtils.isEmpty(filter.getCnpj())) ? spec
					: spec.and(UniversitySpecification.likeCnpj(filter.getCnpj()));

			return spec;

		} else {
			return null;
		}

	}

}
