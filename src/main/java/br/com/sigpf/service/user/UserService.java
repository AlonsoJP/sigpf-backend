package br.com.sigpf.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sigpf.converter.UserConverter;
import br.com.sigpf.repository.UserRepository;
import br.com.sigpf.resource.UserResource;

@Service
public class UserService {

	@Autowired
	UserRepository repository;

	public UserResource findByEmail(String email) {

		UserResource find = UserConverter.toDto(repository.findByEmail(email));

		return find;
	}

}
