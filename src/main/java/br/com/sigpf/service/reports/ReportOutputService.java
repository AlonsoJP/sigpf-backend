package br.com.sigpf.service.reports;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sigpf.filter.OutputFilter;
import br.com.sigpf.resource.OutputResource;
import br.com.sigpf.service.finances.OutputService;
import br.com.sigpf.util.ReportGenerator;

@Service
public class ReportOutputService {

	@Autowired
	OutputService service;

	ReportGenerator reportGenerator = new ReportGenerator();

	public byte[] reportOutput(OutputFilter filter) throws Exception {

		String path = "src/main/resources/reports/rol_output.jasper";

		List<OutputResource> list = service.findByFilter(filter);

		Map<String, Object> parameters = reportGenerator.parameters();
		parameters.put("REPORT_NAME", "Relatório Cadastral de Saídas");

		return reportGenerator.report(path, parameters, list);

	}

}
