package br.com.sigpf.service.reports;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sigpf.filter.RecipeFilter;
import br.com.sigpf.resource.RecipeResource;
import br.com.sigpf.service.finances.RecipeService;
import br.com.sigpf.util.ReportGenerator;

@Service
public class ReportRecipeService {

	@Autowired
	RecipeService service;

	ReportGenerator reportGenerator = new ReportGenerator();

	public byte[] reportRecipe(RecipeFilter filter) throws Exception {

		String path = "src/main/resources/reports/rol_recipe.jasper";

		List<RecipeResource> list = service.findByFilter(filter);

		Map<String, Object> parameters = reportGenerator.parameters();
		parameters.put("REPORT_NAME", "Relatório Cadastral de Receitas");

		return reportGenerator.report(path, parameters, list);

	}

}
