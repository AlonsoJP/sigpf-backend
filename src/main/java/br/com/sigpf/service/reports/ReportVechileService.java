package br.com.sigpf.service.reports;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sigpf.filter.VechileFilter;
import br.com.sigpf.resource.VechileResource;
import br.com.sigpf.service.vechile.VechileService;
import br.com.sigpf.util.ReportGenerator;

@Service
public class ReportVechileService {

	@Autowired
	VechileService service;

	ReportGenerator reportGenerator = new ReportGenerator();

	public byte[] reportVechile(VechileFilter filter) throws Exception {

		String path = "src/main/resources/reports/rol_vechile.jasper";

		List<VechileResource> list = service.findByFilter(filter);

		Map<String, Object> parameters = reportGenerator.parameters();
		parameters.put("REPORT_NAME", "Relatório Cadastral de Veículos");

		return reportGenerator.report(path, parameters, list);

	}
}
