package br.com.sigpf.service.reports;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sigpf.filter.DistrictFilter;
import br.com.sigpf.resource.DistrictResource;
import br.com.sigpf.service.address.DistrictService;
import br.com.sigpf.util.ReportGenerator;

@Service
public class ReportDistrictService {

	@Autowired
	DistrictService service;

	ReportGenerator reportGenerator = new ReportGenerator();

	public byte[] reportStudent(DistrictFilter filter) throws Exception {

		String path = "src/main/resources/reports/rol_district.jasper";

		List<DistrictResource> list = service.findByFilter(filter);

		Map<String, Object> parameters = reportGenerator.parameters();
		parameters.put("REPORT_NAME", "Relatório Cadastral de Distritos");

		return reportGenerator.report(path, parameters, list);

	}

}
