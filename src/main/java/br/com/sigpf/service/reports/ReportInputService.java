package br.com.sigpf.service.reports;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sigpf.filter.InputFilter;
import br.com.sigpf.resource.InputResource;
import br.com.sigpf.service.finances.InputService;
import br.com.sigpf.util.ReportGenerator;

@Service
public class ReportInputService {

	@Autowired
	InputService service;

	ReportGenerator reportGenerator = new ReportGenerator();

	public byte[] reportInput(InputFilter filter) throws Exception {

		String path = "src/main/resources/reports/rol_input.jasper";

		List<InputResource> list = service.findByFilter(filter);

		Map<String, Object> parameters = reportGenerator.parameters();
		parameters.put("REPORT_NAME", "Relatório Cadastral de Entradas");

		return reportGenerator.report(path, parameters, list);

	}

}
