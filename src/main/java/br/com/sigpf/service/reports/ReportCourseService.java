package br.com.sigpf.service.reports;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sigpf.filter.CourseFilter;
import br.com.sigpf.resource.CourseResource;
import br.com.sigpf.service.course.CourseService;
import br.com.sigpf.util.ReportGenerator;

@Service
public class ReportCourseService {

	@Autowired
	CourseService service;

	ReportGenerator reportGenerator = new ReportGenerator();

	public byte[] reportCourse(CourseFilter filter) throws Exception {

		String path = "src/main/resources/reports/rol_course.jasper";

		List<CourseResource> list = service.findByFilter(filter);

		Map<String, Object> parameters = reportGenerator.parameters();
		parameters.put("REPORT_NAME", "Relatório Cadastral de Cursos");

		return reportGenerator.report(path, parameters, list);

	}

}
