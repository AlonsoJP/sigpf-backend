package br.com.sigpf.service.reports;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sigpf.filter.MemberAssociationFilter;
import br.com.sigpf.resource.MemberAssociationResource;
import br.com.sigpf.service.member.MemberAssociationService;
import br.com.sigpf.util.ReportGenerator;

@Service
public class ReportMemberService {

	@Autowired
	MemberAssociationService service;

	ReportGenerator reportGenerator = new ReportGenerator();

	public byte[] reportDriver(MemberAssociationFilter filter) throws Exception {

		String path = "src/main/resources/reports/rol_member.jasper";

		List<MemberAssociationResource> list = service.findByFilter(filter);

		Map<String, Object> parameters = reportGenerator.parameters();
		parameters.put("REPORT_NAME", "Relatório Cadastral de Membros");

		return reportGenerator.report(path, parameters, list);

	}

	public byte[] reportDriverDetail(MemberAssociationFilter filter) throws Exception {

		String path = "src/main/resources/reports/rol_member_detail.jasper";

		List<MemberAssociationResource> list = service.findByFilter(filter);

		Map<String, Object> parameters = reportGenerator.parameters();
		parameters.put("REPORT_NAME", "Relatório Detalhado de Membros");

		return reportGenerator.report(path, parameters, list);

	}

}
