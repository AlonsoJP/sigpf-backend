package br.com.sigpf.service.reports;

import java.util.List;
import java.util.Map;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sigpf.filter.StudentFilter;
import br.com.sigpf.resource.PreferencesResource;
import br.com.sigpf.resource.StudentResource;
import br.com.sigpf.service.preferences.PreferencesService;
import br.com.sigpf.service.studenty.StudentService;
import br.com.sigpf.util.ReportGenerator;

@Service
public class ReportsStudentService {

	@Autowired
	StudentService service;

	@Autowired
	PreferencesService servicePreferences;

	ReportGenerator reportGenerator = new ReportGenerator();

	public byte[] reportStudent(StudentFilter filter) throws Exception {

		String path = "src/main/resources/reports/rol_student.jasper";

		List<StudentResource> list = service.findByFilter(filter);

		Map<String, Object> parameters = reportGenerator.parameters();
		parameters.put("REPORT_NAME", "Relatório Cadastral de Estudantes");

		return reportGenerator.report(path, parameters, list);

	}

	public byte[] reportStudentDetail(StudentFilter filter) throws Exception {

		String path = "src/main/resources/reports/rol_student_detail.jasper";

		List<StudentResource> list = service.findByFilter(filter);

		Map<String, Object> parameters = reportGenerator.parameters();
		parameters.put("REPORT_NAME", "Relatório Cadastral de Estudantes");

		return reportGenerator.report(path, parameters, list);

	}

	public byte[] reportStudentContractOpen(StudentFilter filter) throws Exception {

		PreferencesResource preferences = new PreferencesResource();

		preferences = servicePreferences.findOne(1L).get();

		String path = "src/main/resources/reports/contrato_abertura.jasper";

		List<StudentResource> list = service.findByFilter(filter);

		Map<String, Object> parameters = reportGenerator.parameters();
		parameters.put("REPORT_NAME", "CONTRATO");
		parameters.put("PREFERENCES", preferences);

		return reportGenerator.report(path, parameters, list);

	}

	public byte[] reportStudentContractClosing(StudentFilter filter) throws Exception {

		PreferencesResource preferences = new PreferencesResource();

		preferences = servicePreferences.findOne(1L).get();

		String path = "src/main/resources/reports/contrato_encerramento.jasper";

		List<StudentResource> list = service.findByFilter(filter);

		Map<String, Object> parameters = reportGenerator.parameters();
		parameters.put("REPORT_NAME", "CONTRATO");
		parameters.put("PREFERENCES", preferences);
		parameters.put("MES_CANCELAMENTO", String.valueOf(LocalDate.now().getMonthOfYear()));
		parameters.put("ANO_CANCELAMENTO", String.valueOf(LocalDate.now().getYear()));

		return reportGenerator.report(path, parameters, list);

	}

}
