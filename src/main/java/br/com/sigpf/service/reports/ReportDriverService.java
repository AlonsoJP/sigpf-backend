package br.com.sigpf.service.reports;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sigpf.filter.DriverFilter;
import br.com.sigpf.resource.DriverResource;
import br.com.sigpf.service.driver.DriverService;
import br.com.sigpf.util.ReportGenerator;

@Service
public class ReportDriverService {

	@Autowired
	DriverService service;

	ReportGenerator reportGenerator = new ReportGenerator();

	public byte[] reportDriver(DriverFilter filter) throws Exception {

		String path = "src/main/resources/reports/rol_driver.jasper";

		List<DriverResource> list = service.findByFilter(filter);

		Map<String, Object> parameters = reportGenerator.parameters();
		parameters.put("REPORT_NAME", "Relatório Cadastral de Motoristas");

		return reportGenerator.report(path, parameters, list);

	}

	public byte[] reportDriverDetail(DriverFilter filter) throws Exception {

		String path = "src/main/resources/reports/rol_driver_detail.jasper";

		List<DriverResource> list = service.findByFilter(filter);

		Map<String, Object> parameters = reportGenerator.parameters();
		parameters.put("REPORT_NAME", "Relatório Cadastral de Motoristas");

		return reportGenerator.report(path, parameters, list);

	}

}
