package br.com.sigpf.service.reports;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sigpf.filter.StreetFilter;
import br.com.sigpf.resource.StreetResource;
import br.com.sigpf.service.address.StreetService;
import br.com.sigpf.util.ReportGenerator;

@Service
public class ReportStreetService {

	@Autowired
	StreetService service;

	ReportGenerator reportGenerator = new ReportGenerator();

	public byte[] reportStudent(StreetFilter filter) throws Exception {

		String path = "src/main/resources/reports/rol_street.jasper";

		List<StreetResource> list = service.findByFilter(filter);

		Map<String, Object> parameters = reportGenerator.parameters();
		parameters.put("REPORT_NAME", "Relatório Cadastral de Ruas");

		return reportGenerator.report(path, parameters, list);

	}

}
