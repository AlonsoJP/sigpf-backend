package br.com.sigpf.service.reports;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sigpf.filter.UniversityFilter;
import br.com.sigpf.resource.UniversityResource;
import br.com.sigpf.service.university.UniversityService;
import br.com.sigpf.util.ReportGenerator;

@Service
public class ReportUniversityService {

	@Autowired
	UniversityService service;

	ReportGenerator reportGenerator = new ReportGenerator();

	public byte[] reportUniversity(UniversityFilter filter) throws Exception {

		String path = "src/main/resources/reports/rol_university.jasper";

		List<UniversityResource> list = service.findByFilter(filter);

		Map<String, Object> parameters = reportGenerator.parameters();
		parameters.put("REPORT_NAME", "Relatório Cadastral de Universidades");

		return reportGenerator.report(path, parameters, list);

	}
	public byte[] reportUniversityDetail(UniversityFilter filter) throws Exception {
		
		String path = "src/main/resources/reports/rol_university_detail.jasper";
		
		List<UniversityResource> list = service.findByFilter(filter);
		
		Map<String, Object> parameters = reportGenerator.parameters();
		parameters.put("REPORT_NAME", "Relatório Cadastral de Universidades");
		
		return reportGenerator.report(path, parameters, list);
		
	}

}
