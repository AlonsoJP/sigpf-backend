package br.com.sigpf.service.reports;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sigpf.filter.CompanyFilter;
import br.com.sigpf.resource.CompanyResource;
import br.com.sigpf.service.company.CompanyService;
import br.com.sigpf.util.ReportGenerator;

@Service
public class ReportCompanyService {

	@Autowired
	CompanyService service;

	ReportGenerator reportGenerator = new ReportGenerator();

	public byte[] reportCompany(CompanyFilter filter) throws Exception {

		String path = "src/main/resources/reports/rol_company.jasper";

		List<CompanyResource> list = service.findByFilter(filter);

		Map<String, Object> parameters = reportGenerator.parameters();
		parameters.put("REPORT_NAME", "Relatório Cadastral de Empresas");

		return reportGenerator.report(path, parameters, list);

	}

	public byte[] reportCompanyDetail(CompanyFilter filter) throws Exception {

		String path = "src/main/resources/reports/rol_company_detail.jasper";

		List<CompanyResource> list = service.findByFilter(filter);

		Map<String, Object> parameters = reportGenerator.parameters();
		parameters.put("REPORT_NAME", "Relatório Detalhado de Empresas");

		return reportGenerator.report(path, parameters, list);

	}

}
