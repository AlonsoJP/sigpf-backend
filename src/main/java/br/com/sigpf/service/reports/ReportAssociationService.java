package br.com.sigpf.service.reports;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sigpf.filter.AssociationFilter;
import br.com.sigpf.resource.AssociationResource;
import br.com.sigpf.service.association.AssociationService;
import br.com.sigpf.util.ReportGenerator;

@Service
public class ReportAssociationService {

	@Autowired
	AssociationService service;

	ReportGenerator reportGenerator = new ReportGenerator();

	public byte[] reportAssociation(AssociationFilter filter) throws Exception {

		String path = "src/main/resources/reports/rol_association_basic.jasper";

		List<AssociationResource> list = service.findByFilter(filter);

		Map<String, Object> parameters = reportGenerator.parameters();
		parameters.put("REPORT_NAME", "Relatório Cadastral de Associações");

		return reportGenerator.report(path, parameters, list);

	}

}
