package br.com.sigpf.service.studenty;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import br.com.sigpf.converter.StudentAppendConverter;
import br.com.sigpf.entity.StudentAppendEntity;
import br.com.sigpf.filter.StudentAppendFilter;
import br.com.sigpf.repository.StudentAppendRepository;
import br.com.sigpf.resource.StudentAppendResource;
import br.com.sigpf.service.user.UserService;
import br.com.sigpf.specification.StudentAppendSpecification;
import br.com.sigpf.util.Appends;
import br.com.sigpf.util.GetInfoToken;
import br.com.sigpf.util.ValidationAppends;

@Service
public class StudentAppendService {

	@Value("${path.appends.root}")
	private String root;

	@Value("${path.appends.student-append}")
	private String path;

	ValidationAppends validation = new ValidationAppends();

	@Autowired
	private StudentAppendRepository repository;

	@Autowired
	UserService service;

	GetInfoToken getInfoToken = new GetInfoToken();

	public static final Logger LOG = LogManager.getLogger(StudentAppendEntity.class);

	public Iterable<StudentAppendEntity> findAll() throws Exception {

		try {
			LOG.info("StudentAppend Controller END : findAll");

			return repository.findAll();
		} catch (Exception e) {
			e.getCause().getMessage();

			LOG.error("StudentAppend ERROR : findAll");

			return null;
		}

	}

	public Optional<StudentAppendResource> findOne(Long id) {
		return StudentAppendConverter.toOptionalDto(repository.findById(id));
	}

	public Page<StudentAppendResource> findByFilter(StudentAppendFilter filter, Pageable pageable) {
		return repository.findAll(getSpecification(filter), pageable)
				.map(StudentAppendEntity -> StudentAppendConverter.toDto(StudentAppendEntity));
	}

	public StudentAppendResource save(StudentAppendResource resource, OAuth2Authentication auth) throws Exception {

		if (auth != null) {

			String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

			resource.setCreateUser(user);
		} else {
			throw new Exception("Usuario ausente ou não capturado.");
		}

		LOG.info("StudentAppend END : save");
		return StudentAppendConverter.toDto(repository.save(StudentAppendConverter.toEntity(resource)));

	}

	public String saveDocument(MultipartFile document) throws Exception {

		try {

			validation.validationDocuments(document.getOriginalFilename());

			LOG.info("StudentAppend Controller END : uploadDocument");
			return Appends.saveDocuments(document, root, path);

		} catch (NullPointerException e) {
			LOG.error("StudentAppend Controller NullPointerException ERROR : uploadDocument");
			throw new NullPointerException(e.getMessage());
		} catch (RuntimeException e) {
			LOG.error("StudentAppend Controller RuntimeException ERROR : uploadDocument");
			throw new RuntimeException(e.getMessage());
		}

	}

	public StudentAppendResource update(StudentAppendResource resource, Long id, OAuth2Authentication auth) {

		try {
			findOne(id);

			if (auth != null) {

				String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

				resource.setUpdateUser(user);
			}

			LOG.info("StudentAppend END : update");

		} catch (Exception e) {
			e.getCause().getMessage();
			LOG.error("StudentAppend ERROR : update");
		}

		resource.setId(id);

		return StudentAppendConverter.toDto(repository.save(StudentAppendConverter.toEntity(resource)));

	}

	public void delete(Long id) {
		try {
			findOne(id);
			repository.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("StudentAppend ERROR : delete");
		}
	}

	private Specification<StudentAppendEntity> getSpecification(StudentAppendFilter filter) {

		if (filter != null) {
			Specification<StudentAppendEntity> spec = Specification
					.where((filter.getId() == null) ? null : StudentAppendSpecification.isNotNullId());

			spec = (filter.getId() == null) ? spec : spec.and(StudentAppendSpecification.equalId(filter.getId()));
			spec = (StringUtils.isEmpty(filter.getDescription())) ? spec
					: spec.and(StudentAppendSpecification.likeDescription(filter.getDescription()));
			// referentes a tabela relacionada
			spec = (filter.getStudentId() == null) ? spec
					: spec.and(StudentAppendSpecification.equalAssociation(filter.getStudentId()));

			return spec;

		} else {
			return null;
		}

	}
}
