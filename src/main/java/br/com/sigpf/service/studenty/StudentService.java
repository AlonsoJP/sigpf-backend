package br.com.sigpf.service.studenty;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import br.com.sigpf.converter.StudentConverter;
import br.com.sigpf.entity.StudentEntity;
import br.com.sigpf.enums.PaymentTypeEnum;
import br.com.sigpf.filter.StudentFilter;
import br.com.sigpf.repository.StudentRepository;
import br.com.sigpf.resource.PreferencesResource;
import br.com.sigpf.resource.StudentResource;
import br.com.sigpf.service.preferences.PreferencesService;
import br.com.sigpf.service.user.UserService;
import br.com.sigpf.specification.StudentSpecification;
import br.com.sigpf.util.Appends;
import br.com.sigpf.util.GetInfoToken;
import br.com.sigpf.util.ValidationAppends;

@Service
public class StudentService {

	@Value("${path.appends.root}")
	private String root;

	@Value("${path.appends.student}")
	private String path;

	@Autowired
	private StudentRepository repository;

	@Autowired
	UserService service;

	@Autowired
	PreferencesService servicePreference;

	GetInfoToken getInfoToken = new GetInfoToken();

	ValidationAppends validation = new ValidationAppends();

	public static final Logger LOG = LogManager.getLogger(StudentEntity.class);

	// valor estimado total, considerando as configurações e a quantidade de
	// estudantes
	public Double calculation() throws Exception {

		LOG.info("StudentController START : calculation()");

		PreferencesResource preferenceResource = new PreferencesResource();

		preferenceResource = servicePreference.findOne(1L).get();

		if (preferenceResource.getCalculationType()
				.equalsIgnoreCase(PaymentTypeEnum.INTEGRAL.getDescricao().toString())) {

			LOG.info("StudentController END : calculation()");

			return (estimateIntegralWithAuxPerStudent() * countByActiveAndTypeIntegral());

		} else if (preferenceResource.getCalculationType()
				.equalsIgnoreCase(PaymentTypeEnum.PARCIAL.getDescricao().toString())) {

			Double value1 = (estimateIntegralWithAuxWithParcialPerStudent() * countByActiveAndTypeIntegral());
			Double value2 = (estimateParcialWithAuxPerStudent() * countByActiveAndTypeParcial());

			LOG.info("StudentController END : calculation()");
			System.out.println(" calulo total:  " + (value1 + value2));
			return (value1 + value2);
		} else {

			LOG.error("StudentController ERROR : calculation()");
			throw new Exception();
		}

	}

	// valor estimado para pagamentos integrais com contribuição de caixa, sem
	// considerar os pagamentos parciais
	public Double estimateIntegralWithAuxPerStudent() {

		LOG.info("StudentController START : estimateIntegralWithAuxPerStudent()");

		PreferencesResource preferenceResource = new PreferencesResource();

		preferenceResource = servicePreference.findOne(1L).get();

		Double result = ((preferenceResource.getAmmount() / countByActiveAndTypeIntegral())
				+ preferenceResource.getContributionBank());

		LOG.info("StudentController END : estimateIntegralWithAuxPerStudent()");

		System.out.println("Total com contribuição, estimado somente integral: " + result);
		return result;
	}

	// valor estimado para pagamento integrais sem contribuição de caixa, sem
	// considerar os pagamentos parciais
	public Double estimateIntegralWithOuthAuxPerStudent(Double value) {

		LOG.info("StudentController START : estimateIntegralWithOuthAuxPerStudent()");

		Double result = (value / countByActiveAndTypeIntegral());

		LOG.info("StudentController END : estimateIntegralWithOuthAuxPerStudent()");

		System.out.println("Total sem contribuição, estimado somente integral" + result);
		return result;
	}

	// valor estimado para pagamentos integrais, considerando os pagamentos parciais
	// com axílio de caixa
	public Double estimateIntegralWithAuxWithParcialPerStudent() throws Exception {

		LOG.info("StudentController START : estimateIntegralWithAuxWithParcialPerStudent()");

		PreferencesResource preferenceResource = new PreferencesResource();

		Double resultIntegralWithParcial;
		try {
			preferenceResource = servicePreference.findOne(1L).get();

			Long countParcial = countByActiveAndTypeParcial();

			Double amountParcial = (countParcial * 120.00);// TODO no lugar de 120 seria o valor da mensalidade fixa
															// para os parciais

			Double resultCalc = (preferenceResource.getAmmount() - amountParcial);

			resultIntegralWithParcial = ((estimateIntegralWithOuthAuxPerStudent(resultCalc))
					+ preferenceResource.getContributionBank());

			LOG.info("StudentController END : estimateIntegralWithAuxWithParcialPerStudent()");

			System.out.println("Total com contribuição, incluindo parciais: " + resultIntegralWithParcial);
			return resultIntegralWithParcial;

		} catch (Exception e) {
			throw new Exception(e);
		}

	}

	// valor estimado para pagamento parciais, com contribuição de caixa
	public Double estimateParcialWithAuxPerStudent() {

		LOG.info("StudentController START : estimateParcialWithAuxPerStudent()");

		PreferencesResource preferenceResource = new PreferencesResource();

		preferenceResource = servicePreference.findOne(1L).get();

		Double parcial = 120.00;// TODO no lugar de 120 seria o valor da mensalidade fixa para os parciais

		parcial = (parcial + preferenceResource.getContributionBank());

		LOG.info("StudentController END : estimateParcialWithAuxPerStudent()");

		System.out.println("Total parcial com contribuição de caixa: " + parcial);
		return parcial;

	}

	// contador de estudantes com pagamento integral e ativos
	public Long countByActiveAndTypeIntegral() {

		LOG.info("StudentController START : countByActiveAndTypeIntegral()");

		Long count = repository.countByActiveAndPaymentType(true, PaymentTypeEnum.INTEGRAL);

		LOG.info("StudentController END : countByActiveAndTypeIntegral()");

		System.out.println("Quantidade de estudantes pagantes integrais ativos: " + count);
		return count;
	}

	// contador de estudantes com pagamento parcial e ativos
	public Long countByActiveAndTypeParcial() {
		LOG.info("StudentController START : countByActiveAndTypeParcial()");

		Long count;
		count = repository.countByActiveAndPaymentType(true, PaymentTypeEnum.PARCIAL);

		LOG.info("StudentController END : countByActiveAndTypeParcial()");

		System.out.println("Quantidade de estudantes pagantes parciais ativos: " + count);

		return count;
	}

	// contador de estudantes ativos
	public Long countByActive() {

		Long count = repository.countByActive(true);

		return count;
	}

	// contador de estudantes inativos
	public Long countByInactive() {

		return Math.abs((countByActive() - countTotal()));

	}

	// contador de estudantes, incluindo ativos e inativos
	public Long countTotal() {

		return repository.count();

	}

	// valor estimado para o total arrecadao para o caixa, por mês. Baseado nas
	// configurações do sistema
	public Double estimateContribution() {

		PreferencesResource preferenceResource = new PreferencesResource();

		preferenceResource = servicePreference.findOne(1L).get();

		Double result = (preferenceResource.getContributionBank() * countByActive());

		return result;
	}

	// valor estimado do lucro, considerando a quantidade de alunos e o valor pago
	// pelo serviço de transporte
	public Double estimateLucre() {

		PreferencesResource preferenceResource = new PreferencesResource();

		preferenceResource = servicePreference.findOne(1L).get();

		Double withoutC = preferenceResource.getAmmount();
		Double withC = preferenceResource.getAmmount() + estimateContribution();

		Double result = (withoutC - withC);

		return result;
	}

	public Double mensalityBase() {
		PreferencesResource preferenceResource = new PreferencesResource();

		preferenceResource = servicePreference.findOne(1L).get();

		return preferenceResource.getAmmount();
	}

	public List<StudentResource> findAll() throws Exception {

		try {
			LOG.info("StudentController END : findAll");

			System.out.println(
					"Quantidade de estudantes pagantes ativos: " + StudentConverter.listToDto(repository.findAll()));
			return StudentConverter.listToDto(repository.findAll());
		} catch (Exception e) {

			LOG.error("StudentController ERROR : findAll");

			throw new Exception(e.getCause().getMessage());

		}

	}

	public Optional<StudentResource> findOne(Long id) {
		LOG.info("StudentController END : findOne");

		return StudentConverter.toOptionalDto(repository.findById(id));
	}

	public Page<StudentResource> findByFilter(StudentFilter filter, Pageable pageable) {
		LOG.info("StudentController END : findByFilter");

		return repository.findAll(getSpecification(filter), pageable)
				.map(StudentEntity -> StudentConverter.toDto(StudentEntity));
	}

	public List<StudentResource> findByFilter(StudentFilter filter) {
		LOG.info("StudentController END : findByFilter");

		return StudentConverter.listToDto(repository.findAll(getSpecification(filter)));
	}

	public StudentResource save(StudentResource resource, OAuth2Authentication auth) throws Exception {

		if (auth != null) {

			String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

			resource.setCreateUser(user);
			LOG.info("StudentController END : save");
		} else {
			throw new Exception("Usuario ausente ou não capturado.");
		}

		return StudentConverter.toDto(repository.save(StudentConverter.toEntity(resource)));

	}

	public String savePhotoStudent(MultipartFile photo) throws Exception {

		try {

			validation.validationImage(photo.getOriginalFilename());

			LOG.info("StudentController END : savePhoto");
			return Appends.saveDocuments(photo, root, path);

		} catch (NullPointerException e) {
			LOG.error("StudentController NullPointerException ERROR : savePhoto");
			throw new NullPointerException(e.getMessage());
		} catch (RuntimeException e) {
			LOG.error("StudentController RuntimeException ERROR : savePhoto");
			throw new RuntimeException(e.getMessage());
		}

	}

	public StudentResource update(StudentResource resource, Long id, OAuth2Authentication auth) {

		try {
			findOne(id);

			if (auth != null) {

				String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

				resource.setUpdateUser(user);
			}

			LOG.info("StudentController END : update");

		} catch (Exception e) {
			e.getCause().getMessage();
			LOG.error("StudentController ERROR : update");
		}

		resource.setId(id);

		LOG.info("StudentController END : update");

		return StudentConverter.toDto(repository.save(StudentConverter.toEntity(resource)));

	}

	public void delete(Long id) {
		try {
			findOne(id);
			repository.deleteById(id);
			LOG.info("StudentController END : delete");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("StudentController ERROR : delete");
		}
	}

	private Specification<StudentEntity> getSpecification(StudentFilter filter) {

		if (filter != null) {
			Specification<StudentEntity> spec = Specification
					.where((filter.getId() == null) ? null : StudentSpecification.isNotNullId());

			spec = (filter.getId() == null) ? spec : spec.and(StudentSpecification.equalId(filter.getId()));

			spec = (StringUtils.isEmpty(filter.getName())) ? spec
					: spec.and(StudentSpecification.likeName(filter.getName()));
			spec = (StringUtils.isEmpty(filter.getCpf())) ? spec
					: spec.and(StudentSpecification.likeCpf(filter.getCpf()));
			spec = (StringUtils.isEmpty(filter.getRg())) ? spec : spec.and(StudentSpecification.likeRg(filter.getRg()));
			spec = (StringUtils.isEmpty(filter.getRa())) ? spec : spec.and(StudentSpecification.likeRa(filter.getRa()));
			spec = (StringUtils.isEmpty(filter.getEmail())) ? spec
					: spec.and(StudentSpecification.likeEmail(filter.getEmail()));

			// referentes a tabela relacionada
			spec = (filter.getCourseId() == null) ? spec
					: spec.and(StudentSpecification.equalCourse(filter.getCourseId()));
			spec = (StringUtils.isEmpty(filter.getNameCourse())) ? spec
					: spec.and(StudentSpecification.likeNameCourse(filter.getNameCourse()));

			return spec;

		} else {
			return null;
		}

	}

}
