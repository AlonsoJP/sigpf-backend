package br.com.sigpf.service.association;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

import br.com.sigpf.converter.AssociationConverter;
import br.com.sigpf.entity.AssociationEntity;
import br.com.sigpf.filter.AssociationFilter;
import br.com.sigpf.repository.AssociationRepository;
import br.com.sigpf.resource.AssociationResource;
import br.com.sigpf.service.user.UserService;
import br.com.sigpf.specification.AssociationSpecification;
import br.com.sigpf.util.GetInfoToken;

@Service
public class AssociationService {

	@Autowired
	private AssociationRepository repository;

	@Autowired
	UserService service;

	GetInfoToken getInfoToken = new GetInfoToken();

	public static final Logger LOG = LogManager.getLogger(AssociationEntity.class);

	public Iterable<AssociationEntity> findAll() throws Exception {

		try {
			LOG.info("AssociationController END : findAll");

			return repository.findAll();
		} catch (Exception e) {
			e.getCause().getMessage();

			LOG.error("AssociationController ERROR : findAll");

			return null;
		}

	}

	public Optional<AssociationResource> findOne(Long id) {
		return AssociationConverter.toOptionalDto(repository.findById(id));
	}

	public Page<AssociationResource> findByFilter(AssociationFilter filter, Pageable pageable) {
		LOG.info("AssociationController END : findByFilter");
		return repository.findAll(getSpecification(filter), pageable)
				.map(AssociationEntity -> AssociationConverter.toDto(AssociationEntity));
	}

	public List<AssociationResource> findByFilter(AssociationFilter filter) {
		LOG.info("AssociationController END : findByFilter");

		return AssociationConverter.listToDto(repository.findAll(getSpecification(filter)));
	}

	public AssociationResource save(AssociationResource resource, OAuth2Authentication auth) throws Exception {

		if (auth != null) {

			String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

			resource.setCreateUser(user);
			LOG.info("AssociationController END : save");
		} else {
			throw new Exception("Usuario ausente ou não capturado.");
		}

		return AssociationConverter.toDto(repository.save(AssociationConverter.toEntity(resource)));

	}

	public AssociationResource update(AssociationResource resource, Long id, OAuth2Authentication auth) {

		try {
			findOne(id);

			if (auth != null) {

				String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

				resource.setUpdateUser(user);
			}

			LOG.info("AssociationController END : update");

		} catch (Exception e) {
			e.getCause().getMessage();
			LOG.error("AssociationController ERROR : update");
		}

		resource.setId(id);

		return AssociationConverter.toDto(repository.save(AssociationConverter.toEntity(resource)));

	}

	public void delete(Long id) {
		try {
			findOne(id);
			repository.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("AssociationController ERROR : delete");
		}
	}

	private Specification<AssociationEntity> getSpecification(AssociationFilter filter) {

		if (filter != null) {
			Specification<AssociationEntity> spec = Specification
					.where((filter.getId() == null) ? null : AssociationSpecification.isNotNullId());

			spec = (filter.getId() == null) ? spec : spec.and(AssociationSpecification.equalId(filter.getId()));
			spec = (StringUtils.isEmpty(filter.getOwner())) ? spec
					: spec.and(AssociationSpecification.likeOwner(filter.getOwner()));
			spec = (StringUtils.isEmpty(filter.getFantasyName())) ? spec
					: spec.and(AssociationSpecification.likeFantasyName(filter.getFantasyName()));
			spec = (StringUtils.isEmpty(filter.getCnpj())) ? spec
					: spec.and(AssociationSpecification.likeCnpj(filter.getCnpj()));

			return spec;

		} else {
			return null;
		}

	}

}
