package br.com.sigpf.service.association;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import br.com.sigpf.converter.AssociationAppendConverter;
import br.com.sigpf.entity.AssociationAppendEntity;
import br.com.sigpf.filter.AssociationAppendFilter;
import br.com.sigpf.repository.AssociationAppendRepository;
import br.com.sigpf.resource.AssociationAppendResource;
import br.com.sigpf.service.user.UserService;
import br.com.sigpf.specification.AssociationAppendSpecification;
import br.com.sigpf.util.Appends;
import br.com.sigpf.util.GetInfoToken;
import br.com.sigpf.util.ValidationAppends;

@Service
public class AssociationAppendService {

	@Value("${path.appends.root}")
	private String root;

	@Value("${path.appends.association-append}")
	private String path;

	@Autowired
	UserService service;

	GetInfoToken getInfoToken = new GetInfoToken();

	ValidationAppends validation = new ValidationAppends();

	@Autowired
	private AssociationAppendRepository repository;

	public static final Logger LOG = LogManager.getLogger(AssociationAppendEntity.class);

	public Iterable<AssociationAppendEntity> findAll() throws Exception {

		try {
			LOG.info("AssociationAppend Controller END : findAll");

			return repository.findAll();
		} catch (Exception e) {
			e.getCause().getMessage();

			LOG.error("AssociationAppend Controller ERROR : findAll");

			return null;
		}

	}

	public Optional<AssociationAppendResource> findOne(Long id) {
		return AssociationAppendConverter.toOptionalDto(repository.findById(id));
	}

	public Page<AssociationAppendResource> findByFilter(AssociationAppendFilter filter, Pageable pageable) {
		return repository.findAll(getSpecification(filter), pageable)
				.map(AssociationAppendEntity -> AssociationAppendConverter.toDto(AssociationAppendEntity));
	}

	public AssociationAppendResource save(AssociationAppendResource resource, OAuth2Authentication auth)
			throws Exception {

		if (auth != null) {

			String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

			resource.setCreateUser(user);
			LOG.info("AssociationAppend END : save");
		} else {
			throw new Exception("Usuario ausente ou não capturado.");
		}

		return AssociationAppendConverter.toDto(repository.save(AssociationAppendConverter.toEntity(resource)));

	}

	public String saveDocument(MultipartFile document) throws Exception {

		try {

			validation.validationDocuments(document.getOriginalFilename());

			LOG.info("AssociationAppend Controller END : saveDocument");
			return Appends.saveDocuments(document, root, path);

		} catch (NullPointerException e) {
			LOG.error("AssociationAppend Controller NullPointerException ERROR : saveDocument");
			throw new NullPointerException(e.getMessage());
		} catch (RuntimeException e) {
			LOG.error("AssociationAppend Controller RuntimeException ERROR : saveDocument");
			throw new RuntimeException(e.getMessage());
		}

	}

	public void deleteDocument(String caminho) throws Exception {

		try {

			LOG.info("AssociationAppend Controller START : deleteDocument");
			Appends.deleteDocuments(root, caminho);
			LOG.info("AssociationAppend Controller END : deleteDocument");
		} catch (NullPointerException e) {
			LOG.error("AssociationAppend Controller NullPointerException ERROR : deleteDocument");
			throw new NullPointerException(e.getMessage());
		} catch (RuntimeException e) {
			LOG.error("AssociationAppend Controller RuntimeException ERROR : deleteDocument");
			throw new RuntimeException(e.getMessage());
		}

	}

	public AssociationAppendResource update(AssociationAppendResource resource, Long id, OAuth2Authentication auth) {

		try {
			findOne(id);

			if (auth != null) {

				String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

				resource.setUpdateUser(user);
			}

			LOG.info("AssociationAppend END : update");

		} catch (Exception e) {
			e.getCause().getMessage();
			LOG.error("AssociationAppend Controller ERROR : update");
		}

		resource.setId(id);

		return AssociationAppendConverter.toDto(repository.save(AssociationAppendConverter.toEntity(resource)));

	}

	public void delete(Long id) {
		try {
			AssociationAppendResource resource = findOne(id).get();

			deleteDocument(resource.getPath());

			repository.deleteById(id);
			LOG.info("AssociationAppend Controller ERROR : delete");

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("AssociationAppend Controller ERROR : delete");
		}
	}

	private Specification<AssociationAppendEntity> getSpecification(AssociationAppendFilter filter) {

		if (filter != null) {
			Specification<AssociationAppendEntity> spec = Specification
					.where((filter.getId() == null) ? null : AssociationAppendSpecification.isNotNullId());

			spec = (filter.getId() == null) ? spec : spec.and(AssociationAppendSpecification.equalId(filter.getId()));
			spec = (StringUtils.isEmpty(filter.getDescription())) ? spec
					: spec.and(AssociationAppendSpecification.likeDescription(filter.getDescription()));

			// referentes a tabela relacionada
			spec = (filter.getAssociationId() == null) ? spec
					: spec.and(AssociationAppendSpecification.equalAssociation(filter.getAssociationId()));

			return spec;

		} else {
			return null;
		}

	}

}
