package br.com.sigpf.service.driver;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import br.com.sigpf.converter.DriverConverter;
import br.com.sigpf.entity.DriverEntity;
import br.com.sigpf.filter.DriverFilter;
import br.com.sigpf.repository.DriverRepository;
import br.com.sigpf.resource.DriverResource;
import br.com.sigpf.service.user.UserService;
import br.com.sigpf.specification.DriverSpecification;
import br.com.sigpf.util.Appends;
import br.com.sigpf.util.GetInfoToken;
import br.com.sigpf.util.ValidationAppends;

@Service
public class DriverService {

	@Autowired
	private DriverRepository repository;

	@Autowired
	UserService service;

	GetInfoToken getInfoToken = new GetInfoToken();

	public static final Logger LOG = LogManager.getLogger(DriverEntity.class);

	ValidationAppends validation = new ValidationAppends();

	@Value("${path.appends.root}")
	private String root;

	@Value("${path.appends.driver}")
	private String path;

	public Iterable<DriverEntity> findAll() throws Exception {

		try {
			LOG.info("DriverController END : findAll");

			return repository.findAll();
		} catch (Exception e) {
			e.getCause().getMessage();

			LOG.error("DriverController ERROR : findAll");

			return null;
		}

	}

	public Optional<DriverResource> findOne(Long id) {
		return DriverConverter.toOptionalDto(repository.findById(id));
	}

	public Page<DriverResource> findByFilter(DriverFilter filter, Pageable pageable) {
		return repository.findAll(getSpecification(filter), pageable)
				.map(DriverEntity -> DriverConverter.toDto(DriverEntity));
	}

	public List<DriverResource> findByFilter(DriverFilter filter) {
		LOG.info("DriverController END : findByFilter");
		return DriverConverter.listToDto(repository.findAll(getSpecification(filter)));
	}

	public DriverResource save(DriverResource resource, OAuth2Authentication auth) throws Exception {

		if (auth != null) {

			String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

			resource.setCreateUser(user);
			LOG.info("DriverController END : save");
		} else {
			throw new Exception("Usuario ausente ou não capturado.");
		}

		return DriverConverter.toDto(repository.save(DriverConverter.toEntity(resource)));

	}

	public String savePhoto(MultipartFile photo) throws Exception {

		try {

			validation.validationImage(photo.getOriginalFilename());

			LOG.info("DriverController END : savePhoto");
			return Appends.saveDocuments(photo, root, path);

		} catch (NullPointerException e) {
			LOG.error("DriverController NullPointerException ERROR : savePhoto");
			throw new NullPointerException(e.getMessage());
		} catch (RuntimeException e) {
			LOG.error("DriverController RuntimeException ERROR : savePhoto");
			throw new RuntimeException(e.getMessage());
		}

	}

	public DriverResource update(DriverResource resource, Long id, OAuth2Authentication auth) {

		try {
			findOne(id);

			if (auth != null) {

				String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

				resource.setUpdateUser(user);
			}

			LOG.info("StudentController END : update");

		} catch (Exception e) {
			e.getCause().getMessage();
			LOG.error("DriverController ERROR : update");
		}

		resource.setId(id);

		return DriverConverter.toDto(repository.save(DriverConverter.toEntity(resource)));

	}

	public void delete(Long id) {
		try {
			findOne(id);
			repository.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("DriverController ERROR : delete");
		}
	}

	private Specification<DriverEntity> getSpecification(DriverFilter filter) {

		if (filter != null) {
			Specification<DriverEntity> spec = Specification
					.where((filter.getId() == null) ? null : DriverSpecification.isNotNullId());

			spec = (filter.getId() == null) ? spec : spec.and(DriverSpecification.equalId(filter.getId()));
			spec = (StringUtils.isEmpty(filter.getName())) ? spec
					: spec.and(DriverSpecification.likeName(filter.getName()));
			spec = (StringUtils.isEmpty(filter.getCpf())) ? spec
					: spec.and(DriverSpecification.likeCpf(filter.getName()));
			spec = (StringUtils.isEmpty(filter.getRg())) ? spec : spec.and(DriverSpecification.likeRg(filter.getRg()));
			spec = (StringUtils.isEmpty(filter.getLicense())) ? spec
					: spec.and(DriverSpecification.likeLicense(filter.getLicense()));

			return spec;

		} else {
			return null;
		}

	}

}
