package br.com.sigpf.service.driver;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import br.com.sigpf.converter.DriverAppendConverter;
import br.com.sigpf.entity.DriverAppendEntity;
import br.com.sigpf.filter.DriverAppendFilter;
import br.com.sigpf.repository.DriverAppendRepository;
import br.com.sigpf.resource.DriverAppendResource;
import br.com.sigpf.service.user.UserService;
import br.com.sigpf.specification.DriverAppendSpecification;
import br.com.sigpf.util.Appends;
import br.com.sigpf.util.GetInfoToken;
import br.com.sigpf.util.ValidationAppends;

@Service
public class DriverAppendService {

	@Value("${path.appends.root}")
	private String root;

	@Value("${path.appends.driver-append}")
	private String path;

	ValidationAppends validation = new ValidationAppends();

	@Autowired
	private DriverAppendRepository repository;

	@Autowired
	UserService service;

	GetInfoToken getInfoToken = new GetInfoToken();

	public static final Logger LOG = LogManager.getLogger(DriverAppendEntity.class);

	public Iterable<DriverAppendEntity> findAll() throws Exception {

		try {
			LOG.info("DriverAppend Controller END : findAll");

			return repository.findAll();
		} catch (Exception e) {
			e.getCause().getMessage();

			LOG.error("DriverAppend ERROR : findAll");

			return null;
		}

	}

	public Optional<DriverAppendResource> findOne(Long id) {
		return DriverAppendConverter.toOptionalDto(repository.findById(id));
	}

	public Page<DriverAppendResource> findByFilter(DriverAppendFilter filter, Pageable pageable) {
		return repository.findAll(getSpecification(filter), pageable)
				.map(DriverAppendEntity -> DriverAppendConverter.toDto(DriverAppendEntity));
	}

	public DriverAppendResource save(DriverAppendResource resource, OAuth2Authentication auth) throws Exception {

		if (auth != null) {

			String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

			resource.setCreateUser(user);
			LOG.info("DriverAppend END : save");
		} else {
			throw new Exception("Usuario ausente ou não capturado.");
		}

		return DriverAppendConverter.toDto(repository.save(DriverAppendConverter.toEntity(resource)));

	}

	public String saveDocument(MultipartFile document) throws Exception {

		try {

			validation.validationDocuments(document.getOriginalFilename());

			LOG.info("DriverAppend Controller END : uploadDocument");
			return Appends.saveDocuments(document, root, path);

		} catch (NullPointerException e) {
			LOG.error("DriverAppend Controller NullPointerException ERROR : uploadDocument");
			throw new NullPointerException(e.getMessage());
		} catch (RuntimeException e) {
			LOG.error("DriverAppend Controller RuntimeException ERROR : uploadDocument");
			throw new RuntimeException(e.getMessage());
		}

	}

	public DriverAppendResource update(DriverAppendResource resource, Long id, OAuth2Authentication auth) {

		try {
			findOne(id);

			if (auth != null) {

				String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

				resource.setUpdateUser(user);
			}

		} catch (Exception e) {
			e.getCause().getMessage();
			LOG.error("DriverAppend ERROR : update");
		}

		resource.setId(id);

		return DriverAppendConverter.toDto(repository.save(DriverAppendConverter.toEntity(resource)));

	}

	public void delete(Long id) {
		try {
			DriverAppendResource resource = findOne(id).get();

			deleteDocument(resource.getPath());

			repository.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("DriverAppend ERROR : delete");
		}
	}

	public void deleteDocument(String caminho) throws Exception {

		try {

			LOG.info("DriverAppend Controller START : deleteDocument");
			Appends.deleteDocuments(root, caminho);
			LOG.info("DriverAppend Controller END : deleteDocument");
		} catch (NullPointerException e) {
			LOG.error("DriverAppend Controller NullPointerException ERROR : deleteDocument");
			throw new NullPointerException(e.getMessage());
		} catch (RuntimeException e) {
			LOG.error("DriverAppend Controller RuntimeException ERROR : deleteDocument");
			throw new RuntimeException(e.getMessage());
		}

	}

	private Specification<DriverAppendEntity> getSpecification(DriverAppendFilter filter) {

		if (filter != null) {
			Specification<DriverAppendEntity> spec = Specification
					.where((filter.getId() == null) ? null : DriverAppendSpecification.isNotNullId());

			spec = (filter.getId() == null) ? spec : spec.and(DriverAppendSpecification.equalId(filter.getId()));
			spec = (StringUtils.isEmpty(filter.getDescription())) ? spec
					: spec.and(DriverAppendSpecification.likeDescription(filter.getDescription()));

			// referentes a tabela relacionada
			spec = (filter.getDriverId() == null) ? spec
					: spec.and(DriverAppendSpecification.equalDriver(filter.getDriverId()));

			return spec;

		} else {
			return null;
		}

	}

}
