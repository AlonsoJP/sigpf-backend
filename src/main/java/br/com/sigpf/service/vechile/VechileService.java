package br.com.sigpf.service.vechile;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

import br.com.sigpf.converter.VechileConverter;
import br.com.sigpf.entity.VechileEntity;
import br.com.sigpf.filter.VechileFilter;
import br.com.sigpf.repository.VechileRepository;
import br.com.sigpf.resource.VechileResource;
import br.com.sigpf.service.user.UserService;
import br.com.sigpf.specification.VechileSpecification;
import br.com.sigpf.util.GetInfoToken;

@Service
public class VechileService {

	@Autowired
	private VechileRepository repository;

	@Autowired
	UserService service;

	GetInfoToken getInfoToken = new GetInfoToken();

	public static final Logger LOG = LogManager.getLogger(VechileEntity.class);

	public List<VechileResource> findAll() throws Exception {

		try {
			LOG.info("VechileController END : findAll");

			return VechileConverter.listToDto(repository.findAll());
		} catch (Exception e) {

			LOG.error("VechileController ERROR : findAll");

			throw new Exception(e.getCause().getMessage());

		}

	}

	public Optional<VechileResource> findOne(Long id) {
		LOG.info("VechileController END : findOne");
		return VechileConverter.toOptionalDto(repository.findById(id));
	}

	public Page<VechileResource> findByFilter(VechileFilter filter, Pageable pageable) {
		LOG.info("VechileController END : findByFilter");
		return repository.findAll(getSpecification(filter), pageable)
				.map(VechileEntity -> VechileConverter.toDto(VechileEntity));
	}

	public List<VechileResource> findByFilter(VechileFilter filter) {
		LOG.info("VechileController END : findByFilter");
		return VechileConverter.listToDto(repository.findAll(getSpecification(filter)));
	}

	public VechileResource save(VechileResource resource, OAuth2Authentication auth) throws Exception {
		LOG.info("VechileController END : save");

		if (auth != null) {

			String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

			resource.setCreateUser(user);
		} else {
			throw new Exception("Usuario ausente ou não capturado.");
		}

		return VechileConverter.toDto(repository.save(VechileConverter.toEntity(resource)));

	}

	public VechileResource update(VechileResource resource, Long id, OAuth2Authentication auth) {

		try {
			findOne(id);

			if (auth != null) {

				String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

				resource.setUpdateUser(user);
			}

			LOG.info("VechileController END : update");

		} catch (Exception e) {
			e.getCause().getMessage();
			LOG.error("VechileController ERROR : update");
		}

		resource.setId(id);

		LOG.info("VechileController END : update");

		return VechileConverter.toDto(repository.save(VechileConverter.toEntity(resource)));

	}

	public void delete(Long id) {
		try {
			findOne(id);
			repository.deleteById(id);
			LOG.info("VechileController END : delete");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("VechileController ERROR : delete");
		}
	}

	private Specification<VechileEntity> getSpecification(VechileFilter filter) {

		if (filter != null) {
			Specification<VechileEntity> spec = Specification
					.where((filter.getId() == null) ? null : VechileSpecification.isNotNullId());

			spec = (filter.getId() == null) ? spec : spec.and(VechileSpecification.equalId(filter.getId()));

			return spec;

		} else {
			return null;
		}

	}

}
