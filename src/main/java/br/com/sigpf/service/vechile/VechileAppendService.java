package br.com.sigpf.service.vechile;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import br.com.sigpf.converter.VechileAppendConverter;
import br.com.sigpf.entity.VechileAppendEntity;
import br.com.sigpf.filter.VechileAppendFilter;
import br.com.sigpf.repository.VechileAppendRepository;
import br.com.sigpf.resource.VechileAppendResource;
import br.com.sigpf.service.user.UserService;
import br.com.sigpf.specification.VechileAppendSpecification;
import br.com.sigpf.util.Appends;
import br.com.sigpf.util.GetInfoToken;
import br.com.sigpf.util.ValidationAppends;

@Service
public class VechileAppendService {

	@Value("${path.appends.root}")
	private String root;

	@Value("${path.appends.vechile-append}")
	private String path;

	ValidationAppends validation = new ValidationAppends();

	@Autowired
	private VechileAppendRepository repository;

	@Autowired
	UserService service;

	GetInfoToken getInfoToken = new GetInfoToken();

	public static final Logger LOG = LogManager.getLogger(VechileAppendEntity.class);

	public Iterable<VechileAppendEntity> findAll() throws Exception {

		try {
			LOG.info("VechileAppend Controller END : findAll");

			return repository.findAll();
		} catch (Exception e) {
			e.getCause().getMessage();

			LOG.error("VechileAppend Controller ERROR : findAll");

			return null;
		}

	}

	public Optional<VechileAppendResource> findOne(Long id) {
		return VechileAppendConverter.toOptionalDto(repository.findById(id));
	}

	public Page<VechileAppendResource> findByFilter(VechileAppendFilter filter, Pageable pageable) {
		return repository.findAll(getSpecification(filter), pageable)
				.map(VechileAppendEntity -> VechileAppendConverter.toDto(VechileAppendEntity));
	}

	public VechileAppendResource save(VechileAppendResource resource, OAuth2Authentication auth) throws Exception {

		if (auth != null) {

			String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

			resource.setCreateUser(user);
			LOG.info("VechileAppend Controller END : save");
		} else {
			throw new Exception("Usuario ausente ou não capturado.");
		}

		return VechileAppendConverter.toDto(repository.save(VechileAppendConverter.toEntity(resource)));

	}

	public String saveDocument(MultipartFile document) throws Exception {

		try {

			validation.validationDocuments(document.getOriginalFilename());

			LOG.info("VechileAppend Controller Controller END : uploadDocument");
			return Appends.saveDocuments(document, root, path);

		} catch (NullPointerException e) {
			LOG.error("VechileAppend Controller NullPointerException ERROR : uploadDocument");
			throw new NullPointerException(e.getMessage());
		} catch (RuntimeException e) {
			LOG.error("VechileAppend Controller RuntimeException ERROR : uploadDocument");
			throw new RuntimeException(e.getMessage());
		}

	}

	public VechileAppendResource update(VechileAppendResource resource, Long id, OAuth2Authentication auth) {

		try {
			findOne(id);

			if (auth != null) {

				String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

				resource.setUpdateUser(user);
			}

			LOG.info("VechileAppend Controller END : update");

		} catch (Exception e) {
			e.getCause().getMessage();
			LOG.error("VechileAppend Controller ERROR : update");
		}

		resource.setId(id);

		return VechileAppendConverter.toDto(repository.save(VechileAppendConverter.toEntity(resource)));

	}

	public void delete(Long id) {
		try {
			VechileAppendResource resource = findOne(id).get();

			deleteDocument(resource.getPath());

			repository.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("VechileAppend Controller ERROR : delete");
		}
	}

	public void deleteDocument(String caminho) throws Exception {

		try {

			LOG.info("VechileAppend Controller START : deleteDocument");
			Appends.deleteDocuments(root, caminho);
			LOG.info("VechileAppend Controller END : deleteDocument");
		} catch (NullPointerException e) {
			LOG.error("VechileAppend Controller NullPointerException ERROR : deleteDocument");
			throw new NullPointerException(e.getMessage());
		} catch (RuntimeException e) {
			LOG.error("VechileAppend Controller RuntimeException ERROR : deleteDocument");
			throw new RuntimeException(e.getMessage());
		}

	}

	private Specification<VechileAppendEntity> getSpecification(VechileAppendFilter filter) {

		if (filter != null) {
			Specification<VechileAppendEntity> spec = Specification
					.where((filter.getId() == null) ? null : VechileAppendSpecification.isNotNullId());

			spec = (filter.getId() == null) ? spec : spec.and(VechileAppendSpecification.equalId(filter.getId()));
			spec = (StringUtils.isEmpty(filter.getDescription())) ? spec
					: spec.and(VechileAppendSpecification.likeDescription(filter.getDescription()));

			// referentes a tabela relacionada
			spec = (filter.getVechileId() == null) ? spec
					: spec.and(VechileAppendSpecification.equalVechile(filter.getVechileId()));

			return spec;

		} else {
			return null;
		}

	}

}
