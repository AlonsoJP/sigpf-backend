package br.com.sigpf.service.preferences;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

import br.com.sigpf.config.MailConfig;
import br.com.sigpf.converter.PreferencesConverter;
import br.com.sigpf.entity.PreferencesEntity;
import br.com.sigpf.filter.PreferencesFilter;
import br.com.sigpf.repository.PreferencesRepository;
import br.com.sigpf.resource.PreferencesResource;
import br.com.sigpf.service.studenty.StudentService;
import br.com.sigpf.service.user.UserService;
import br.com.sigpf.specification.PreferencesSpecification;
import br.com.sigpf.util.GetInfoToken;
import br.com.sigpf.util.Others;

@Service
public class PreferencesService {

	@Autowired
	private PreferencesRepository repository;

	@Autowired
	UserService service;

	@Autowired
	StudentService serviceStudent;

	@Autowired
	MailConfig serviceMail;

	GetInfoToken getInfoToken = new GetInfoToken();

	Others others = new Others();

	public static final Logger LOG = LogManager.getLogger(PreferencesEntity.class);

	// Agendamento de tarefa para notificação de encerramento de mês.
//	@Scheduled(cron = "0 0 7 28 * *")//TODO USAR ESSE EM PRODUÇÃO
	@Scheduled(cron = "0 0 10 * * *")
	public void sendMailEndMounth() {

		PreferencesResource preferences = findOne(1L).get();

		String subject = "Fechamento de mensalidade!";

		String messageBody = "O fechamento do mês de ".concat(LocalDate.now().getMonth().toString())
				.concat(" está próximo! Lembre-se de revisar todos os recebimentos, "
						+ "atrasados, não lançados e demais. Verifique também a situação cadastral dos alunos! Tenha um bom dia! \n\n E-mail gerado automaticamente, no dia: ")
				.concat(others.castLocalDateString(LocalDate.now())
						.concat(" \n Para desabilitar o envio de mensagens, acesse as preferências do sistema SIGPF."));

		LOG.info("PreferencesService BEGIN : sendMailEndMounth()");

		if (preferences.getNotificationEndMounth() == true) {
			LOG.info("PreferencesService END : sendMailEndMounth()");
			serviceMail.sendMail(preferences.getEmail(), subject, messageBody);
		}

	}

//	@Scheduled(cron = "0 0 7 1 * *")//TODO USAR ESSE EM PRODUÇÃO
	@Scheduled(cron = "0 0 10 * * *")
	public void sendMailCreateMounth() {

		PreferencesResource preferences = findOne(1L).get();

		String subject = "Lançamento de mensalidade!";

		String messageBody = "O lançamento do mês de ".concat(LocalDate.now().getMonth().toString())
				.concat(" acabou de chegar! Lembre-se de revisar todos os recebimentos, "
						+ "atrasados, não lançados e demais. Verifique também a situação cadastral dos alunos! Tenha um bom dia! \n\n E-mail gerado automaticamente, no dia: ")
				.concat(others.castLocalDateString(LocalDate.now())
						.concat(" \n Para desabilitar o envio de mensagens, acesse as preferências do sistema SIGPF."));

		LOG.info("PreferencesService BEGIN : sendMailCreateMounth()");

		if (preferences.getNotificationCreateMounth() == true) {
			LOG.info("PreferencesService END : sendMailCreateMounth()");
			serviceMail.sendMail(preferences.getEmail(), subject, messageBody);
		}

	}

//	@Scheduled(cron = "0 0 7 * * *")//TODO USAR ESSE EM PRODUÇÃO
	@Scheduled(cron = "0 0 10 * * *")
	public void sendMailStatus() {

		PreferencesResource preferences = findOne(1L).get();

//		serviceStudent.findByFilter();
		String subject = "Revise os dados cadastrais dos alunos e integrantes! Confira alumas estatísticas.";

		String messageBody = " Estudantes cadastros ATIVOS: "// TODO aqui vai a busca dos ativos
				.concat("\n Estudantes cadastrados INATIVOS: ")// TODO aqui vai a busca dos inatvos
				.concat("\n Veículos cadastrados: ")// TODO aqui vai a quantidade de veículos
				.concat("\n\n E-mail gerado automaticamente, no dia: ")
				.concat(others.castLocalDateString(LocalDate.now())
						.concat(" \n Para desabilitar o envio de mensagens, acesse as preferências do sistema SIGPF."));

		LOG.info("PreferencesService BEGIN : sendMailStatus()");

		if (preferences.getNotificationStatsSystem() == true) {
			LOG.info("PreferencesService END : sendMailStatus()");
			serviceMail.sendMail(preferences.getEmail(), subject, messageBody);
		}

	}

	public List<PreferencesResource> findAll() throws Exception {

		try {
			LOG.info("PreferencesController END : findAll");

			return PreferencesConverter.listToDto(repository.findAll());
		} catch (Exception e) {

			LOG.error("PreferencesController ERROR : findAll");

			throw new Exception(e.getCause().getMessage());

		}

	}

	public Optional<PreferencesResource> findOne(Long id) {
		LOG.info("PreferencesController END : findOne");
		return PreferencesConverter.toOptionalDto(repository.findById(id));
	}

	public Page<PreferencesResource> findByFilter(PreferencesFilter filter, Pageable pageable) {
		LOG.info("PreferencesController END : findByFilter");
		return repository.findAll(getSpecification(filter), pageable)
				.map(PreferencesEntity -> PreferencesConverter.toDto(PreferencesEntity));
	}

	public List<PreferencesResource> findByFilter(PreferencesFilter filter) {
		LOG.info("PreferencesController END : findByFilter");
		return PreferencesConverter.listToDto(repository.findAll(getSpecification(filter)));
	}

	public PreferencesResource save(PreferencesResource resource, OAuth2Authentication auth) throws Exception {
		LOG.info("PreferencesController END : save");

		if (auth != null) {

			String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

			resource.setCreateUser(user);
		} else {
			throw new Exception("Usuario ausente ou não capturado.");
		}

		return PreferencesConverter.toDto(repository.save(PreferencesConverter.toEntity(resource)));

	}

	public PreferencesResource update(PreferencesResource resource, Long id, OAuth2Authentication auth) {

		try {
			findOne(id);

			System.out.println(resource);

			if (auth != null) {

				String user = service.findByEmail(getInfoToken.opentToken(auth)).getUser_name();

				resource.setUpdateUser(user);
			}

			LOG.info("PreferencesController END : update");

		} catch (Exception e) {
			e.getCause().getMessage();
			LOG.error("PreferencesController ERROR : update");
		}

		resource.setId(id);

		LOG.info("PreferencesController END : update");

		return PreferencesConverter.toDto(repository.save(PreferencesConverter.toEntity(resource)));

	}

	private Specification<PreferencesEntity> getSpecification(PreferencesFilter filter) {

		if (filter != null) {
			Specification<PreferencesEntity> spec = Specification
					.where((filter.getId() == null) ? null : PreferencesSpecification.isNotNullId());

			spec = (filter.getId() == null) ? spec : spec.and(PreferencesSpecification.equalId(filter.getId()));// TODO
																												// não
																												// esta
																												// funcionando

			return spec;

		} else {
			return null;
		}

	}

}
