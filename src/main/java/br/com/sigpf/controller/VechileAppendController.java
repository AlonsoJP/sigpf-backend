package br.com.sigpf.controller;

import java.net.URI;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriBuilder;

import br.com.sigpf.entity.VechileAppendEntity;
import br.com.sigpf.filter.VechileAppendFilter;
import br.com.sigpf.resource.VechileAppendResource;
import br.com.sigpf.service.vechile.VechileAppendService;

@RestController
@RequestMapping("/api/vechiles/appends")
public class VechileAppendController {

	public static final Logger LOG = LogManager.getLogger(VechileAppendEntity.class);

	UriBuilder uriBuilder;

	@Autowired
	VechileAppendService service;

	@GetMapping({ "", "/" })
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Page<VechileAppendResource>> findAll(VechileAppendFilter filter, Pageable pageable)
			throws Exception {

		LOG.info("AddressController START : findAll");

		Page<VechileAppendResource> find = service.findByFilter(filter, pageable.previousOrFirst());

		return (find != null) ? ResponseEntity.ok(find) : ResponseEntity.notFound().build();

	}

	@GetMapping({ "{id}", "/{id}" })
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Optional<VechileAppendResource>> findById(@RequestHeader("Authorization") String jwt,
			@PathVariable(name = "id") Long id) throws Exception {

		LOG.info("AddressController START : findById");

		System.out.println(jwt);

		Optional<VechileAppendResource> resource = service.findOne(id);

		return resource != null ? ResponseEntity.ok(resource) : ResponseEntity.notFound().build();

	}

	@PostMapping({ "", "/" })
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('write')")
	public ResponseEntity<VechileAppendResource> save(@Valid @RequestBody VechileAppendResource resource,
			HttpServletResponse response, OAuth2Authentication auth) throws Exception {

		LOG.info("StudentAppendController START : save()");

		VechileAppendResource saved = service.save(resource, auth);

		URI path = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(saved.getId())
				.toUri();

		response.setHeader("Location", path.toASCIIString());

		return saved != null ? ResponseEntity.created(path).body(saved)
				: ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();

	}

	@PostMapping({ "/send" })
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('write')")
	public ResponseEntity<String> uploadDocument(@RequestParam MultipartFile document) throws Exception {

		LOG.info("StudentAppendController START : uploadDocument()");

		String path = service.saveDocument(document);

		return path != null ? ResponseEntity.status(HttpStatus.OK).body(path)
				: ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
	}

	@PutMapping({ "{id}", "/{id}" })
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('write')")
	public ResponseEntity<VechileAppendResource> update(@PathVariable(name = "id") Long id,
			@Valid @RequestBody VechileAppendResource updating, OAuth2Authentication auth) {

		LOG.info("AddressController START : update()");

		VechileAppendResource update = service.update(updating, id, auth);

		return update != null ? ResponseEntity.status(HttpStatus.OK).body(update)
				: ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();

	}

	@DeleteMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_ADMIN') and #oauth2.hasScope('write')")
	public ResponseEntity<Object> delete(@PathVariable(name = "id") Long id) {

		LOG.info("AddressController START : delete()");

		service.delete(id);

		return new ResponseEntity<Object>(HttpStatus.OK);

	}

}
