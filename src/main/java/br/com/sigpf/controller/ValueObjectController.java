package br.com.sigpf.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriBuilder;

import br.com.sigpf.service.vo.StatistcVOService;
import br.com.sigpf.vo.ResultsStatistcVO;
import br.com.sigpf.vo.StatistcVO;
import br.com.sigpf.vo.ValueStatisticsVO;

@RestController
@RequestMapping("/api/vo")
public class ValueObjectController {

	public static final Logger LOG = LogManager.getLogger(StatistcVO.class);

	UriBuilder uriBuilder;

	@Autowired
	StatistcVOService service;

	@GetMapping({ "/statistics" })
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Object> findAll() throws Exception {

		LOG.info("VOController START : statistcs");

		StatistcVO response = service.statistcs();

		return (response != null) ? ResponseEntity.ok(response) : ResponseEntity.notFound().build();

	}

	@GetMapping({ "/statisticsValues/inputs" })
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Object> findStatistValuesInputs() throws Exception {

		LOG.info("InputController START : findAll");

		ValueStatisticsVO find = service.findStatistInputValues();

		return (find != null) ? ResponseEntity.ok(find) : ResponseEntity.notFound().build();

	}

	@GetMapping({ "/statisticsValues/outputs" })
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Object> findStatistValuesOutputs() throws Exception {

		LOG.info("InputController START : findAll");

		ValueStatisticsVO find = service.findStatistOutputValues();

		return (find != null) ? ResponseEntity.ok(find) : ResponseEntity.notFound().build();

	}

	@GetMapping({ "/statisticsValues/results" })
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Object> findStatistValuesResults() throws Exception {

		LOG.info("InputController START : findAll");

		ResultsStatistcVO find = service.results();

		return (find != null) ? ResponseEntity.ok(find) : ResponseEntity.notFound().build();

	}

}
