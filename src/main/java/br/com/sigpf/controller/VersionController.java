package br.com.sigpf.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sigpf.entity.VersionEntity;
import br.com.sigpf.service.version.VersionService;

@RestController
@RequestMapping("/api/version")
public class VersionController {

	@Autowired
	VersionService service;

	public static final Logger LOG = LogManager.getLogger(VersionEntity.class);

	@GetMapping({ "last", "/last" })
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Object> findLast() {

		LOG.info("StudentController START : findAll");

		return ResponseEntity.ok(service.findLast());

	}

	@GetMapping({ "desc", "/desc" })
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<List<VersionEntity>> findAllDesc() {

		LOG.info("StudentController START : findAll");

		return ResponseEntity.ok(service.findAllDesc());

	}
}
