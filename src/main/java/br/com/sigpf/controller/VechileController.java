package br.com.sigpf.controller;

import java.net.URI;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriBuilder;

import br.com.sigpf.entity.VechileEntity;
import br.com.sigpf.filter.VechileFilter;
import br.com.sigpf.resource.VechileResource;
import br.com.sigpf.service.vechile.VechileService;

@RestController
@RequestMapping("/api/vechiles")
public class VechileController {

	public static final Logger LOG = LogManager.getLogger(VechileEntity.class);

	UriBuilder uriBuilder;

	@Autowired
	VechileService service;

	@GetMapping({ "", "/" })
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Page<VechileResource>> findAll(VechileFilter filter, Pageable pageable) throws Exception {

		LOG.info("AddressController START : findAll");

		Page<VechileResource> find = service.findByFilter(filter, pageable.previousOrFirst());

		return (find != null) ? ResponseEntity.ok(find) : ResponseEntity.notFound().build();

	}

	@GetMapping({ "{id}", "/{id}" })
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Optional<VechileResource>> findById(@RequestHeader("Authorization") String jwt,
			@PathVariable(name = "id") Long id) throws Exception {

		LOG.info("AddressController START : findById");

		System.out.println(jwt);

		Optional<VechileResource> resource = service.findOne(id);

		return resource != null ? ResponseEntity.ok(resource) : ResponseEntity.notFound().build();

	}

	@PostMapping({ "", "/" })
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('write')")
	public ResponseEntity<VechileResource> save(@Valid @RequestBody VechileResource resource,
			HttpServletResponse response, OAuth2Authentication auth) throws Exception {

		LOG.info("AddressController START : save()");

		VechileResource saved = service.save(resource, auth);

		URI path = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(saved.getId())
				.toUri();

		response.setHeader("Location", path.toASCIIString());

		return saved != null ? ResponseEntity.created(path).body(saved)
				: ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();

	}

	@PutMapping({ "{id}", "/{id}" })
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('write')")
	public ResponseEntity<VechileResource> update(@PathVariable(name = "id") Long id,
			@Valid @RequestBody VechileResource updating, OAuth2Authentication auth) {

		LOG.info("AddressController START : update()");

		VechileResource update = service.update(updating, id, auth);

		return update != null ? ResponseEntity.status(HttpStatus.OK).body(update)
				: ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();

	}

	@DeleteMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_ADMIN') and #oauth2.hasScope('write')")
	public ResponseEntity<Object> delete(@PathVariable(name = "id") Long id) {

		LOG.info("AddressController START : delete()");

		service.delete(id);

		return new ResponseEntity<Object>(HttpStatus.OK);

	}

}
