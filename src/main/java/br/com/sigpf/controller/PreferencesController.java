package br.com.sigpf.controller;

import java.net.URI;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriBuilder;

import br.com.sigpf.entity.PreferencesEntity;
import br.com.sigpf.filter.PreferencesFilter;
import br.com.sigpf.resource.PreferencesResource;
import br.com.sigpf.service.preferences.PreferencesService;

@RestController
@RequestMapping("/api/preferences")
public class PreferencesController {

	public static final Logger LOG = LogManager.getLogger(PreferencesEntity.class);

	UriBuilder uriBuilder;

	@Autowired
	PreferencesService service;

	@GetMapping({ "", "/" })
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Page<PreferencesResource>> findAll(PreferencesFilter filter, Pageable pageable)
			throws Exception {

		LOG.info("AddressController START : findAll");

		Page<PreferencesResource> find = service.findByFilter(filter, pageable.previousOrFirst());

		return (find != null) ? ResponseEntity.ok(find) : ResponseEntity.notFound().build();

	}

	@GetMapping({ "{id}", "/{id}" })
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Optional<PreferencesResource>> findById(@RequestHeader("Authorization") String jwt,
			@PathVariable(name = "id") Long id) throws Exception {

		LOG.info("AddressController START : findById");

		System.out.println(jwt);

		Optional<PreferencesResource> resource = service.findOne(id);

		return resource != null ? ResponseEntity.ok(resource) : ResponseEntity.notFound().build();

	}

	// não vai ser necessário
	@PostMapping({ "", "/" })
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('write')")
	public ResponseEntity<PreferencesResource> save(@Valid @RequestBody PreferencesResource resource,
			HttpServletResponse response, OAuth2Authentication auth) throws Exception {

		LOG.info("AddressController START : save()");

		PreferencesResource saved = service.save(resource, auth);

		URI path = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(saved.getId())
				.toUri();

		response.setHeader("Location", path.toASCIIString());

		return saved != null ? ResponseEntity.created(path).body(saved)
				: ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();

	}

	@PutMapping({ "{id}", "/{id}" })
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('write')")
	public ResponseEntity<PreferencesResource> update(@PathVariable(name = "id") Long id,
			@Valid @RequestBody PreferencesResource updating, OAuth2Authentication auth) {

		LOG.info("AddressController START : update()");

		PreferencesResource update = service.update(updating, id, auth);

		return update != null ? ResponseEntity.status(HttpStatus.OK).body(update)
				: ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();

	}

}
