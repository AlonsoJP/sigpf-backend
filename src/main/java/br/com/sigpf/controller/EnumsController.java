package br.com.sigpf.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sigpf.enums.PaymentTypeEnum;
import br.com.sigpf.enums.RoleTypeEnum;
import br.com.sigpf.enums.SubRoleTypeEnum;
import br.com.sigpf.enums.TypeTransactionEnum;

@RestController
@RequestMapping("/api/enums")
public class EnumsController {

	@GetMapping("/paymentTypeEnum")
	public ResponseEntity<List<PaymentTypeEnum>> paymentTypeEnum() {

		return ResponseEntity.ok(Arrays.asList(PaymentTypeEnum.values()));

	}

	@GetMapping("/roleTypeEnum")
	public ResponseEntity<List<RoleTypeEnum>> roleTypeEnum() {

		return ResponseEntity.ok(Arrays.asList(RoleTypeEnum.values()));

	}

	@GetMapping("/subRoleTypeEnum")
	public Object subRoleTypeEnum() {

		List<SubRoleTypeEnum> list = Arrays.asList(SubRoleTypeEnum.values());

		return list;

	}

	@GetMapping("/typeTransactionEnum")
	public ResponseEntity<List<TypeTransactionEnum>> typeTransactionEnum() {

		return ResponseEntity.ok(Arrays.asList(TypeTransactionEnum.values()));

	}

}