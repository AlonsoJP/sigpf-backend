package br.com.sigpf.controller;

import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sigpf.filter.AssociationFilter;
import br.com.sigpf.filter.CompanyFilter;
import br.com.sigpf.filter.CourseFilter;
import br.com.sigpf.filter.DistrictFilter;
import br.com.sigpf.filter.DriverFilter;
import br.com.sigpf.filter.InputFilter;
import br.com.sigpf.filter.OutputFilter;
import br.com.sigpf.filter.RecipeFilter;
import br.com.sigpf.filter.StreetFilter;
import br.com.sigpf.filter.StudentFilter;
import br.com.sigpf.filter.UniversityFilter;
import br.com.sigpf.filter.VechileFilter;
import br.com.sigpf.service.reports.ReportAssociationService;
import br.com.sigpf.service.reports.ReportCompanyService;
import br.com.sigpf.service.reports.ReportCourseService;
import br.com.sigpf.service.reports.ReportDistrictService;
import br.com.sigpf.service.reports.ReportDriverService;
import br.com.sigpf.service.reports.ReportInputService;
import br.com.sigpf.service.reports.ReportOutputService;
import br.com.sigpf.service.reports.ReportRecipeService;
import br.com.sigpf.service.reports.ReportStreetService;
import br.com.sigpf.service.reports.ReportUniversityService;
import br.com.sigpf.service.reports.ReportVechileService;
import br.com.sigpf.service.reports.ReportsStudentService;

@RestController
@RequestMapping("/api/reports")
public class ReportsController {

	@Autowired
	ReportsStudentService reportService;

	@Autowired
	ReportDistrictService districtService;

	@Autowired
	ReportStreetService streetService;

	@Autowired
	ReportCourseService courseService;

	@Autowired
	ReportUniversityService universityService;

	@Autowired
	ReportDriverService driverService;

	@Autowired
	ReportAssociationService associationService;

	@Autowired
	ReportVechileService vechileService;

	@Autowired
	ReportCompanyService companyService;

	@Autowired
	ReportRecipeService recipeService;

	@Autowired
	ReportInputService inputService;

	@Autowired
	ReportOutputService outputService;

	@GetMapping("/students")
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Object> reportStudent(StudentFilter filter) throws Exception {

		byte[] report = reportService.reportStudent(filter);

		String encodedString = Base64.getEncoder().encodeToString(report);

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, "application/pdf").body(encodedString);

	}

	@GetMapping("/students/detail")
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Object> reportStudentDetail(StudentFilter filter) throws Exception {

		byte[] report = reportService.reportStudentDetail(filter);

		String encodedString = Base64.getEncoder().encodeToString(report);

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, "application/pdf").body(encodedString);

	}

	@GetMapping("/students/newcontract")
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Object> reportStudentNewContract(StudentFilter filter) throws Exception {

		byte[] report = reportService.reportStudentContractOpen(filter);

		String encodedString = Base64.getEncoder().encodeToString(report);

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, "application/pdf").body(encodedString);

	}

	@GetMapping("/students/endcontract")
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Object> reportStudentEndContract(StudentFilter filter) throws Exception {

		byte[] report = reportService.reportStudentContractClosing(filter);

		String encodedString = Base64.getEncoder().encodeToString(report);

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, "application/pdf").body(encodedString);

	}

	@GetMapping("/districts")
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Object> reportDistrict(DistrictFilter filter) throws Exception {

		byte[] report = districtService.reportStudent(filter);

		String encodedString = Base64.getEncoder().encodeToString(report);

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, "application/pdf").body(encodedString);

	}

	@GetMapping("/streets")
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Object> reportStreet(StreetFilter filter) throws Exception {

		byte[] report = streetService.reportStudent(filter);

		String encodedString = Base64.getEncoder().encodeToString(report);

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, "application/pdf").body(encodedString);

	}

	@GetMapping("/courses")
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Object> reportStreet(CourseFilter filter) throws Exception {

		byte[] report = courseService.reportCourse(filter);

		String encodedString = Base64.getEncoder().encodeToString(report);

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, "application/pdf").body(encodedString);

	}

	@GetMapping("/universitys")
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Object> reportUniversity(UniversityFilter filter) throws Exception {

		byte[] report = universityService.reportUniversity(filter);

		String encodedString = Base64.getEncoder().encodeToString(report);

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, "application/pdf").body(encodedString);

	}

	@GetMapping("/universitys/detail")
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Object> reportUniversityDetail(UniversityFilter filter) throws Exception {

		byte[] report = universityService.reportUniversityDetail(filter);

		String encodedString = Base64.getEncoder().encodeToString(report);

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, "application/pdf").body(encodedString);

	}

	@GetMapping("/drivers")
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Object> reportDriver(DriverFilter filter) throws Exception {

		byte[] report = driverService.reportDriver(filter);

		String encodedString = Base64.getEncoder().encodeToString(report);

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, "application/pdf").body(encodedString);

	}

	@GetMapping("/drivers/detail")
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Object> reportDriverDetail(DriverFilter filter) throws Exception {

		byte[] report = driverService.reportDriverDetail(filter);

		String encodedString = Base64.getEncoder().encodeToString(report);

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, "application/pdf").body(encodedString);

	}

	@GetMapping("/companys")
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Object> reportCompany(CompanyFilter filter) throws Exception {

		byte[] report = companyService.reportCompany(filter);

		String encodedString = Base64.getEncoder().encodeToString(report);

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, "application/pdf").body(encodedString);

	}

	@GetMapping("/companys/detail")
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Object> reportCompanyDetail(CompanyFilter filter) throws Exception {

		byte[] report = companyService.reportCompanyDetail(filter);

		String encodedString = Base64.getEncoder().encodeToString(report);

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, "application/pdf").body(encodedString);

	}

	@GetMapping("/associations")
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Object> reportAssociation(AssociationFilter filter) throws Exception {

		byte[] report = associationService.reportAssociation(filter);

		String encodedString = Base64.getEncoder().encodeToString(report);

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, "application/pdf").body(encodedString);

	}

	@GetMapping("/vechiles")
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Object> reportVechile(VechileFilter filter) throws Exception {

		byte[] report = vechileService.reportVechile(filter);

		String encodedString = Base64.getEncoder().encodeToString(report);

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, "application/pdf").body(encodedString);

	}

	@GetMapping("/recipes")
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Object> reportRecipe(RecipeFilter filter) throws Exception {

		byte[] report = recipeService.reportRecipe(filter);

		String encodedString = Base64.getEncoder().encodeToString(report);

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, "application/pdf").body(encodedString);

	}

	@GetMapping("/inputs")
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Object> reportInputs(InputFilter filter) throws Exception {

		byte[] report = inputService.reportInput(filter);

		String encodedString = Base64.getEncoder().encodeToString(report);

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, "application/pdf").body(encodedString);

	}

	@GetMapping("/outputs")
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Object> reportOutputs(OutputFilter filter) throws Exception {

		byte[] report = outputService.reportOutput(filter);

		String encodedString = Base64.getEncoder().encodeToString(report);

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, "application/pdf").body(encodedString);

	}

}
