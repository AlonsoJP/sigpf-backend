package br.com.sigpf.controller;

import java.net.URI;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriBuilder;

import br.com.sigpf.entity.AssociationAppendEntity;
import br.com.sigpf.filter.AssociationAppendFilter;
import br.com.sigpf.resource.AssociationAppendResource;
import br.com.sigpf.service.association.AssociationAppendService;

@RestController
@RequestMapping("/api/associations/appends")
public class AssociationAppendController {

	public static final Logger LOG = LogManager.getLogger(AssociationAppendEntity.class);

	UriBuilder uriBuilder;

	@Autowired
	AssociationAppendService service;

	@GetMapping({ "", "/" })
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Page<AssociationAppendResource>> findAll(AssociationAppendFilter filter, Pageable pageable)
			throws Exception {

		LOG.info("AssociationAppendController START : findAll");

		Page<AssociationAppendResource> find = service.findByFilter(filter, pageable.previousOrFirst());

		return (find != null) ? ResponseEntity.ok(find) : ResponseEntity.notFound().build();

	}

	@GetMapping({ "{id}", "/{id}" })
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<Optional<AssociationAppendResource>> findById(@PathVariable(name = "id") Long id)
			throws Exception {

		LOG.info("AssociationAppendController START : findById");

		Optional<AssociationAppendResource> resource = service.findOne(id);

		return resource != null ? ResponseEntity.ok(resource) : ResponseEntity.notFound().build();

	}

	@PostMapping({ "", "/" })
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('write')")
	public ResponseEntity<AssociationAppendResource> save(@Valid @RequestBody AssociationAppendResource resource,
			HttpServletResponse response, OAuth2Authentication auth) throws Exception {

		LOG.info("AssociationAppendController START : save()");

		AssociationAppendResource saved = service.save(resource, auth);

		URI path = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(saved.getId())
				.toUri();

		response.setHeader("Location", path.toASCIIString());

		return saved != null ? ResponseEntity.created(path).body(saved)
				: ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();

	}

	@PostMapping({ "/send" })
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('write')")
	public ResponseEntity<String> uploadDocument(@RequestParam MultipartFile document) throws Exception {

		LOG.info("AddressController START : uploadDocument()");

		String path = service.saveDocument(document);

		return path != null ? ResponseEntity.status(HttpStatus.OK).body(path)
				: ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
	}

//	@DeleteMapping({ "/delete/{id}" })
//	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('write')")
//	public ResponseEntity<Object> deleteDocument(@PathVariable(name = "id") Long id, @RequestBody String caminho)
//			throws Exception {
//
//		LOG.info("AddressController START : deleteDocument()");
//		
//		service.deleteDocument(caminho);
//		
//		
//
//		String path = service.saveDocument(document);
//
//		return path != null ? ResponseEntity.status(HttpStatus.OK).body(path)
//				: ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
//	}

	@PutMapping({ "{id}", "/{id}" })
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('write')")
	public ResponseEntity<AssociationAppendResource> update(@PathVariable(name = "id") Long id,
			@Valid @RequestBody AssociationAppendResource updating, OAuth2Authentication auth) {

		LOG.info("AssociationAppendController START : update()");

		AssociationAppendResource update = service.update(updating, id, auth);

		return update != null ? ResponseEntity.status(HttpStatus.OK).body(update)
				: ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();

	}

	@DeleteMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_ADMIN') and #oauth2.hasScope('write')")
	public ResponseEntity<Object> delete(@PathVariable(name = "id") Long id) {

		LOG.info("AssociationAppendController START : delete()");

		service.delete(id);

		return new ResponseEntity<Object>(HttpStatus.OK);

	}

}
