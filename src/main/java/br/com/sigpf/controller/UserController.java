package br.com.sigpf.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriBuilder;

import br.com.sigpf.entity.StudentEntity;
import br.com.sigpf.filter.StudentFilter;
import br.com.sigpf.resource.UserResource;
import br.com.sigpf.service.user.UserService;
import br.com.sigpf.util.GetInfoToken;

@RestController
@RequestMapping("/api/users")
public class UserController {

	public static final Logger LOG = LogManager.getLogger(StudentEntity.class);

	UriBuilder uriBuilder;

	@Autowired
	UserService service;

	GetInfoToken infoToken = new GetInfoToken();

	@GetMapping({ "", "/" })
	@PreAuthorize("hasAuthority('ROLE_BASIC') and #oauth2.hasScope('read')")
	public ResponseEntity<UserResource> findAll(OAuth2Authentication auth, StudentFilter filter, Pageable pageable)
			throws Exception {

		LOG.info("UserController START : findAll");

		UserResource find = service.findByEmail(infoToken.opentToken(auth));

		return (find != null) ? ResponseEntity.ok(find) : ResponseEntity.notFound().build();

	}

}
